<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Pos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_list_data($like = null, $length = null, $start) {

        $sql = "SELECT a.opd_id,
            a.nopd,
            a.opd_name,
            a.opd_class_code ,
            a.opd_status ,
            a.opd_register_date ,
            a.opd_address ,
            a.opd_msisdn_1,
            a.npwpd,
            e.wpd_name,
            a.opd_mail,c.nm_kabkot,d.nm_kecamatan,f.nm_keldes
        FROM mst_opd a 
        INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
        INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
        INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
        INNER JOIN mst_wpd e on a.npwpd = e.npwpd
        where a.opd_status ='Y'";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.opd_class_code || a.opd_name || a.opd_address  || a.nopd || e.wpd_name  || c.nm_kabkot  || f.nm_keldes || d.nm_kecamatan ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.opd_name asc ) as rownum
                from( $sql order by a.opd_name asc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }
    
    public function get_count_data($like, $jml_data){

        $sql = "select count(*) cnt from (SELECT a.opd_id,
a.nopd,
            a.opd_name,
            a.opd_class_code ,
            a.opd_status ,
            a.opd_register_date ,
            a.opd_address ,
            a.opd_msisdn_1,
            a.npwpd,
            e.wpd_name,
            a.opd_mail,c.nm_kabkot,d.nm_kecamatan,f.nm_keldes
        FROM mst_opd a 
        INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
        INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
        INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
        INNER JOIN mst_wpd e on a.npwpd = e.npwpd
        where a.opd_status ='Y') x where 1=1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(x.opd_class_code || x.opd_name || x.opd_address || x.nopd || x.wpd_name  || x.nm_kabkot  || x.nm_keldes || x.nm_kecamatan) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    public function get_list_data_detail($like = null, $length = null, $start,$nopd) {

        $sql = "SELECT a.pos_id,
            a.pos_name,
            a.nopd ,
            b.opd_name,
            a.npwpd ,
            a.pos_node_id ,
            a.pos_code 
        FROM mst_pos a 
        inner join mst_opd b on  a.nopd = b.nopd
        where a.pos_status ='Y' and a.nopd = '".$nopd."'";
        if (!empty($like) && count($like) > 0) {
            $sql .= "and upper(a.pos_name || a.pos_node_id || a.pos_code ||a.nopd  || a.npwpd || a.opd_name ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.pos_name asc ) as rownum
                from( $sql order by a.pos_name asc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    public function get_count_data_detail($like, $jml_data,$nopd){

        $sql = "select count(*)cnt FROM (SELECT a.pos_id,
            a.pos_name,
            a.nopd ,
            b.opd_name,
            a.npwpd ,
            a.pos_node_id ,
            a.pos_code 
        FROM mst_pos a 
        inner join mst_opd b on a.nopd = b.nopd
        where a.pos_status ='Y' and a.nopd = '".$nopd."') x where 1=1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .=" and upper(x.pos_name || x.pos_node_id || x.pos_code ||x.nopd  || x.npwpd || x.opd_name ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    
    public function load_combo_npwpd() {
        $sql = "select nm_ctg_outlet,kd_ctg_outlet from mst_categ_outlet where sts_ctg_outlet='Y' order by nm_ctg_outlet asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    

    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from mst_pos
                where 1 = 1
                and upper(pos_name) = upper('" . $arr['pos_name'] . "')
                and pos_status = 'Y'
                and nopd = '" . $arr['nopd'] . "'
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    public function cek_node_id_before_insert($arr) {
        $sql = "select count(*) cnt
                from mst_pos
                where 1 = 1
                and pos_node_id = '" . $arr['pos_node_id'] . "'
                and pos_status = 'Y'
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
   
    private function cek_kode_outlet($pos_code) {
        $sql0 = "select count(*) cnt
                 from mst_pos
                 where 1 = 1
                 and pos_code = '$pos_code'";
        $result = $this->db->query($sql0)->row();
        return $result->cnt;
    }
    
    public function insert_data($arr) {
        $code_user = $this->session->userdata('efiskus_code');
        
        $pos_code = generate_code('mst_pos');

        while (intval($this->cek_kode_outlet($pos_code)) > 0) {
            $pos_code = generate_code('mst_pos');
        }
        $sql = "insert into mst_pos
                        (
                    pos_code,
                    pos_name,
                    pos_node_id,
                    pos_ref_code ,
                    pos_register_date ,
                    npwpd,
                    nopd,
                    pos_status,
                    create_by,
                    create_date)
                values (
                        '". $pos_code. "',
                        '". $arr['pos_name'] . "',
                        '". $arr['pos_node_id'] . "',
                        '". $arr['pos_ref_code'] . "',
                        now(),
                        '". $arr['npwpd'] . "',
                        '". $arr['nopd'] . "',
                        'Y',
                        '$code_user', now())";
        
        $this->db->query($sql);
    }
    
    public function cek_before_update($arr) {
        $sql = "select count(*) cnt
                from mst_pos 
                where 1 = 1
                and upper(pos_name) = upper('" . $arr['pos_name'] . "')
                and pos_status = 'Y'   
                and nopd =  '".$arr['pos_code']."'
                and pos_code not in('".$arr['pos_code']."')";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function cek_node_id_before_update($arr) {
        $sql = "select count(*) cnt
                from mst_pos
                where 1 = 1
                and pos_node_id = '" . $arr['pos_node_id'] . "'
                and pos_status = 'Y'
                and pos_code not in('".$arr['pos_code']."')
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function update_data($arr) {
        $code_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_pos set
                    pos_name = '". $arr['pos_name'] . "',
                    pos_node_id ='". $arr['pos_node_id'] . "',
                        update_by ='$code_user',
                        update_date =now()
                where pos_code ='". $arr['pos_code'] . "'";
                        
        $this->db->query($sql);
    }
    
    public function delete_data($arr) {
        $code_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_pos
                set pos_status = 'N',
                update_by ='$code_user',
                        update_date =now()
                where pos_code ='" . $arr['pos_code']."'";
        $this->db->query($sql);
    }

    public function get_detail_opd($nopd) {
         $sql = "SELECT a.*,c.wpd_name  
FROM mst_opd a 
INNER JOIN mst_wpd c on a.npwpd = c.npwpd 
where a.opd_status ='Y'
                and nopd ='" .$nopd."'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    public function get_detail_pos($pos_code) {
        $sql = "SELECT a.*,c.opd_name  
FROM mst_pos a 
INNER JOIN mst_opd c on a.nopd = c.nopd 
where a.pos_status ='Y'
                and pos_code ='" .$pos_code."'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    
}
