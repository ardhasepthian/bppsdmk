<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Sspd_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $npwpd_session ='';
        }else{
            $npwpd_session = " and a.npwpd = '".$npwpd_sess."'";
        }
		$sql = "select 
            a.sspd_code,
            a.wpd_name,
            a.wpd_address,
            a.npwpd,
            a.nopd,
            a.jenis_pajak,
            a.opd_name,
            a.opd_address,
            a.sspd_month,
            a.sspd_year,
            a.sspd_deposit_month_year
         from t_sspd a where 1 = 1   ".$npwpd_session;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.sspd_code || a.sspd_month || a.sspd_year||a.nopd ||a.npwpd || a.wpd_name || a.wpd_address || a.opd_name || a.opd_address  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY  x.sspd_year desc,x.sspd_month desc ) as rownum
                from( $sql order by a.sspd_year desc,a.sspd_month desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $npwpd_session ='';
        }else{
            $npwpd_session = " and a.npwpd = '".$npwpd_sess."'";
        }

        $sql = "select count(*) cnt
         from t_sspd a where 1 = 1 ".$npwpd_session;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.sspd_code || a.sspd_month || a.sspd_year||a.nopd ||a.npwpd || a.wpd_name || a.wpd_address || a.opd_name || a.opd_address  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    //=============================================================================================================================================

    public function get_list_data_opd($like = null, $length = null, $start) {
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else{
            $wherecon = 'and y.npwpd ='.$npwpd_sess;
        }

        $sql = "select y.* from( select a.trans_rekap_id,a.trans_rekap_month,a.trans_rekap_year,a.nopd,a.npwpd,a.trans_rekap_total,a.trans_rekap_servicecharge
,(select count(x.*)cnt from t_sspd x  where x.sspd_draft_status ='Y' and  x.sspd_month =a.trans_rekap_month and x.sspd_year = a.trans_rekap_year and x.nopd = a.nopd and x.npwpd = a.npwpd ) cnt
from t_rekap_opd a) y where y.cnt <= 0  ".$wherecon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(y.trans_rekap_month || y.trans_rekap_year || y.nopd  || y.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.trans_rekap_year desc, x.trans_rekap_month desc ) as rownum
                from( $sql order by y.trans_rekap_year desc, y.trans_rekap_month desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }

    public function get_count_data_opd($like, $jml_data){
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else{
            $wherecon = 'and y.npwpd ='.$npwpd_sess;
        }
        $sql = "select count(*) cnt
         from (select y.* from( select a.trans_rekap_id,a.trans_rekap_month,a.trans_rekap_year,a.nopd,a.npwpd,a.trans_rekap_total,a.trans_rekap_servicecharge
,(select count(x.*)cnt from t_sspd x  where x.sspd_draft_status ='Y' and  x.sspd_month =a.trans_rekap_month and x.sspd_year = a.trans_rekap_year and x.nopd = a.nopd and x.npwpd = a.npwpd ) cnt
from t_rekap_opd a) y where y.cnt <= 0 ".$wherecon.") v  where 1 = 1";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(v.trans_rekap_month || v.trans_rekap_year || v.nopd  || v.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    //=============================================================================================================================================

    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from t_sspd 
                where 1 = 1
                and upper(npwpd) = upper('" . $arr['npwpd'] . "')
                and upper(nopd) = upper('" . $arr['nopd'] . "')
                and sspd_month = '" . $arr['trans_rekap_month'] . "'
                and sspd_year = '" . $arr['trans_rekap_year'] . "'
                and sspd_draft_status = 'Y'
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
   
    public function cek_sspd_code($sspd_code) {
        $sql = "select count(*) cnt
                 from t_sspd
                 where 1 = 1
                 and sspd_code = '$sspd_code'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function insert_data($arr) {
        $code_user = $this->session->userdata('efiskus_code');
        $sspd_code = generate_code('t_sspd');
        while (intval($this->cek_sspd_code($sspd_code)) > 0) {
            $sspd_code = generate_code('t_sspd');
        }
        $sql = " insert into t_sspd
        (sspd_code,
	    sspd_month,
        sspd_year,
        sspd_correction_no,
        wpd_name,
        wpd_address,
        npwpd,
        nopd,
        opd_name,
        opd_address,
        opd_class_code,
        opd_class_desc,
        sspd_draft_total,
        sspd_draft_servicecharge,
        sspd_draft_dpp,
        sspd_draft_owe,
        att_sspd_status,
        att_omset_status,
        att_bill_status,
        att_other_status,
        sspd_draft_status,
        create_by,
        create_date
		) 
select '".$sspd_code."' as sspd_code,
	a.trans_rekap_month,
	a.trans_rekap_year,
	0 as sspd_correction_no,
	b.wpd_name,
	b.wpd_address,
	a.npwpd,
	a.nopd,
	c.opd_name,
	c.opd_address, 
	c.opd_class_code,
	(select x.opd_class_desc from mst_opd_class x where x.opd_class_code =c.opd_class_code) opd_class_desc,
	a.trans_rekap_total,
	a.trans_rekap_servicecharge,
	(a.trans_rekap_total + a.trans_rekap_servicecharge) dpp,
	(0.1 *(a.trans_rekap_total + a.trans_rekap_servicecharge)) owe,
	'N','N','N','N',
	'Y' as sspd_draft_status,
	'".$code_user."' as create_by,
	now() 
	from t_rekap_opd a 
 inner join mst_wpd b on a.npwpd =b.npwpd
inner join mst_opd c on a.nopd = c.nopd 
where a.trans_rekap_month ='".$arr['trans_rekap_month']."'
     and  a.trans_rekap_year ='".$arr['trans_rekap_year']."'
	and a.npwpd ='".$arr['npwpd']."'
	and a.nopd='".$arr['nopd']."'";

    // =========================================================================================================sql duplicate for t_sspd

        $sql1 = " insert into t_sspd
        (sspd_code,
	    sspd_month,
        sspd_year,
        sspd_correction_no,
        wpd_name,
        wpd_address,
        npwpd,
        nopd,
        opd_name,
        opd_address,
        opd_class_code,
        opd_class_desc,
        sspd_total,
        sspd_servicecharge,
        sspd_dpp,
        sspd_owe,
        sspd_status,
        create_by,
        create_date
		) 
select '".$sspd_code."' as sspd_code,
	a.trans_rekap_month,
	a.trans_rekap_year,
	0 as sspd_correction_no,
	b.wpd_name,
	b.wpd_address,
	a.npwpd,
	a.nopd,
	c.opd_name,
	c.opd_address, 
	c.opd_class_code,
	(select x.opd_class_desc from mst_opd_class x where x.opd_class_code =c.opd_class_code) opd_class_desc,
	a.trans_rekap_total,
	a.trans_rekap_servicecharge,
	(a.trans_rekap_total + a.trans_rekap_servicecharge) dpp,
	(0.1 *(a.trans_rekap_total + a.trans_rekap_servicecharge)) owe,
	'Y' as sspd_status,
	'".$code_user."' as create_by,
	now() 
	from t_rekap_opd a 
 inner join mst_wpd b on a.npwpd =b.npwpd
inner join mst_opd c on a.nopd = c.nopd 
where a.trans_rekap_month ='".$arr['trans_rekap_month']."'
     and  a.trans_rekap_year ='".$arr['trans_rekap_year']."'
	and a.npwpd ='".$arr['npwpd']."'
	and a.nopd='".$arr['nopd']."'";


        $this->db->query($sql);
        $this->db->query($sql1);
    }
    
    public function cek_before_update($arr) {
        $sql = "select count(*) cnt
                from mst_employee 
                where 1 = 1
                and upper(emp_name) = upper('" . $arr['emp_name'] . "')
                and emp_status = 'Y'
                and kd_kabkot = '" . $arr['kd_kabkot'] . "'
                and kd_kecamatan = '" . $arr['kd_kecamatan'] . "'    
                and emp_code not in('".$arr['emp_code']."')";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function update_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update t_sspd  set
                        sspd_total ='". $arr['sspd_total'] . "',
                        sspd_others ='". $arr['sspd_others'] . "',
                        sspd_servicecharge ='". $arr['sspd_servicecharge'] . "',
                        sspd_dpp ='". $arr['sspd_dpp'] . "',
                        sspd_owe ='". $arr['sspd_owe'] . "',
                        sspd_paid ='". $arr['sspd_paid'] . "',
                        sspd_more_paid ='". $arr['sspd_more_paid'] . "',
                        sspd_paidoff_date ='". $arr['sspd_paidoff_date'] . "',
                        att_other_desc='". $arr['att_other_desc'] . "',
                        att_sspd_status='". $arr['att_sspd_status'] . "',
                        att_bill_status='". $arr['att_bill_status'] . "',
                        att_omset_status='". $arr['att_omset_status'] . "',
                        att_other_status='". $arr['att_other_status'] . "',
                        update_by ='$id_user',
                        update_date =now()
                where sspd_code ='". $arr['sspd_code'] . "'";
        $this->db->query($sql);
    }
    
    public function send_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update t_sspd
                set sspd_status = 'SENT',
                update_by ='$id_user',
                        update_date =now()
                where sspd_code ='" . $arr."'";
        $sql2 = "update t_sspd
                set sspd_draft_status = 'SENT',
                update_by ='$id_user',
                        update_date =now()
                where sspd_code ='" . $arr."'";
        $this->db->query($sql);
        $this->db->query($sql2);
    }

    public function get_status_sspd($arr) {
        $sql = "select count(*) cnt
                from t_sspd 
                where 1 = 1
                and sspd_status <> 'SENT'    
                and sspd_code ='". $arr."'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    public function get_detail_sspd($sspd_code) {
         $sql = "SELECT a.* 
        FROM t_sspd a 
        where a.sspd_code ='" .$sspd_code."'";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }
    public function get_detail_draft_sspd($sspd_code) {
        $sql = "SELECT a.* 
        FROM t_sspd a 
        where a.sspd_code ='" .$sspd_code."'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }



    

    
}
