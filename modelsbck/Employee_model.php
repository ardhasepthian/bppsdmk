<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Employee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
		$usergroup =$this->session->userdata('siap_nm_group');
        $kd_prov =$this->session->userdata('siap_kd_prov');
        if ($usergroup !='SUPER ADMIN'){
            if($kd_prov ==''){
                $whercon ='';
            }else{
                $whercon =" and a.kd_prov ='".$kd_prov."'";
            }
            }else{
            $whercon ='';
            }
        $sql = "SELECT a.emp_id,
            a.emp_code,
            a.emp_name,
            a.kd_prov,
            a.kd_kabkot,
            a.kd_kecamatan,
            a.kd_keldes,
            a.emp_address,
            a.emp_msisdn_1,
            a.emp_status,
            a.emp_mail,
            c.nm_kabkot,d.nm_kecamatan,f.nm_keldes
        FROM mst_employee a 
        INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
        INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
        INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
        where a.emp_status ='Y'".$whercon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.emp_name || a.emp_address || c.nm_kabkot  || f.nm_keldes || d.nm_kecamatan ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.emp_code desc ) as rownum
                from( $sql order by a.emp_code desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){
		$usergroup =$this->session->userdata('siap_nm_group');
        $kd_prov =$this->session->userdata('siap_kd_prov');
        if ($usergroup !='SUPER ADMIN'){
            if($kd_prov ==''){
                $whercon ='';
            }else{
                $whercon =" and a.kd_prov ='".$kd_prov."'";
            }
            }else{
            $whercon ='';
            }
        $sql = "select count(*) cnt
                FROM ( SELECT a.emp_id,
            a.emp_code,
            a.emp_name,
            a.kd_prov,
            a.kd_kabkot,
            a.kd_kecamatan ,
            a.kd_keldes,
            a.emp_address ,
            a.emp_msisdn_1,
            a.emp_status,
            a.emp_mail,
            c.nm_kabkot,d.nm_kecamatan,f.nm_keldes
        FROM mst_employee a 
        INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
        INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
        INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
        where a.emp_status ='Y') x where 1 = 1 ".$whercon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(x.emp_name || x.emp_address || x.kd_kabkot || x.nm_keldes || x.nm_kecamatan) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
	public function load_combo_prov() {
        $kd_prov =$this->session->userdata('siap_kd_prov');
        $kd_kabkot =$this->session->userdata('siap_kd_kabkot');
        $usergroup =$this->session->userdata('siap_nm_group');
        if ($usergroup !='SUPER ADMIN'){
            if($kd_prov =='' && $kd_kabkot==''){
                $whercon ='';
            }else{
                $whercon =" where kd_prov ='".$kd_prov."'";
            }
            }else{
            $whercon ='';
            }
        
        
        $sql = "select nm_prov,kd_prov from ref_dati_1 ".$whercon." order by nm_prov asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
	
    public function load_combo_prov2() {
        $sql = "select nm_prov,kd_prov from ref_dati_1 order by nm_prov asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    
    public function load_combo_kabkot($id) {
        $sql = "select nm_kabkot,kd_kabkot from ref_dati_2 where kd_prov='$id' order by nm_kabkot asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        
        return FALSE;
    }
    
    public function load_combo_kecamatan($id) {
        $sql = "select nm_kecamatan,kd_kecamatan from ref_kecamatan where kd_kabkot='$id' order by nm_kecamatan asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    
    public function load_combo_keldes($id) {
        $sql = "select nm_keldes,kd_keldes from ref_kelurahan where kd_kecamatan='$id' order by nm_keldes asc ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    
    public function load_combo_kec() {
        $sql = "select nm_kecamatan,kd_kecamatan from ref_kecamatan  order by nm_kecamatan asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    
    public function load_combo_kdes() {
        $sql = "select nm_keldes,kd_keldes from ref_kelurahan order by nm_keldes asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    
    public function load_combo_kab() {
        $sql = "select nm_kabkot,kd_kabkot from ref_dati_2 order by nm_kabkot asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        
        return FALSE;
    }
    
    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from mst_employee 
                where 1 = 1
                and upper(emp_name) = upper('" . $arr['emp_name'] . "')
                and emp_status = 'Y'
                and kd_kabkot = '" . $arr['kd_kabkot'] . "'
                and kd_kecamatan = '" . $arr['kd_kecamatan'] . "'    
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
   
    public function cek_kode_emp($emp_code) {
        $sql = "select count(*) cnt
                 from mst_employee
                 where 1 = 1
                 and emp_code = '$emp_code'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function insert_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $emp_code = generate_code('mst_employee');
        while (intval($this->cek_kode_emp($emp_code)) > 0) {
            $emp_code = generate_code('mst_employee');
        }
        $sql = "insert into mst_employee
        (
        emp_code,
        emp_name,
        kd_prov,
        kd_kabkot,
        kd_kecamatan,
        kd_keldes,
        emp_address,
        emp_msisdn_1,
        emp_status,
        emp_mail,
        create_by,
        create_date,
        update_by,
        update_date)
                values (
                        '$emp_code',
                        '". $arr['emp_name'] . "',
                        '". $arr['kd_prov'] . "',
                        '". $arr['kd_kabkot'] . "',
                        '". $arr['kd_kecamatan'] . "',
                        '". $arr['kd_keldes'] . "',
                        '". $arr['emp_address'] . "',
                        '". $arr['emp_msisdn_1'] . "',
                        'Y',
                        '". $arr['emp_mail'] . "',  
                        '$id_user',
                        now(),
                        '$id_user', 
                        now())";
        $this->db->query($sql);
    }
    
    public function cek_before_update($arr) {
        $sql = "select count(*) cnt
                from mst_employee 
                where 1 = 1
                and upper(emp_name) = upper('" . $arr['emp_name'] . "')
                and emp_status = 'Y'
                and kd_kabkot = '" . $arr['kd_kabkot'] . "'
                and kd_kecamatan = '" . $arr['kd_kecamatan'] . "'    
                and emp_code not in('".$arr['emp_code']."')";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function update_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_employee set
                        emp_name ='". $arr['emp_name'] . "',
                        kd_prov ='". $arr['kd_prov'] . "',
                        kd_kabkot ='". $arr['kd_kabkot'] . "',
                        kd_kecamatan ='". $arr['kd_kecamatan'] . "',
                        kd_keldes ='". $arr['kd_keldes'] . "',
                        emp_address ='". $arr['emp_address'] . "',
                        emp_msisdn_1 ='". $arr['emp_msisdn_1'] . "',
                        emp_status ='Y',
                        update_by ='$id_user',
                        update_date =now()
                where emp_code ='". $arr['emp_code'] . "'";
        $this->db->query($sql);
    }
    
    public function delete_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_employee
                set emp_status = 'N',
                update_by ='$id_user',
                        update_date =now()
                where emp_code ='" . $arr['emp_code']."'";
        $this->db->query($sql);
    }

    public function get_detail_employee($emp_code) {
         $sql = "SELECT a.*,c.nm_kabkot,d.nm_kecamatan ,e.nm_prov,f.nm_keldes 
FROM mst_employee a 
INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
INNER JOIN ref_dati_1 e on a.kd_prov = e.kd_prov
INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
where a.emp_status ='Y'
                and emp_code ='" .$emp_code."'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }


    public function table_paging_data_user($like = null, $length = null, $start = 0 ,$wcon) {
        $sql = "select a.npwpd, a.wpd_name,a.wpd_address
                from mst_wpd a 
                where  1= 1 and a.npwpd  in (select x.kode from mst_user x where x.sts_user ='Y' and x.id_group = (select y.id_group from mst_usergroup y  where upper(y.nm_group) = upper('WAJIB PAJAK'))) ".$wcon;
        $sql = "select x.*, row_number() over(ORDER BY x.wpd_name asc) as rownum from($sql) x ";
        if (!empty($like) && count($like) > 0) {
            $sql .= "where 1=1";
            $sql .= " and (upper(x.npwpd) || upper(x.wpd_name) )) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    function count_paging_data_user($like, $jml_data,$wcon) {
        $sql = "select a.npwpd, a.wpd_name,a.wpd_address
                from mst_wpd a 
                where  1= 1 and a.npwpd  in (select x.kode from mst_user x where x.sts_user ='Y' and x.id_group = (select y.id_group from mst_usergroup y  where upper(y.nm_group) = upper('WAJIB PAJAK'))) ".$wcon;
        $sql = "select count(x.*) as total from ($sql) x ";
        if (!empty($like) && count($like) > 0) {
            $sql .= "where 1=1";
            $sql .= " and (upper(x.npwpd) || upper(x.wpd_name) ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $result = $this->db->query($sql);
        $result2 = $result->row();
        return $result2->total;
    }
    
   //=========================================EXCEL====================================== 
    
    public function insert_data2($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        
        $kd_emp = generate_code('mst_employee');
        while (intval($this->cek_kode_emp($kd_emp)) > 0) {
            $kd_emp = generate_code('mst_employee');
        }
        $sql = "insert into mst_employee
                        (emp_code,
                        emp_name,
                        kd_kabkot,
                        kd_kecamatan,
                        kd_keldes,
                        emp_address,
                        emp_msisdn_1,
                        emp_status,
                        emp_mail,
                        create_by,
                        create_date,
                        update_by,
                        update_date)
                values ('$kd_emp',
                        '". $arr['emp_name'] . "',
                        '". $arr['kd_kabkot'] . "',
                        '". $arr['kd_kecamatan'] . "',
                        '". $arr['kd_keldes'] . "',
                        '". $arr['emp_address'] . "',
                        '". $arr['emp_msisdn_1'] . "',
                        'Y',    
                        '". $arr['emp_mail'] . "',
                        '". $arr['kd_kabkot'] . "',
                        '$id_user',
                        '$id_user',
                        now())";
        
        $this->db->query($sql);
    }
    
}
