<?php

class Usergroup_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_list_data($like = null, $length = null, $start) {
        $sql = "select a.id_group,a.sts_group, a.nm_group from mst_usergroup a where 1 = 1 and a.sts_group = 'Y'";
        if (!empty($like)) {
            $sql .= " and upper(a.nm_group) like upper('%".$this->db->escape_like_str($like)."%') order by a.nm_group asc";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.nm_group asc ) as rownum from($sql) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    public function get_count_data($like, $jml_data){
        $sql = "select count(*) as cnt from mst_usergroup a where 1 = 1 and a.sts_group = 'Y'";
        if (!empty($like)) {
            $sql .= " and upper(a.nm_group) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt from mst_usergroup where upper(nm_group) = upper('" . $arr['nm_group'] . "') and sts_group = 'Y'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    private function cek_primarykey_id_group($id_group) {
        $sql0 = "select count(*)cnt from mst_usergroup where id_group = $id_group";
        $result = $this->db->query($sql0)->row();
        return $result->cnt;
    }

    public function insert_data($arr) {
        $nip = $this->session->userdata('simpati_nip');
        $sql = "insert into mst_usergroup
                 (nm_group, sts_group)
                 values
                 ('" . $arr['nm_group'] . "','Y')";
        $this->db->query($sql);
    }

    public function cek_before_update($arr) {
        $sql = "select count(*) cnt from mst_usergroup where upper(nm_group) = upper('" . $arr['nm_group'] . "')"
              ."and id_group not in(" . $arr['id_group'] . ") and sts_group = 'Y'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function update_data($arr) {
        $nip = $this->session->userdata('simpati_nip');
        $sql = "update mst_usergroup set nm_group='" . $arr['nm_group'] . "' where id_group = " . $arr['id_group'];
        $this->db->query($sql);
    }

    public function delete_data($arr) {
        $sql = "update mst_usergroup set sts_group='N' where id_group=" . $arr['id_group'];
        $this->db->query($sql);
    }

    public function get_detail_usergroup($id_group) {
        $sql = "select id_group, nm_group, sts_group from mst_usergroup where id_group =" . $id_group;
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }
    public function load_list_menu($id_group) {
        $sql ="select a.menu_id, x.menu_desc, b.id_group ,x.menu_urut from efiskus_mst_usergroup_menu a
                INNER JOIN  mst_usergroup b on a.id_group= b.id_group
                INNER JOIN  efiskus_mst_menu x on a.menu_id= x.menu_id
                where  a.usergroup_menu_sts = 1 and b.id_group =$id_group ORDER BY x.menu_urut";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
    }
    public function load_combo_menu() {
        $sql = "SELECT menu_id, menu_desc
                FROM efiskus_mst_menu
                where 1 = 1
                order by menu_urut asc";
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    public function load_combo_menu_selected($id_group) {
        $sql = "SELECT A.menu_id
                FROM efiskus_mst_usergroup_menu A INNER JOIN efiskus_mst_menu b ON A .menu_id = b.menu_id
                                                  AND b.menu_sts = 1
                WHERE 1 = 1
                AND A.usergroup_menu_sts = 1
                AND A.id_group =" . $id_group." order by b.menu_urut asc";
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }


    public function update_pengaturan_data($arr) {
        $id_group       = strtoupper($arr['id_group']);
        $arr_menu_data      = $arr['arr_menu_data'];
        $create_by          = $this->session->userdata('simpati_nip');

        $this->delete_usergroup_menu($id_group);
        $menu_count = count($arr_menu_data);
        if ($menu_count >= 1) {
            for ($a = 0; $a < $menu_count; $a++):
                $menu_id = $arr_menu_data[$a];
                $sql = "insert into efiskus_mst_usergroup_menu (id_group, menu_id, usergroup_menu_sts, create_by, create_date)
                                                                values ('$id_group', $menu_id, 1,
                                                                        '$create_by', now())";
                $this->db->query($sql);
            endfor;
        }
    }

    public function delete_usergroup_menu($id_group) {
        $update_by = $this->session->userdata('simpati_nip');
        $sql = "update efiskus_mst_usergroup_menu set usergroup_menu_sts = 0, update_by = '$update_by', update_date = now()
                where id_group = $id_group";
        $this->db->query($sql);
    }

    private function cek_primarykey_usergroup_menu_id($usergroup_menu_id) {
        $sql0 = "select count(*) cnt from efiskus_mst_usergroup_menu where usergroup_menu_id =$usergroup_menu_id";
        $result = $this->db->query($sql0)->row();
        return $result->cnt;
    }

}
