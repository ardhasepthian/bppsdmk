<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Sptpd_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else if($nama_group =='OBJEK PAJAK'){

            $wherecon = " and a.nopd ='".$npwpd_sess."'";
        }else if($nama_group =='WAJIB PAJAK'){
            $wherecon = " and a.npwpd ='".$npwpd_sess."'";
        }
		$sql = "select 
            a.sptpd_draft_code,
            a.sptpd_month,
            a.sptpd_year,
            a.sptpd_draft_total,
            a.sptpd_draft_servicecharge,
            a.npwpd,
            a.nopd,
            a.sptpd_draft_status
         from t_sptpd_draft a where 1 = 1  ".$wherecon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.sptpd_draft_code || a.sptpd_month || a.sptpd_year  || a.sptpd_draft_total || a.sptpd_draft_servicecharge||a.nopd ||a.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY  x.sptpd_year desc,x.sptpd_month desc ) as rownum
                from( $sql order by a.sptpd_year desc,a.sptpd_month desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){

        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else if($nama_group =='OBJEK PAJAK'){

            $wherecon = " and a.nopd ='".$npwpd_sess."'";
        }else if($nama_group =='WAJIB PAJAK'){
            $wherecon = " and a.npwpd ='".$npwpd_sess."'";
        }
        $sql = "select count(*) cnt
         from t_sptpd_draft a where 1 = 1  ".$wherecon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.sptpd_draft_code || a.sptpd_month || a.sptpd_year  || a.sptpd_draft_total || a.sptpd_draft_servicecharge||a.nopd ||a.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    //=============================================================================================================================================

    public function get_list_data_opd($like = null, $length = null, $start) {
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else if($nama_group =='OBJEK PAJAK'){

            $wherecon = " and y.nopd ='".$npwpd_sess."'";
        }else if($nama_group =='WAJIB PAJAK'){
            $wherecon = " and y.npwpd ='".$npwpd_sess."'";
        }

        $sql = "select y.* from( select a.trans_rekap_id,a.trans_rekap_month,a.trans_rekap_year,a.nopd,a.npwpd,a.trans_rekap_total,a.trans_rekap_servicecharge
,(select count(x.*)cnt from t_sptpd_draft x  where x.sptpd_draft_status in ('NEW','SENT','GENERATE SSPD') and  x.sptpd_month =a.trans_rekap_month and x.sptpd_year = a.trans_rekap_year and x.nopd = a.nopd and x.npwpd = a.npwpd ) cnt
from t_rekap_opd a) y where y.cnt <= 0  ".$wherecon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(y.trans_rekap_month || y.trans_rekap_year || y.nopd  || y.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.trans_rekap_year desc, x.trans_rekap_month desc ) as rownum
                from( $sql order by y.trans_rekap_year desc, y.trans_rekap_month desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }

    public function get_count_data_opd($like, $jml_data){
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else if($nama_group =='OBJEK PAJAK'){

            $wherecon = " and y.nopd ='".$npwpd_sess."'";
        }else if($nama_group =='WAJIB PAJAK'){
            $wherecon = " and y.npwpd ='".$npwpd_sess."'";
        }
        $sql = "select count(*) cnt  from(select y.* from( select a.trans_rekap_id,a.trans_rekap_month,a.trans_rekap_year,a.nopd,a.npwpd,a.trans_rekap_total,a.trans_rekap_servicecharge
,(select count(x.*)cnt from t_sptpd_draft x  where x.sptpd_draft_status in ('NEW','SENT','GENERATE SSPD') and  x.sptpd_month =a.trans_rekap_month and x.sptpd_year = a.trans_rekap_year and x.nopd = a.nopd and x.npwpd = a.npwpd ) cnt
from t_rekap_opd a) y where y.cnt <= 0 ".$wherecon.") v  where 1 = 1";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(v.trans_rekap_month || v.trans_rekap_year || v.nopd  || v.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    //=============================================================================================================================================

    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from t_sptpd_draft 
                where 1 = 1
                and upper(npwpd) = upper('" . $arr['npwpd'] . "')
                and upper(nopd) = upper('" . $arr['nopd'] . "')
                and sptpd_month = '" . $arr['trans_rekap_month'] . "'
                and sptpd_year = '" . $arr['trans_rekap_year'] . "'
                and sptpd_draft_status = 'Y'
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
   
    public function cek_sptpd_draft_code($sptpd_draft_code) {
        $sql = "select count(*) cnt
                 from t_sptpd_draft
                 where 1 = 1
                 and sptpd_draft_code = '$sptpd_draft_code'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function insert_data($arr) {
        $code_user = $this->session->userdata('efiskus_code');
        $sptpd_draft_code = generate_code('t_sptpd_draft');
        while (intval($this->cek_sptpd_draft_code($sptpd_draft_code)) > 0) {
            $sptpd_draft_code = generate_code('t_sptpd_draft');
        }
        $sql = " insert into t_sptpd_draft
        (sptpd_draft_code,
        kd_kabkot,
	    sptpd_month,
        sptpd_year,
        sptpd_correction_no,
        wpd_name,
        wpd_address,
        npwpd,
        nopd,
        opd_name,
        opd_address,
        opd_class_code,
        opd_class_desc,
        sptpd_draft_total,
        sptpd_draft_servicecharge,
        sptpd_draft_dpp,
        sptpd_draft_owe,
        att_sspd_status,
        att_omset_status,
        att_bill_status,
        att_other_status,
        sptpd_draft_status,
        create_by,
        create_date
		) 
select '".$sptpd_draft_code."' as sptpd_draft_code,
    a.kd_kabkot,
	a.trans_rekap_month,
	a.trans_rekap_year,
	0 as sptpd_correction_no,
	b.wpd_name,
	b.wpd_address,
	a.npwpd,
	a.nopd,
	c.opd_name,
	c.opd_address, 
	c.opd_class_code,
	(select x.opd_class_desc from mst_opd_class x where x.opd_class_code =c.opd_class_code) opd_class_desc,
	((a.trans_rekap_subtotal +a.trans_rekap_tax)-a.trans_rekap_discount),
	a.trans_rekap_servicecharge,
	(a.trans_rekap_total) dpp,
	(0.1 *(a.trans_rekap_total)) owe,
	'Y','N','N','N',
	'NEW' as sptpd_draft_status,
	'".$code_user."' as create_by,
	now() 
	from t_rekap_opd a 
 inner join mst_wpd b on a.npwpd =b.npwpd
inner join mst_opd c on a.nopd = c.nopd 
where a.trans_rekap_month ='".$arr['trans_rekap_month']."'
     and  a.trans_rekap_year ='".$arr['trans_rekap_year']."'
	and a.npwpd ='".$arr['npwpd']."'
	and a.nopd='".$arr['nopd']."'";

    // =========================================================================================================sql duplicate for t_SPTPD

        $sql1 = " insert into t_sptpd
        (sptpd_code,
        kd_kabkot,
	    sptpd_month,
        sptpd_year,
        sptpd_correction_no,
        wpd_name,
        wpd_address,
        npwpd,
        nopd,
        opd_name,
        opd_address,
        opd_class_code,
        opd_class_desc,
        sptpd_total,
        sptpd_servicecharge,
        sptpd_dpp,
        sptpd_owe,
        sptpd_status,
        att_sspd_status,
        create_by,
        create_date
		) 
select '".$sptpd_draft_code."' as sptpd_code,
    a.kd_kabkot,
	a.trans_rekap_month,
	a.trans_rekap_year,
	0 as sptpd_correction_no,
	b.wpd_name,
	b.wpd_address,
	a.npwpd,
	a.nopd,
	c.opd_name,
	c.opd_address, 
	c.opd_class_code,
	(select x.opd_class_desc from mst_opd_class x where x.opd_class_code =c.opd_class_code) opd_class_desc,
	((a.trans_rekap_subtotal +a.trans_rekap_tax)-a.trans_rekap_discount),
	a.trans_rekap_servicecharge,
	(a.trans_rekap_total) dpp,
	(0.1 *(a.trans_rekap_subtotal + a.trans_rekap_servicecharge)) owe,
	'NEW' as sptpd_status,'Y',
	'".$code_user."' as create_by,
	now() 
	from t_rekap_opd a 
 inner join mst_wpd b on a.npwpd =b.npwpd
inner join mst_opd c on a.nopd = c.nopd 
where a.trans_rekap_month ='".$arr['trans_rekap_month']."'
     and  a.trans_rekap_year ='".$arr['trans_rekap_year']."'
	and a.npwpd ='".$arr['npwpd']."'
	and a.nopd='".$arr['nopd']."'";


        $this->db->query($sql);
        $this->db->query($sql1);
    }
    
    public function update_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        if($arr['sptpd_paidoff_date']==''){
            $pod ='';
        }else{
            $pod ="sptpd_paidoff_date ='". $arr['sptpd_paidoff_date'] . "',";
        }


        $sql = "update t_sptpd  set
                        sptpd_total ='". $arr['sptpd_total'] . "',
                        sptpd_others ='". $arr['sptpd_others'] . "',
                        sptpd_servicecharge ='". $arr['sptpd_servicecharge'] . "',
                        sptpd_dpp ='". $arr['sptpd_dpp'] . "',
                        sptpd_owe ='". $arr['sptpd_owe'] . "',
                        sptpd_paid ='". $arr['sptpd_paid'] . "',
                        sptpd_more_paid ='". $arr['sptpd_more_paid'] . "',
                        ".$pod."
                        att_other_desc='". $arr['att_other_desc'] . "',
                        att_sspd_status='". $arr['att_sspd_status'] . "',
                        att_bill_status='". $arr['att_bill_status'] . "',
                        att_omset_status='". $arr['att_omset_status'] . "',
                        att_other_status='". $arr['att_other_status'] . "',
                        update_by ='$id_user',
                        update_date =now()
                where sptpd_code ='". $arr['sptpd_code'] . "'";
        $this->db->query($sql);
    }
    
    public function send_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update t_sptpd
                set sptpd_status = 'SENT',
                update_by ='$id_user',
                        update_date =now()
                where sptpd_code ='" . $arr."'";
        $sql2 = "update t_sptpd_draft
                set sptpd_draft_status = 'SENT',
                update_by ='$id_user',
                        update_date =now()
                where sptpd_draft_code ='" . $arr."'";
        $this->db->query($sql);
        $this->db->query($sql2);
    }




    public function get_status_sptpd($arr) {
        $sql = "select count(*) cnt
                from t_sptpd 
                where 1 = 1
                and sptpd_status <> 'SENT'    
                and sptpd_code ='". $arr."'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    public function get_detail_sptpd($sptpd_code) {
         $sql = "SELECT a.* 
        FROM t_sptpd a 
        where a.sptpd_code ='" .$sptpd_code."'";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }
    public function get_detail_draft_sptpd($sptpd_draft_code) {
        $sql = "SELECT a.* 
        FROM t_sptpd_draft a 
        where a.sptpd_draft_code ='" .$sptpd_draft_code."'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }


    public function cek_sspd_code($sspd_code) {
        $sql = "select count(*) cnt
                 from t_sspd
                 where 1 = 1
                 and sspd_code = '$sspd_code'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    public function generate_sspd_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sspd_code = generate_code('t_sspd');
        while (intval($this->cek_sspd_code($sspd_code)) > 0) {
            $sspd_code = generate_code('t_sspd');
        }

         $sql_insert="
         insert into t_sspd (
	npwpd ,
	kd_kabkot,
	wpd_name ,
	wpd_address ,
	nopd ,
	opd_name ,
	opd_address ,
	jenis_pajak ,
	sspd_year ,
	sspd_month ,
	sspd_deposit_month ,
	sspd_deposit_year ,
 	sspd_deposit_skpd_status ,
	sspd_deposit_skpd ,
	sspd_deposit_skpdkb_status , 
	sspd_deposit_skpdkb ,
	sspd_deposit_skpdkbt_status , 
	sspd_deposit_skpdkbt ,
	sspd_deposit_stp_status , 
	sspd_deposit_stp ,
	sspd_deposit_others_desc ,
	sspd_deposit_others_status ,
	sspd_deposit_others ,
	sspd_deposit_month_year ,
	sspd_status ,
	sspd_code ,
	create_by ,
	create_date ,
	sspd_date ,
	sspd_deposit_month_year_status,
	sptpd_code  ) 
	select  a.npwpd,a.kd_kabkot, a.wpd_name,a.wpd_address,
		a.nopd ,a.opd_name, a.opd_address,
		'PB1 Restoran' as jenis_pajak, a.sptpd_year,a.sptpd_month,a.sptpd_month as depo_month,a.sptpd_year as depo_year,
		null,null,null,null,null,null,null,null,null,null,null,
		a.sptpd_owe,'NEW','".$sspd_code."','".$id_user."',now(),NOW(),'Y','".$arr."'
	from t_sptpd a where a.sptpd_code ='".$arr."' 
         ";




            $sql1 = "update t_sptpd
                set sptpd_status = 'GENERATE SSPD',
                sptpd_paid = sptpd_owe,
                update_by ='$id_user',
                        update_date =now()
                where sptpd_code ='" . $arr."'";
            $sql2 = "update t_sptpd_draft
                set sptpd_draft_status = 'GENERATE SSPD',
                sptpd_draft_paid = sptpd_draft_owe,
                update_by ='$id_user',
                        update_date =now()
                where sptpd_draft_code ='" . $arr."'";
            $this->db->query($sql_insert);
            $this->db->query($sql1);
            $this->db->query($sql2);
        }


    

    
}
