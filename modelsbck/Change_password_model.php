<?php
/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Pegawai
  | Date       : 21-04-2017
  ========================================================= */

class Change_password_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function load_data() {
        $id_user = $this->session->userdata('id_user');
        $sql="select username,id_user  from mst_user where id_user=$id_user and sts_user='Y'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }

    }
    public function update_data($arr) {
        $id_user = $this->session->userdata('id_user');
        $pass = $arr['password'];
        $user_id = ($arr['user_id']);
        $sql="update mst_user set password ='$pass' where id_user = $id_user ";
        $result = $this->db->query($sql);

    }

}
