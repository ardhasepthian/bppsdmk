<?php

/* =======================================================
| Author     : Syafira Puji Virginia
| Email      :
| Class name : Satuan
| Date       : 02-05-2017
========================================================= */

class Opd_class_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
		$usergroup =$this->session->userdata('siap_nm_group');
        $kd_prov =$this->session->userdata('siap_kd_prov');
        if ($usergroup !='SUPER ADMIN'){
            if($kd_prov ==''){
                $whercon ='';
            }else{
                $whercon ='';
            }
        }
        $sql = "SELECT opd_class_code,opd_class_desc
        FROM mst_opd_class a
        where a.opd_class_status ='Y'".$whercon;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(opd_class_desc) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.opd_class_code desc ) as rownum
        from( $sql order by a.opd_class_code desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){
		$usergroup =$this->session->userdata('siap_nm_group');
        $kd_prov =$this->session->userdata('siap_kd_prov');
        if ($usergroup !='SUPER ADMIN'){
            if($kd_prov ==''){
                $whercon ='';
            }else{
                $whercon ='';
            }
        }
        $sql = "select count(*) cnt
        FROM mst_opd_class
        where opd_class_status ='Y'".$whercon;
        if (!empty($like) && count($like) > 0) {
            $sql .= "and upper(opd_class_desc) like upper ('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
	
    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
        from mst_opd_class
        where 1 = 1
        and upper(opd_class_desc) = upper('" . $arr['opd_class_desc'] . "')
        and opd_class_status = 'Y'  
        ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function cek_kode_opd($opd_code) {
        $sql = "select count(*) cnt
        from mst_opd_class
        where 1 = 1
        and opd_class_code = '$opd_code'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function insert_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $opd_code = generate_code('mst_opd_class');
        while (intval($this->cek_kode_opd($opd_code)) > 0) {
            $opd_code = generate_code('mst_opd_class');
        }
        $sql = "insert into mst_opd_class
        (
            opd_class_code,
            opd_class_desc,
            opd_class_status,
            create_by,
            create_date,
            update_by,
            update_date)
            values (
                '$opd_code',
                '". $arr['opd_class_desc'] . "',
                'Y',
                '$id_user',
                now(),
                '$id_user',
                now())";
                $this->db->query($sql);
            }
            
            public function cek_before_update($arr) {
                $sql = "select count(*) cnt
                from mst_opd_class 
                where 1 = 1
                and upper(opd_class_desc) = upper('" . $arr['opd_class_desc'] . "')
                and opd_class_status = 'Y'    
                and opd_class_code not in('".$arr['opd_class_code']."')";
                $result = $this->db->query($sql)->row();
                return $result->cnt;
            }
            
            public function update_data($arr) {
                $id_user = $this->session->userdata('efiskus_code');
                $sql = "update mst_opd_class set
                opd_class_desc ='". $arr['opd_class_desc'] . "',
                opd_class_status ='Y',
                update_by ='$id_user',
                update_date =now()
                where opd_class_code ='". $arr['opd_class_code'] . "'";
                $this->db->query($sql);
            }
            
            public function delete_data($arr) {
                $id_user = $this->session->userdata('efiskus_code');
                $sql = "update mst_opd_class
                set opd_class_status = 'N',
                update_by ='$id_user',
                update_date =now()
                where opd_class_code ='" . $arr['opd_class_code']."'";
                $this->db->query($sql);
            }
            
            public function get_detail_opd_class($opd_class_code) {
                $sql = "SELECT * from mst_opd_class where opd_class_status ='Y'
                and opd_class_code ='" .$opd_class_code."'";
                $result = $this->db->query($sql);
                if ($result->num_rows() > 0) {
                    return $result->row_array();
                }
                return FALSE;
            }
            
            
            public function table_paging_data_user($like = null, $length = null, $start = 0 ,$wcon) {
                $sql = "select a.npwpd, a.wpd_name,a.wpd_address
                from mst_wpd a 
                where  1= 1 and a.npwpd  in (select x.kode from mst_user x where x.sts_user ='Y' and x.id_group = (select y.id_group from mst_usergroup y  where upper(y.nm_group) = upper('WAJIB PAJAK'))) ".$wcon;
                $sql = "select x.*, row_number() over(ORDER BY x.wpd_name asc) as rownum from($sql) x ";
                if (!empty($like) && count($like) > 0) {
                    $sql .= "where 1=1";
                    $sql .= " and (upper(x.npwpd) || upper(x.wpd_name) )) like upper('%".$this->db->escape_like_str($like)."%')";
                }
                if (!empty($length)) {
                    $sql .= " limit $length offset $start";
                }
                $result = $this->db->query($sql);
                return ($result->num_rows() > 0) ? $result->result_array() : false;
            }
            
            function count_paging_data_user($like, $jml_data,$wcon) {
                $sql = "select a.npwpd, a.wpd_name,a.wpd_address
                from mst_wpd a 
                where  1= 1 and a.npwpd  in (select x.kode from mst_user x where x.sts_user ='Y' and x.id_group = (select y.id_group from mst_usergroup y  where upper(y.nm_group) = upper('WAJIB PAJAK'))) ".$wcon;
                $sql = "select count(x.*) as total from ($sql) x ";
                if (!empty($like) && count($like) > 0) {
                    $sql .= "where 1=1";
                    $sql .= " and (upper(x.npwpd) || upper(x.wpd_name) ) like upper('%".$this->db->escape_like_str($like)."%')";
                }
                $result = $this->db->query($sql);
                $result2 = $result->row();
                return $result2->total;
            }
            
            //=========================================EXCEL====================================== 
            
            public function insert_data2($arr) {
                $id_user = $this->session->userdata('efiskus_code');
                
                $kd_emp = generate_code('mst_opd_class');
                while (intval($this->cek_kode_emp($kd_emp)) > 0) {
                    $kd_emp = generate_code('mst_opd_class');
                }
                $sql = "insert into mst_opd_class
                (emp_code,
                emp_name,
                kd_kabkot,
                kd_kecamatan,
                kd_keldes,
                emp_address,
                emp_msisdn_1,
                emp_status,
                emp_mail,
                create_by,
                create_date,
                update_by,
                update_date)
                values ('$kd_emp',
                '". $arr['emp_name'] . "',
                '". $arr['kd_kabkot'] . "',
                '". $arr['kd_kecamatan'] . "',
                '". $arr['kd_keldes'] . "',
                '". $arr['emp_address'] . "',
                '". $arr['emp_msisdn_1'] . "',
                'Y',    
                '". $arr['emp_mail'] . "',
                '". $arr['kd_kabkot'] . "',
                '$id_user',
                '$id_user',
                now())";
                
                $this->db->query($sql);
            }
            
        }
        