<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Opd_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_list_data($like = null, $length = null, $start) {

        $sql = "SELECT a.nopd,
            a.opd_name,
            a.opd_address ,
            c.nm_kabkot,d.nm_kecamatan,f.nm_keldes
        FROM mst_opd a 
        INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
        INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
        INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
        where a.opd_status ='Y'";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper( a.opd_address || a.nopd || a.opd_name  || c.nm_kabkot  || f.nm_keldes || d.nm_kecamatan ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.opd_name asc ) as rownum
                from( $sql order by a.opd_name asc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }
    
    public function get_count_data($like, $jml_data){

        $sql = "select count(*) cnt from (select a.nopd,
            a.opd_name,
            a.opd_address ,
            c.nm_kabkot,d.nm_kecamatan,f.nm_keldes
        FROM mst_opd a 
        INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
        INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
        INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
        where a.opd_status ='Y') x where 1=1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper( x.opd_name || x.opd_address  || x.nopd || x.opd_name  || x.nm_kabkot  || x.nm_keldes || x.nm_kecamatan) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }




    public function get_detail_opd($nopd) {
        $sql = "SELECT a.*, c.nm_kabkot,d.nm_kecamatan,f.nm_keldes,b.nm_prov
                FROM mst_opd a 
                INNER JOIN ref_dati_1 b on a.kd_prov = b.kd_prov
                INNER JOIN ref_dati_2 c on a.kd_kabkot = c.kd_kabkot
                INNER JOIN ref_kecamatan d on a.kd_kecamatan = d.kd_kecamatan
                INNER JOIN ref_kelurahan f on a.kd_keldes = f.kd_keldes
                where a.opd_status ='Y'
                and a.nopd ='" .$nopd."'";
       // echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    
}
