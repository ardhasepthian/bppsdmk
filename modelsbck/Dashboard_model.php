<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
public function load_combo_prov() {
        $kd_prov =$this->session->userdata('siap_kd_prov');
        $kd_kabkot =$this->session->userdata('siap_kd_kabkot');
        $usergroup =$this->session->userdata('nm_group');
        if ($usergroup !='SUPER ADMIN'){
            if($kd_prov =='' && $kd_kabkot==''){
                $whercon ='';
            }else{
                $whercon =" where kd_prov ='".$kd_prov."'";
            }
            }else{
            $whercon ='';
            }
        
        
        $sql = "select nm_prov,kd_prov from ref_dati_1 ".$whercon." order by nm_prov asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }    

    public function load_combo_tahun() {
        $sql = "select distinct trans_rekap_year as tahun from t_rekap_opd order by trans_rekap_year desc ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    public function load_combo_kabkot($kd) {
        $sql = "select nm_kabkot,kd_kabkot from ref_dati_2 where kd_prov='$kd' order by nm_kabkot asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        
        return FALSE;
    }
    
    
    public function get_data_opd_permonth($arr) {
        $npwpd_sess = $this->session->userdata('efiskus_code');
        $nama_group = $this->session->userdata('efiskus_nm_group');
        if ($nama_group == 'SUPER ADMIN'){
            $wherecon='';
        }else if($nama_group =='OBJEK PAJAK'){
            $wherecon = " and c.nopd ='".$npwpd_sess."'";
        }else if($nama_group =='WAJIB PAJAK'){
            $wherecon = " and c.npwpd ='".$npwpd_sess."'";
        }

       $sql=  "select  c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
        coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.nopd,
	    coalesce( sum(c.trans_rekap_total),0) omset ,
        (select x.opd_name from mst_opd x where c.nopd =x.nopd)opd_name,
        (select z.nm_kecamatan from ref_kecamatan z where kd_kecamatan = (select x.kd_kecamatan from mst_opd x where c.nopd =x.nopd))opd_address,
        coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.nopd),0) pembetulan,
        coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.nopd ),0) realisasi
        from t_rekap_opd c 
        where  c.trans_rekap_month ='".$arr['bulan']."' and
        c.trans_rekap_year ='".$arr['tahun']."'
        ".$wherecon."
        GROUP BY c.trans_rekap_month,c.trans_rekap_year,nopd ORDER BY tahun asc, bulan asc";
        //return $sql;die;
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        
        return FALSE;
    }

    public function get_data_opd_permonth_detail($arr) {

        $sql=  "
select  a.tgl,
	coalesce((select sum(c.trans_total) omset
	from t_trans_opd c	
        where 
	to_char(c.trans_date,'MM')='".$arr['bulan']."' and
        to_char(c.trans_date,'YYYY') ='".$arr['tahun']."'
        and c.nopd  ='".$arr['nopd']."'
	and to_char(c.trans_date,'DD') = a.tgl
        GROUP BY a.tgl, to_char(c.trans_date,'DD') ),0) omset 
from mst_tanggal a 
	where 1= 1 and a.tgl BETWEEN '01' and '".$arr['jml_hari']."'
";
       // echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }



    public function get_list_data_perwp($like = null, $length = null, $start,$arr) {
        $sql = "select  c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
        coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.nopd,
        (select x.opd_name from mst_opd x where c.nopd =x.nopd)opd_name,
        (select z.nm_kecamatan from ref_kecamatan z where kd_kecamatan = (select x.kd_kecamatan from mst_opd x where c.nopd =x.nopd))opd_address,
        coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.nopd),0) pembetulan,
        coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.nopd ),0) realisasi
        from t_rekap_opd c 
        where c.trans_rekap_month ='".$arr['bulan']."' and
        c.trans_rekap_year ='".$arr['tahun']."'
        and c.kd_kabkot ='".$arr['kd_kabkot']."'
        GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.nopd
        ";

        $sql = "select x.*, row_number() over(ORDER BY x.draft desc ,x.pembetulan desc ) as rownum
                from( $sql ) x where 1 = 1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper( x.opd_name || x.opd_address|| x.nopd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    public function get_count_data_perwp($like, $jml_data,$arr){
        $sql = "select  c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
        coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.nopd,
        (select x.opd_name from mst_opd x where c.nopd =x.nopd)opd_name,
        (select z.nm_kecamatan from ref_kecamatan z where kd_kecamatan = (select x.kd_kecamatan from mst_opd x where c.nopd =x.nopd))opd_address,
        coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.nopd),0) pembetulan,
        coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.nopd ),0) realisasi
        from t_rekap_opd c
        where c.trans_rekap_month ='".$arr['bulan']."' and
        c.trans_rekap_year ='".$arr['tahun']."'
        and c.kd_kabkot ='".$arr['kd_kabkot']."'
        GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.nopd";
        $sql ="select count (x.*)cnt from ($sql) x where 1 =1  " ;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(x.opd_name || x.opd_address|| x.nopd  )like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function load_data_chart ($arr){
        $sql="select a.nm_kabkot,a.kd_kabkot,x.bulan,x.tahun,
coalesce(x.draft,0)draft,
coalesce(x.pembetulan,0)pembetulan,
coalesce(x.realisasi,0)realisasi from ref_dati_2 a
left join (
select c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
	coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.kd_kabkot,
coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.kd_kabkot),0) pembetulan,
coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.kd_kabkot ),0) realisasi

 from t_rekap_opd c 

where c.trans_rekap_month ='".$arr['bulan']."' and
c.trans_rekap_year ='".$arr['tahun']."'

GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.kd_kabkot

) x on x.kd_kabkot = a.kd_kabkot";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }


    public function load_data_opd_year ($arr){
        $sql="select a.bulan,x.opd_name,x.tahun,
coalesce(x.omset,0)omset,
coalesce(x.draft,0)draft,
coalesce(x.pembetulan,0)pembetulan,
coalesce(x.realisasi,0)realisasi 
from mst_bulan a
left join (
select c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
	coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,
coalesce(sum(c.trans_rekap_total),0) omset ,
c.nopd, (select x.opd_name from mst_opd x where x.nopd = c.nopd and x.opd_status ='Y' )opd_name,
coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.nopd),0) pembetulan,
coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.nopd ),0) realisasi

 from t_rekap_opd c 

where c.nopd ='".$arr['nopd']."' and
c.trans_rekap_year ='".$arr['tahun']."'

GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.nopd ) x on x.bulan = a.bulan order by a.bulan asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }



    public function get_list_data_opd($like = null, $length = null, $start) {

        $sql = "select * from mst_opd  where 1 = 1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.nopd || a.opd_name || a.opd_address   ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.opd_name asc ) as rownum
                from( $sql ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }

    public function get_count_data_opd($like, $jml_data){
        $sql = "select count(*) cnt from mst_opd  where 1 = 1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(a.nopd || a.opd_name || a.opd_address   ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    public function get_list_data_hari($like = null, $length = null, $start,$arr) {

        $sql = "select to_char(a.trans_date,'HH24:mi') jam,a.trans_total, a.trans_code,
(select x.pos_name from mst_pos x where x.pos_code = a.pos_code)pos_name 
from t_trans_opd a where a.nopd='".$arr['nopd']."' and to_char(a.trans_date,'dd-mm-YYYY') ='".$arr['hari']."-".$arr['bulan']."-".$arr['tahun']."'";
        if (!empty($like) && count($like) > 0) {
            $sql = "select x.* from ($sql)x where 1 = 1  and upper(x.pos_name || x.trans_code || x.jam   ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.jam asc ) as rownum
                from( $sql ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }

    public function get_count_data_hari($like, $jml_data,$arr){
        $sql = "select count(x.*) cnt from (select to_char(a.trans_date,'HH24:mi') jam,a.trans_total, a.trans_code,
(select x.pos_name from mst_pos x where x.pos_code = a.pos_code)pos_name 
from t_trans_opd a where a.nopd='".$arr['nopd']."' and to_char(a.trans_date,'dd-mm-YYYY') ='".$arr['hari']."-".$arr['bulan']."-".$arr['tahun']."') x where 1=1";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(x.pos_name || x.trans_code || x.jam   ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }



}

?>
