<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Report_realisasi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }



    public function load_combo_tahun() {
        $sql = "select distinct trans_rekap_year as tahun from t_rekap_opd ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    public function get_list_data_perwp($like = null, $length = null, $start,$arr) {
                $sql = "select  c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
        coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.nopd,
        (select x.opd_name from mst_opd x where c.nopd =x.nopd)opd_name,
        (select z.nm_kecamatan from ref_kecamatan z where kd_kecamatan = (select x.kd_kecamatan from mst_opd x where c.nopd =x.nopd))opd_address,
        coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.nopd),0) pembetulan,
        coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.nopd ),0) realisasi
        from t_rekap_opd c 
        where c.trans_rekap_month ='".$arr['bulan']."' and
        c.trans_rekap_year ='".$arr['tahun']."'
        and c.kd_kabkot ='".$arr['kd_kabkot']."'
        GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.nopd
        ";

        $sql = "select x.*, row_number() over(ORDER BY x.draft desc ,x.pembetulan desc ) as rownum
                from( $sql ) x where 1 = 1 ";
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper( x.wpd_name || x.wpd_address|| x.npwpd ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    public function get_count_data_perwp($like, $jml_data,$arr){
        $sql = "select  c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
        coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.nopd,
        (select x.opd_name from mst_opd x where c.nopd =x.nopd)opd_name,
        (select z.nm_kecamatan from ref_kecamatan z where kd_kecamatan = (select x.kd_kecamatan from mst_opd x where c.nopd =x.nopd))opd_address,
        coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.nopd),0) pembetulan,
        coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.nopd ),0) realisasi
        from t_rekap_opd c
        where c.trans_rekap_month ='".$arr['bulan']."' and
        c.trans_rekap_year ='".$arr['tahun']."'
        and c.kd_kabkot ='".$arr['kd_kabkot']."'
        GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.nopd";
        $sql ="select count (x.*)cnt from ($sql) x where 1 =1  " ;
        if (!empty($like) && count($like) > 0) {
            $sql .= " and upper(x.wpd_name || x.wpd_address|| x.npwpd  )like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function load_data_chart ($arr){
        $sql="select a.nm_kabkot,a.kd_kabkot,x.bulan,x.tahun,
coalesce(x.draft,0)draft,
coalesce(x.pembetulan,0)pembetulan,
coalesce(x.realisasi,0)realisasi from ref_dati_2 a
left join (
select c.trans_rekap_month as bulan,c.trans_rekap_year as tahun,
	coalesce(0.1 * (sum(c.trans_rekap_total)),0) draft ,c.kd_kabkot,
coalesce((select sum(d.sptpd_owe) from t_sptpd d where c.trans_rekap_month = d.sptpd_month and c.trans_rekap_year = d.sptpd_year GROUP BY d.sptpd_month,d.sptpd_year,d.kd_kabkot),0) pembetulan,
coalesce((select sum(e.sspd_deposit_month_year) from t_sspd e where e.sspd_status ='PAID' and c.trans_rekap_month = e.sspd_month and c.trans_rekap_year = e.sspd_year GROUP BY e.sspd_month,e.sspd_year,e.kd_kabkot ),0) realisasi

 from t_rekap_opd c 

where c.trans_rekap_month ='".$arr['bulan']."' and
c.trans_rekap_year ='".$arr['tahun']."'

GROUP BY c.trans_rekap_month,c.trans_rekap_year,c.kd_kabkot

) x on x.kd_kabkot = a.kd_kabkot";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }



    public function get_data_excel($arr) {
        $sql = "SELECT nm_penjual,nm_ctg_produk,nm_produk,
                max(harga_satuan)harga
                from v_jual
                where 1 = 1 
                ".$arr['kd_ctg_produk'].$arr['kd_kabkot'].$arr['kd_prov'].$arr['kd_produk']." and tanggal between to_date('".$arr['tgl_awal']."','yyyy-mm-dd') and to_date('".$arr['tgl_akhir']."','yyyy-mm-dd')
		GROUP BY nm_penjual,nm_ctg_produk,nm_produk ";
        $sql = "select x.*, row_number() over(ORDER BY x.nm_ctg_produk asc ,x.nm_produk asc ,x.harga desc ) as rownum
                from( $sql ) x where 1 = 1";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
}
