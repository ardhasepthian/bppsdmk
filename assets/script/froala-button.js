/**
 * Created by User on 11/7/2017.
 */
$(function(){
    $.FroalaEditor.DefineIcon('doubleLine', {NAME: 'th-list'});
    $.FroalaEditor.RegisterCommand('doubleLine', {
        title: 'Double Line',
        focus: true,
        undo: true,
        refreshAfterCallback: true,
        callback: function () {
            this.html.insert('<hr class="thick"><hr class="thin">');
            this.undo.saveStep();
        }
    });

    $.FroalaEditor.DefineIcon('tabSpace', {NAME: 'angle-double-right'});
    $.FroalaEditor.RegisterCommand('tabSpace', {
        title: 'Tab',
        focus: true,
        undo: true,
        refreshAfterCallback: true,
        callback: function () {
            this.html.insert('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
            this.undo.saveStep();
        }
    });

    $.FroalaEditor.DefineIcon('checkList', {NAME: 'square-o'});
    $.FroalaEditor.RegisterCommand('checkList', {
        title: 'Box Checklist',
        focus: true,
        undo: true,
        refreshAfterCallback: true,
        callback: function () {
            this.html.insert('&#9633;');
            this.undo.saveStep();
        }
    });

    $.FroalaEditor.DefineIcon('checkListFull', {NAME: 'square'});
    $.FroalaEditor.RegisterCommand('checkListFull', {
        title: 'Box Checked',
        focus: true,
        undo: true,
        refreshAfterCallback: true,
        callback: function () {
            this.html.insert('&#9635;');
            this.undo.saveStep();
        }
    });
});