<!-- =======================================================
  | Author     : ardha
  | Email      :
  | Class name : change password
  | Date       : 21-04-2017
  ========================================================= -->

<?php
$this->load->view('layout/header');
?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>
<script>
    
    $(document).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);
        
        $.validator.setDefaults({ignore: ":hidden:not(select)"}); 
        $("#form").validate({
            submitHandler: function (form) {
            var password1 = $("#password1").val().trim();
            var password2 = $("#password2").val().trim();
            //alert(password1);
            if(password1 != password2 ){
                $('#msgx').show().html('Password Tidak Sama');
                $('#msgx').fadeOut(5000);
                return;
            }
            if (!password1){
                $('#msgx').show().html('Password kosong');
                $('#msgx').fadeOut(5000);
                return;
            }
                $.blockUI({ message: 'Mohon tunggu' });
                $.post("<?php echo base_url(); ?>change_password/change_pass", {
                password : $("#password1").val().trim(), 
                user_id : $('#user_id').val().trim()
                },
                        function (data) {
                            alert('Data akan di proses');
                            alert('Password berhasil diubah');
                            window.location= "<?php echo base_url(); ?>login/signout";
                        });
            }
        });
    });
    function cek(){
            var password1 = $("#password1").val().trim();
            var password2 = $("#password2").val().trim();
            if(password1 != password2 ){
                $('#msgx').show().html('Password Tidak Sama');
                $('#msgx').fadeOut(5000);
                return;
            }
    }
    function cancel(){
        window.location ="<?php echo base_url()?>dashboard";
    }
</script>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> Form Ubah Password</h4>

            </div>

        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class=panel>
            <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">

            <div class="form-horizontal form-label-left col-md-8" >
                <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username<span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <input type="hidden" id="user_id" value="<?php echo $username['id_user'] ;?>"/>
                                <input type="text" autocomplete="off" id="username" readonly  maxlength="18" value="<?php echo $username['username'] ;?>" class="form-control col-md-7 col-xs-12"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password Baru
                            </label>
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <input type="password" autocomplete="off" id="password1" name="password1" maxlength="32" class="form-control col-md-7 col-xs-12 required" ;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Konfirmasi Password 
                            </label>
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <input type="password" autocomplete="off" id="password2" name="password2" onblur="cek();"  maxlength="32" class="form-control col-md-7 col-xs-12 required"  />
                            </div>
                        </div>
                        
                     </div>   
                        


    </div>
                <div class="panel-footer">
                    <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
                    <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
                </div>
            </form>
</div>
  </div>
    </div>

<?php
$this->load->view('layout/footer');
?>