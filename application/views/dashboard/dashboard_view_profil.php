<?php
$this->load->view('layout/header_dash');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->
<script src="<?php echo base_url(); ?>assets/script/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/js/orgchart.js"></script>
<script src="<?php echo base_url(); ?>assets/script/apex/apexcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/script/jquery.easypiechart.min.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/script/apex/styles.css" rel="stylesheet">
<style>

    .opd_jm {
        text-align: center;
        font-size: 16px;
        font-weight: bolder;
    }

    .opd_tit {
        text-align: center;
        font-size: 12px;
    }

    .opd_jm2 {
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
    }

    .opd_tit2 {
        text-align: center;
        font-size: 14px;
    }

    @media only screen and (max-width: 600px) {
        .xx2 {
            border-radius: 2px;
            max-height: 40px;
            padding: 1px;
            margin-bottom: 2px;
            border: solid 1px white;

        }
        .opd_jm2{
            text-align: center;
            font-size: 14px;
            font-weight: bolder;
        }
        .opd_tit2{
            text-align: center;
            font-size: 10px;
        }
        .padlef{
            padding-left: 5px;
        }

        .opd_jm {
            text-align: center;
            font-size: 12px;
            font-weight: bolder;
        }
        .mhgh {
            max-height: 50px !important;
            padding-top: 10px !important;
        }
        .opd_tit {
            text-align: center;
            font-size: 10px;
        }
        /*.dchart{*/
        /*max-height: 150px;*/
        /*font-size: 10px;*/

        /*}*/
    }

    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 480px)
    and (-webkit-min-device-pixel-ratio: 2) {
        .width-cols {
            color: red;
            height: 100px;
            width: 50%;
        }
    }



    .mhgh {
        height: 250px;
        padding-top: 50px;

    }




    .grad_bluemod {
        background: #fdfdff; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Safari 5.1-6*/
        background: -o-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 49, 48)); /*Opera 11.1-12*/
        background: -moz-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Fx 3.6-15*/
        background: linear-gradient(to bottom, #fff, #b33b3f); /*Standard*/

        border: solid 2px white;
    }

    .xx {
        border-radius: 2px;
        /*height: 65px;*/
        padding: 1px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .xx2 {
        border-radius: 5px;
        height: 65px;
        padding: 5px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .pman {
        margin: 1px;
        /*font-size: larger;*/
    }



    .abangtext {
        color: #ffffff;
        background-color: #b40000;
    }

</style>


<div class="page-title hidden">
    <div class="title_left">

    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
        <h3>&nbsp;&nbsp;
            Profil Badan PPSDM Kesehatan
        </h3>

        <div class="col-md-12 panel">
            <div class="panel  col-md-6 col-xs-12 ">
                <div class="panel-heading ">Visi & Misi
                    <div class="panel-actions">
                        <!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('ttl')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 400px ; font-size:16px;" id="vsms">
                    <p><strong>Visi Badan PPSDM Kesehatan</strong></p>
                    <p>" Penggerak Terwujudnya Pengembangan dan Pemberdayaan Sumber Daya Manusia Kesehatan yang Professional Dalam Mewujudkan Masyarakat Sehat yang Mandiri dan Berkeadilan" <br /><strong></strong></p>
                    <p><strong>Misi Badan PPSDM Kesehatan</strong></p>
                    <ol>
                        <li>Memenuhi jumlah, jenis, dan mutu SDM Kesehatan sesuai yang direncanakan dalam mendukung penyelenggaraan pembangunan kesehatan</li>
                        <li>Menyerasikan pengadaan SDM Kesehatan melalui pendidikan dan pelatihan dengan kebutuhan SDM Kesehatan dalam mendukung pembangunan kesehatan</li>
                        <li>Menjamin pemerataan, pemanfaatan, dan pengembangan SDM Kesehatan dalam pelayanan kesehatan kepada masyarakat</li>
                        <li>Meningkatkan pembinaan dan pengawasan mutu SDM Kesehatan</li>
                        <li>Memantapkan manajemen dan dukungan kegiatan teknis serta sumber daya pengembangan dan pemberdayaan SDM kesehatan</li>
                    </ol>
                    <p></p>
                </div>
            </div>
            <div class="panel  col-md-6 col-xs-12 ">
                <div class="panel-heading ">Renstra
                    <div class="panel-actions">
                        <!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('ttl')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 400px; font-size:16px; " id="rsntr">
                    <p><strong>Rencana Strategis </strong></p>
                    <ol>
                        <li>Memenuhi jumlah, jenis, dan mutu SDM Kesehatan sesuai yang direncanakan dalam mendukung penyelenggaraan pembangunan kesehatan</li>
                        <li>Menyerasikan pengadaan SDM Kesehatan melalui pendidikan dan pelatihan dengan kebutuhan SDM Kesehatan dalam mendukung pembangunan kesehatan</li>
                        <li>Menjamin pemerataan, pemanfaatan, dan pengembangan SDM Kesehatan dalam pelayanan kesehatan kepada masyarakat</li>
                        <li>Meningkatkan pembinaan dan pengawasan mutu SDM Kesehatan</li>
                        <li>Memantapkan manajemen dan dukungan kegiatan teknis serta sumber daya pengembangan dan pemberdayaan SDM kesehatan</li>
                    </ol>
                </div>
            </div>

            <div class="panel  col-md-12 col-xs-12 ">
                <div class="panel-heading ">Struktur Organisasi
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('ttl')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body " style="min-height: 400px;width: 100%; overflow-x: scroll;" id="stkorg">

                </div>
            </div>
<!--            <div class="panel  col-md-12 col-xs-12 ">-->
<!--                <div class="panel-heading ">Struktur Organisasi 2-->
<!--                    <div class="panel-actions">-->
<!--                       -->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="panel-body" style="min-height: 430px" >-->
<!--                    <img src="--><?php //echo base_url()?><!--assets/img/sotk.jpg" alt="" width="100%" >-->
<!--                </div>-->
<!--            </div>-->

        </div>

        <div class="col-md-12" id="loc-wid" style="margin-top:15px;">







        </div>


    </section>
</section>
<script>
    function init_EasyPieChart() {

        if (typeof ($.fn.easyPieChart) === 'undefined') {
            return;
        }
        console.log('init_EasyPieChart');

        $('.chart').easyPieChart({
            easing: 'easeOutElastic',
            delay: 3000,
            barColor: '#26B99A',
            trackColor: '#fff',
            scaleColor: false,
            lineWidth: 20,
            trackWidth: 16,
            lineCap: 'butt',
            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
        var chart = window.chart = $('.chart').data('easyPieChart');
        $('.js_update').on('click', function () {
            chart.update(Math.random() * 200 - 100);
        });

        //hover and retain popover when on popover content
        var originalLeave = $.fn.popover.Constructor.prototype.leave;
        $.fn.popover.Constructor.prototype.leave = function (obj) {
            var self = obj instanceof this.constructor ?
                obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
            var container, timeout;

            originalLeave.call(this, obj);

            if (obj.currentTarget) {
                container = $(obj.currentTarget).siblings('.popover');
                timeout = self.timeout;
                container.one('mouseenter', function () {
                    //We entered the actual popover – call off the dogs
                    clearTimeout(timeout);
                    //Let's monitor popover content instead
                    container.one('mouseleave', function () {
                        $.fn.popover.Constructor.prototype.leave.call(self, self);
                    });
                });
            }
        };

        $('body').popover({
            selector: '[data-popover]',
            trigger: 'click hover',
            delay: {
                show: 50,
                hide: 400
            }
        });

    };


    $(document).ready(function () {
        var ptimg ="<?php echo base_url()?>assets/img/avatar.jpg";
        var chart = new OrgChart(document.getElementById("stkorg"), {
            mouseScrool: OrgChart.action.none,
            template: "polina",
            scaleInitial: 0.6,
            menu: {
                pdf: { text: "Export PDF" },
                png: { text: "Export PNG" },
                svg: { text: "Export SVG" },
                csv: { text: "Export CSV" }
            },

            layout: OrgChart.mixed,
            nodeBinding: {
                img_0: "img",
                field_0: "name",
                field_1: "title"

            },
            nodes: [
                { id: "1",pid:null, name: "Nama", title: "Kepala Badan PPSDM", email: "amber@domain.com", img:ptimg },
                { id: "2", pid: "1", name: "Nama", title: "Sekertaris Badan", email: "ava@domain.com", img:ptimg },
                { id: "3", pid: "1", name: "Nama", title: "Kepala Pusat Perencanaan", email: "jay@domain.com", img: ptimg },
                { id: "99", pid: "1", name: "Nama", title: "Kepala Pusat Pendidikan SDMK", email: "99@domain.com", img: ptimg },
                { id: "98", pid: "1", name: "Nama", title: "Kepala Pusat Pelatihan SDMK", email: "97@domain.com", img: ptimg },
                { id: "97", pid: "1", name: "Nama", title: "Kepala Pusat Peningkatan Mutu SDMK", email: "98@domain.com", img: ptimg },
                { id: "5", pid: "2", name: "Nama", title: "Kabag Program dan informasi", img: ptimg },
                { id: "6", pid: "2", name: "Nama", title: "Kabag Hukum dan humas", img: ptimg },
                { id: "7", pid: "2", name: "Nama", title: "Kabag Keuangan dan BMN", img: ptimg },
                { id: "21", pid: "2", name: "Nama", title: "Kabag Kepegawaian dan Umum", email: "kohen@domain.com", img: ptimg },
                { id: "8", pid: "3", name: "Nama", title: "Kabid", email: "kohen@domain.com", img: ptimg },
                { id: "9", pid: "3", name: "Nama", title: "Kabid", img: ptimg },
                { id: "10", pid: "97", name: "Nama", title: "Kabid", img: ptimg },
                { id: "11", pid: "98", name: "Nama", title: "Kabid", img: ptimg },
                { id: "12", pid: "99", name: "Nama", title: "Kabid", img: ptimg },
                { id: "13", pid: "99", name: "Nama", title: "Kabid", img: ptimg },
                { id: "14", pid: "97", name: "Nama", title: "Kabid", img: ptimg },
                { id: "15", pid: "98", name: "Nama", title: "Kabid", img: ptimg },
                { id: "16", pid: "99", name: "Nama", title: "Kabid", img: ptimg }
            ]
        });


    });









</script>



<?php
$this->load->view('layout/footer');
?>
