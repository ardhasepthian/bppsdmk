<?php
$this->load->view('layout/header_dash');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->

<style>

    .tdkanan {
        text-align: right;
    }

    .opd_tit {
        text-align: center;
        font-size: 12px;
    }

    .opd_jm2 {
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
    }

    .opd_tit2 {
        text-align: center;
        font-size: 14px;
    }

    @media only screen and (max-width: 600px) {

        .opd_tit {
            text-align: center;
            font-size: 10px;
        }

        /*.dchart{*/
        /*max-height: 150px;*/
        /*font-size: 10px;*/
        /*}*/
    }

    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 480px)
    and (-webkit-min-device-pixel-ratio: 2) {
        .width-cols {
            color: red;
            height: 100px;
            width: 50%;
        }
    }

    .mhgh {
        height: 250px;
        padding-top: 50px;

    }

    .grad_bluemod {
        background: #fdfdff; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Safari 5.1-6*/
        background: -o-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 49, 48)); /*Opera 11.1-12*/
        background: -moz-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Fx 3.6-15*/
        background: linear-gradient(to bottom, #fff, #b33b3f); /*Standard*/

        border: solid 2px white;
    }

    .xx {
        border-radius: 2px;
        /*height: 65px;*/
        padding: 1px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .xx2 {
        border-radius: 5px;
        height: 65px;
        padding: 5px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .pman {
        margin: 1px;
        /*font-size: larger;*/
    }

    .abangtext {
        color: #ffffff;
        background-color: #b40000;
    }

</style>


<div class="page-title hidden">
    <div class="title_left">

    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
        <b>
            <center><h3 style="margin-top: 1px;margin-bottom: 8px">&nbsp;
                    Monitoring Kinerja Program
                </h3></center>
        </b>
        <div class="col-md-12">
            <!--            <h4 style="margin-top: 3px;margin-bottom: 3px">PAGU</h4>-->
            <div class="panel  col-md-4 col-xs-12 ">
                <div class="panel-heading ">Renstra
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('ttl')"-->
<!--                           class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>


                <div class="panel-body" style="min-height: 200px;font-size: 15px;">
                    <p><strong>Rencana Strategis </strong></p>
                    <ol>
                        <li>Memenuhi jumlah, jenis, dan mutu SDM Kesehatan sesuai yang direncanakan dalam mendukung penyelenggaraan pembangunan kesehatan</li>
                        <li>Menyerasikan pengadaan SDM Kesehatan melalui pendidikan dan pelatihan dengan kebutuhan SDM Kesehatan dalam mendukung pembangunan kesehatan</li>
                        <li>Menjamin pemerataan, pemanfaatan, dan pengembangan SDM Kesehatan dalam pelayanan kesehatan kepada masyarakat</li>
                    </ol>
                </div>
            </div>
            <div class="panel  col-md-8 col-xs-12 ">
                <div class="panel-heading">Kinerja Program
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('jb')"-->
<!--                           class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="col-md-3 col-xs-6">
                        <h5 style="text-align: center">Presentase Puskesmas Tanpa Dokter <br/><br/></h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="40">
                                            <span class="percent">40</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 40</h5>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <h5 style="text-align: center">Presentase Puskesmas Dengan Jenis Tenaga Kesehatan Sesuai Standar <br/>
                        </h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="57">
                                            <span class="percent">57</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 57</h5>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <h5 style="text-align: center">Presentase RSUD Yang Memiliki 4 Dokter Spesialis Dasar dan 3 Dokter Spesialis Lainnya
                        </h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="68">
                                            <span class="percent">68</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 68</h5>
                    </div>
                    <div class="col-md-3 col-xs-6">
                        <h5 style="text-align: center">Jumlah SDM Kesehatan Yang ditingkatkan Kompetensinya <br/><br/>
                        </h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="68">
                                            <span class="percent">68</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 68</h5>
                    </div>
                    <!--                    <div class="" id="chart_jenis_belanja"></div>-->
                </div>
            </div>
            <div class="panel  col-md-12 col-xs-12 ">
                <div class="panel-heading">Kinerja Utama
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('kwn')"-->
<!--                           class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="col-md-2 col-xs-6">
                        <h5 style="text-align: center">Jumlah tenaga kesehatan yang ditempatkan secara team based (Peserta baru)<br/></h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="40">
                                            <span class="percent">40</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 40</h5>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <h5 style="text-align: center">Jumlah tenaga kesehatan yang ditempatkan dalam rangka penugasan khusus perorangan<br/></h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="57">
                                            <span class="percent">57</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 57</h5>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <h5 style="text-align: center">Jumlah dokter residen yang ditempatkan dalam rangka penugasan khusus Residen</h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="68">
                                            <span class="percent">68</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 68</h5>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <h5 style="text-align: center">Jumlah lulusan pendidikan Dokter Spesialis Baru yang menjalani WKS<br/></h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="40">
                                            <span class="percent">40</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 40</h5>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <h5 style="text-align: center">Jumlah dokumen perencanaan SDMK<br/><br/></h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="57">
                                            <span class="percent">57</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 57</h5>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <h5 style="text-align: center">Jumlah tenaga kesehatan yang melaksanakan internsip<br/></h5>
                        <span class="chart " style="margin-top: 20px;margin-left: 40px " data-percent="68">
                                            <span class="percent">68</span>
                                            <canvas height="220" width="220"
                                                    style="height: 110px; width: 110px;"></canvas>
                                            </span>
                        <h5 style="text-align: center"> Target  : 100</h5>
                        <h5 style="text-align: center"> Capaian : 68</h5>
                    </div>


<!--                    <div class="" id="chart_kewenangan"></div>-->
                </div>
            </div>
                <div class="panel  col-md-12 col-xs-12 ">
                <div class="panel-heading">Rincian Kinerja Program
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('sd')"-->
<!--                           class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
<!--                    <div class="" id="chart_sd"></div>-->
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Indikator Kinerja</th>
                            <th>2020</th>
                            <th>2021</th>
                            <th>2022</th>
                            <th>2023</th>
                            <th>2024</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Presentase Puskesmas Tanpa Dokter</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Presentase Puskesmas Dengan Jenis Tenaga Kesehatan Sesuai Standar</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Presentase RSUD Yang Memiliki 4 Dokter Spesialis Dasar dan 3 Dokter Spesialis Lainnya</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Jumlah SDM Kesehatan Yang ditingkatkan Kompetensinya</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="panel  col-md-12 col-xs-12 ">
                <div class="panel-heading">Rincian Kinerja Utama
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('hdrbulan')"-->
<!--                           class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
<!--                    <div class="" id="chart_lelang_bulan"></div>-->
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Indikator Kinerja</th>
                            <th>2020</th>
                            <th>2021</th>
                            <th>2022</th>
                            <th>2023</th>
                            <th>2024</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Jumlah tenaga kesehatan yang ditempatkan secara team based (Peserta baru)</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Jumlah tenaga kesehatan yang ditempatkan dalam rangka penugasan khusus perorangan</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Jumlah dokter residen yang ditempatkan dalam rangka penugasan khusus Residen</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Jumlah lulusan pendidikan Dokter Spesialis Baru yang menjalani WKS</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Jumlah dokumen perencanaan SDMK</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Jumlah tenaga kesehatan yang melaksanakan internsip</td>
                            <td>10 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                            <td>0 %</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>

    </section>
</section>

<div id="modalsatker" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Usia per Satker</h4>
            </div>
            <div class="modal-body">
                <table id="data_user_table" class="table table-bordered responsive" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align:center">No</th>
                        <th>Satker</th>
                        <th>20-30</th>
                        <th>30-35</th>
                        <th>35-40</th>
                        <th>40-45</th>
                        <th> >45</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>


<script>


    $(document).ready(function () {


       // go();
       // kepeg();
        $('#modalsatker').on('show.bs.modal', function (e) {
            $('#data_user_table').css('width', '');
        });
        $('#modalsatker').on('hidden.bs.modal', function (e) {
            var dTable = $('#data_user_table').dataTable();
            dTable.fnClearTable();
            dTable.fnDestroy();

        });


        init_EasyPieChart();


    });


    function showsatker(ok) {
        $("#data_user_table tbody").append('<tr>\n' +
            '                        <td style="text-align:center">1</td>\n' +
            '                        <td>Satker B</td>\n' +
            '                        <td class="tdkanan">120</td>\n' +
            '                        <td class="tdkanan">160</td>\n' +
            '                        <td class="tdkanan">80</td>\n' +
            '                        <td class="tdkanan">40</td>\n' +
            '                        <td class="tdkanan">20</td>\n' +
            '                    </tr>\n' +
            '                    <tr>\n' +
            '                        <td style="text-align:center">2</td>\n' +
            '                        <td>Satker C</td>\n' +
            '                        <td class="tdkanan">120</td>\n' +
            '                        <td class="tdkanan">160</td>\n' +
            '                        <td class="tdkanan">80</td>\n' +
            '                        <td class="tdkanan">40</td>\n' +
            '                        <td class="tdkanan">20</td>\n' +
            '                    </tr>\n' +
            '                    <tr>\n' +
            '                        <td style="text-align:center">3</td>\n' +
            '                        <td>Satker D</td>\n' +
            '                        <td class="tdkanan">120</td>\n' +
            '                        <td class="tdkanan">160</td>\n' +
            '                        <td class="tdkanan">80</td>\n' +
            '                        <td class="tdkanan">40</td>\n' +
            '                        <td class="tdkanan">20</td>\n' +
            '                    </tr>\n' +
            '                    <tr>\n' +
            '                        <td style="text-align:center">4</td>\n' +
            '                        <td>Satker E</td>\n' +
            '                        <td class="tdkanan">120</td>\n' +
            '                        <td class="tdkanan">160</td>\n' +
            '                        <td class="tdkanan">80</td>\n' +
            '                        <td class="tdkanan">40</td>\n' +
            '                        <td class="tdkanan">20</td>\n' +
            '                    </tr>');

        $('#data_user_table').dataTable({
            pageLength: 100,
        });
    }




    function test(id) {
        var tahun = $("#tahun").val();
        window.location = "<?php echo base_url(); ?>dashboard/dashboard_detail?tahun=" + tahun + "&pro_id=" + id;
    }

    function kepeg() {
        var seriesusia = [{
            name: 'Pagu',
            data: [4000000, 1000000, 3000000]
        }];
        var catusia = ["Pagu", "Realsisasi", "Sisa Pagu"];

        ac_column_kepeg(seriesusia, 180, catusia, 'chart_ttl', 'bar');

        var seriesgol = [{
            name: 'Pagu',
            data: [1000000, 2000000, 1000000]
        }, {
            name: 'Realisasi',
            data: [400000, 800000, 400000]
        }, {
            name: 'Sisa',
            data: [600000, 1200000, 600000]
        }];
        var catgol = ["Pegawai", "Barang", "Modal"];
        ac_column_kepeg(seriesgol, 180, catgol, 'chart_jenis_belanja', 'bar');

        var seriespend = [{
            name: 'Pagu',
            data: [1000000, 2000000, 1000000]
        }, {
            name: 'Realisasi',
            data: [400000, 800000, 400000]
        }, {
            name: 'Sisa',
            data: [600000, 1200000, 600000]
        }];
        var catpend = ["Pusat", "Daerah", "Dekon"];
        ac_column_kepeg(seriespend, 180, catpend, 'chart_kewenangan', 'bar',);

        var seriessd = [{
            name: 'Pagu',
            data: [1000000, 2000000, 1000000, 100000]
        }, {
            name: 'Realisasi',
            data: [400000, 800000, 400000, 50000]
        }, {
            name: 'Sisa',
            data: [600000, 1200000, 600000, 50000]
        }];
        var catsd = ["Rupiah Murni", "BLU", "PNBP", "Hibah"];
        ac_column_kepeg(seriessd, 180, catsd, 'chart_sd', 'bar');

        var serieshdb = [{
            name: 'Lelang',
            data: [44, 55, 50, 60, 0, 0, 0, 0, 0, 0, 0, 0]
        }, {
            name: 'Pemenang',
            data: [34, 40, 40, 50, 0, 0, 0, 0, 0, 0, 0, 0]
        },
            //     {
            //     name: 'Absen',
            //     data: [3, 7,4,4,0,0,0, 0,0,0,0,0,]
            // }
        ];
        var cathdb = ["jan", "feb", "mar", "apr", "jun", "jul", "aug", "sep", "okt", "nov", "dec"];
        ac_column_kepeg(serieshdb, 180, cathdb, 'chart_lelang_bulan', 'bar',);

        var serieshdb = [{
            name: 'Pagu',
            data: [44, 55, 50, 60, 0, 0, 0, 0, 0, 0, 0, 0]
        }, {
            name: 'Realisasi',
            data: [34, 40, 40, 50, 0, 0, 0, 0, 0, 0, 0, 0]
        },
            //     {
            //     name: 'Absen',
            //     data: [3, 7,4,4,0,0,0, 0,0,0,0,0,]
            // }
        ];
        var cathdb = ["jan", "feb", "mar", "apr", "jun", "jul", "aug", "sep", "okt", "nov", "dec"];
        ac_column_kepeg(serieshdb, 180, cathdb, 'chart_kontrak', 'bar',);

        // var seriespro =[{
        //     name: 'Profesi',
        //     data: [44, 55,50,60, 70]
        // }];
        // var catpro=["Perawat", "Bidan", "Dr umum", "Dr sp","Pegawai"];
        // ac_column_kepeg(seriespro,180,catpro,'chart_profesi','bar','#b33130');


        // var serieshdb =[{
        //     name: 'Masuk',
        //     data: [44, 55,50,60, 0, 0,0, 0,0,0, 0, 0]
        // },{
        //     name: 'Ijin',
        //     data: [3, 8,4,6, 0, 0,0, 0,0,0, 0,0]
        // },{
        //     name: 'Absen',
        //     data: [3, 7,4,4,0,0,0, 0,0,0,0,0,]
        // }
        // ];
        // var cathdb=["jan", "feb", "mar", "apr","jun","jul","aug","sep","okt","nov","dec"];
        // ac_column_kepeg(serieshdb,180,cathdb,'chart_hdr_bulan','bar',);
        //
        // var serieshdb =[{
        //     name: 'Masuk',
        //     data: [44]
        // },{
        //     name: 'Ijin',
        //     data: [3]
        // },{
        //     name: 'Absen',
        //     data: [3]
        // }
        // ];
        // var cathdb=["Total"];
        // ac_column_kepeg(serieshdb,180,cathdb,'chart_hdr_total','bar',);
    }

</script>

<?php
$this->load->view('layout/footer');
?>
