<?php
$this->load->view('layout/header');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->
<script src="<?php echo base_url(); ?>assets/script/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/script/apex/apexcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/script/jquery.easypiechart.min.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/script/apex/styles.css" rel="stylesheet">
<style>

    .opd_jm {
        text-align: center;
        font-size: 16px;
        font-weight: bolder;
    }

    .info-box-icon {
        border-top-left-radius: 2px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 2px;
        display: block;
        float: left;
        height: 100px;
        width: 150px;
        text-align: center;
        font-size: 45px;
        line-height: 90px;
        background: rgba(0,0,0,0.4);
    }
    .info-box{
        min-height: 100px;
        margin-bottom: 20px;
        padding: 0px;
        color: white;
        -webkit-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
        -moz-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
        box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
    }
    .oye{
        padding-top: 20px;
    }
    .info-box i {
        display: block;
        height: 100px;
        font-size: 80px;
        line-height: 100px;
        width: 150px;
        float: left;
        text-align: center;
        margin: auto;
        /* margin-left: 20px; */
        /*padding-right: 5px;*/
        color: rgba(255, 255, 255, 1);
    }
    .judul{
        font-size: 40px;
        text-align: center;
        font-weight: bolder;
        margin-top: 40px;
        color: #0f0f0f;
        margin-bottom: 40px;
    }

    @media only screen and (max-width: 400px) {

        .opd_jm {
            text-align: center;
            font-size: 12px;
            font-weight: bolder;
        }

        .info-box-icon {
            border-top-left-radius: 2px;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 2px;
            display: block;
            float: left;
            height: 90px;
            width: 90px;
            text-align: center;
            font-size: 45px;
            line-height: 90px;
            background: rgba(0,0,0,0.4);
        }
        .info-box-2 {
            min-height: 90px;
            margin-bottom: 20px;
            padding: 0px;
            color: white;
            -webkit-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
            box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
        }
        .oye{
            padding-top: 20px;
        }
        .info-box-2 i {
            display: block;
            height: 90px;
            font-size: 70px;
            line-height: 90px;
            width: 90px;
            float: left;
            text-align: center;
            margin: auto;
            /* margin-left: 20px; */
            padding-right: 5px;
            color: rgba(255, 255, 255, 1);
        }
        .judul{
            font-size: 12px;
            text-align: center;
            font-weight: bolder;
        }


    }

</style>


<div class="page-title hidden">
    <div class="title_left">
        <h5>&nbsp;&nbsp;

        </h5>
    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
        <div class="judul">
            Selamat datang Administrator
        </div>
    </section>
</section>
<script>


</script>

<?php
$this->load->view('layout/footer');
?>
