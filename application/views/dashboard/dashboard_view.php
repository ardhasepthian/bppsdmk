<?php
$this->load->view('layout/header_dash');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->
<script src="<?php echo base_url(); ?>assets/script/moment.js"></script>
<script src="<?php echo base_url(); ?>assets/script/apex/apexcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/script/jquery.easypiechart.min.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/script/apex/styles.css" rel="stylesheet">
<style>

    .opd_jm {
        text-align: center;
        font-size: 16px;
        font-weight: bolder;
    }

    .info-box-icon {
        border-top-left-radius: 2px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 2px;
        display: block;
        float: left;
        height: 100px;
        width: 150px;
        text-align: center;
        font-size: 45px;
        line-height: 90px;
        background: rgba(0,0,0,0.4);
    }
    .info-box{
        min-height: 100px;
        margin-bottom: 20px;
        padding: 0px;
        color: white;
        -webkit-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
        -moz-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
        box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
    }
    .oye{
        padding-top: 20px;
    }
    .info-box i {
        display: block;
        height: 100px;
        font-size: 80px;
        line-height: 100px;
        width: 150px;
        float: left;
        text-align: center;
        margin: auto;
        /* margin-left: 20px; */
        /*padding-right: 5px;*/
        color: rgba(255, 255, 255, 1);
    }
    .judul{
        font-size: 40px;
        text-align: center;
        font-weight: bolder;
        margin-top: 40px;
        color: #0f0f0f;
        margin-bottom: 40px;
    }

    @media only screen and (max-width: 400px) {

        .opd_jm {
            text-align: center;
            font-size: 12px;
            font-weight: bolder;
        }

        .info-box-icon {
            border-top-left-radius: 2px;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 2px;
            display: block;
            float: left;
            height: 90px;
            width: 90px;
            text-align: center;
            font-size: 45px;
            line-height: 90px;
            background: rgba(0,0,0,0.4);
        }
        .info-box-2 {
            min-height: 90px;
            margin-bottom: 20px;
            padding: 0px;
            color: white;
            -webkit-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
            -moz-box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
            box-shadow: inset 0 0 1px 1px rgba(255, 255, 255, 0.35), 0 3px 1px -1px rgba(0, 0, 0, 0.1);
        }
        .oye{
            padding-top: 20px;
        }
        .info-box-2 i {
            display: block;
            height: 90px;
            font-size: 70px;
            line-height: 90px;
            width: 90px;
            float: left;
            text-align: center;
            margin: auto;
            /* margin-left: 20px; */
            padding-right: 5px;
            color: rgba(255, 255, 255, 1);
        }
        .judul{
            font-size: 12px;
            text-align: center;
            font-weight: bolder;
        }


    }

</style>


<div class="page-title hidden">
    <div class="title_left">
        <h5>&nbsp;&nbsp;

        </h5>
    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
        <div class="judul">
            Portal Dashboard Badan PPSDM Kesehatan
        </div>

        <div class="col-md-12 panel" style="margin: auto">
           <a href="<?php echo base_url()?>dashboard/profil"> <div class="col-xs-12 col-md-6 opd_jm ">
                <div class="info-box twitter-bg">
                    <div class="info-box-icon">
                        <i class="fa fa-pie-chart"></i>
                    </div>
                    <div class="count oye" >Profil</div>
                    <div class="title">Badan PPSDM Kesehatan</div>
                </div>
            </div>
        </a>
            <a href="<?php echo base_url()?>dashboard/keuangan"> <div class="col-xs-12 col-md-6 opd_jm ">
                <div class="info-box red-bg">
                    <div class="info-box-icon">
                        <i class="fa fa-bar-chart-o"></i>
                    </div>
                    <div class="count oye" >Kinerja Keuangan</div>
                    <div class="title"></div>
                </div>
            </div>
            </a>
            <a href="<?php echo base_url()?>dashboard/barjas">
                <div class="col-xs-12 col-md-6 opd_jm ">
                            <div class="info-box blue2-bg">
                                <div class="info-box-icon">
                                    <i class="fa fa-barcode"></i>
                                </div>
                                <div class="count oye" >Pengadaan Barang dan Jasa</div>
                                <div class="title"></div>
                            </div>
                </div>
            </a>

            <a href="<?php echo base_url()?>dashboard/program">
                <div class="col-xs-12 col-md-6 opd_jm ">
                <div class="info-box green-bg">
                    <div class="info-box-icon">
                        <i class="fa fa-line-chart"></i>
                    </div>
                    <div class="count oye" >Kinerja Program</div>
                    <div class="title"></div>
                </div>
            </div>
            </a>
            <a href="<?php echo base_url()?>dashboard/kepegawaian"><div class="col-xs-12 col-md-6 opd_jm ">
                <div class="info-box orange-bg">
                    <div class="info-box-icon">
                        <i class="fa fa-area-chart"></i>
                    </div>
                    <div class="count oye" >Kepegawaian</div>
                    <div class="title"></div>
                </div>
            </div>
            </a>


        </div>

<!--        <div class="col-md-12 panel">-->
<!--            <div class="col-md-3 col-xs-12  mhgh"  >-->
<!--                <div class="blue2-bg xx2 col-xs-6 col-md-12 opd_jm " id="targetall"> </div>-->
<!--                <div class="blue2-bg xx2 col-xs-6 col-md-12 opd_jm " id="realisasiall"></div>-->
<!--            </div>-->
<!--            <div class="col-md-3 "id="chart1" ></div>-->
<!--            <div class="col-md-6" id="chart2"></div>-->
<!--        </div>-->

        <div class="col-md-12" id="loc-wid" style="margin-top:15px;">

<!--            <div class="col-md-3   widget widget_tally_box grad_bluemod">-->
<!--                <div class="x_panel ui-ribbon-container fixed_height_390">-->
<!--                                       <div class="x_panel  fixed_height_390">-->
<!---->
<!--                    <div class="x_title">-->
<!--                        <h2 style="font-weight: bolder">Sidoarjo</h2><a href="#" onclick="test()"-->
<!--                                                                              class="btn btn-primary pull-right"-->
<!--                                                                              style="z-index: 999"><i-->
<!--                                    class="icon_zoom-in pull-right"></i></a>-->
<!--                        <div class="clearfix"></div>-->
<!--                    </div>-->
<!--                    <div class="x_content">-->
<!--                        <div style="text-align: center; margin-bottom: 7px">-->
<!--                        <span class="chart" data-percent="86">-->
<!--                        <span class="percent">86</span>-->
<!--                        <canvas height="220" width="220" style="height: 110px; width: 110px;"></canvas></span>-->
<!--                        </div>-->
<!--                        <h3 class="name_title" style="color: firebrick"><b>234,000,000,000</b></h3>-->
<!--                        <p>Target: 300,000,000,000</p>-->
<!--                        <div class="divider"></div>-->
<!--                        <div class="col-md-12 col-xs-12  xx blue-bg">-->
<!--                            <div class="opd_tit" style="min-height: 50px"><p class="pman">IMPLEMENTASI TENANT DASHBOARD AP1</p></div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-xs-3 xx twitter2-bg">-->
<!--                            <div class="opd_tit"><p class="pman">Status</p></div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-9 col-xs-9 xx red-bg">-->
<!--                            <div class="opd_tit"><p class="pman">KICK OFF</p></div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-xs-3 xx twitter2-bg">-->
<!--                            <div class="opd_tit"><p class="pman">Durasi</p></div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-9 col-xs-9 xx blue-bg">-->
<!--                            <div class="opd_tit"><p class="pman">100 Hari</p></div>-->
<!---->
<!--                        </div>-->
<!---->
<!--                        <div class="col-md-3 col-xs-3 xx twitter2-bg">-->
<!--                            <div class="opd_tit"><p class="pman">PIC</p></div>-->
<!---->
<!--                        </div>-->
<!--                        <div class="col-md-9 col-xs-9 xx blue-bg">-->
<!--                            <div class="opd_tit"><p class="pman">Henry,Latifu</p></div>-->
<!---->
<!--                        </div>-->
<!---->
<!---->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->





        </div>


    </section>
</section>
<script>


</script>

<?php
$this->load->view('layout/footer');
?>
