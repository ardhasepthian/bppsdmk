<?php
$this->load->view('layout/header_dash');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->

<style>

    .tdkanan {
        text-align: right;
    }

    .opd_tit {
        text-align: center;
        font-size: 12px;
    }

    .opd_jm2 {
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
    }

    .opd_tit2 {
        text-align: center;
        font-size: 14px;
    }

    @media only screen and (max-width: 600px) {

        .opd_tit {
            text-align: center;
            font-size: 10px;
        }
        /*.dchart{*/
        /*max-height: 150px;*/
        /*font-size: 10px;*/

        /*}*/
    }

    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 480px)
    and (-webkit-min-device-pixel-ratio: 2) {
        .width-cols {
            color: red;
            height: 100px;
            width: 50%;
        }
    }



    .mhgh {
        height: 250px;
        padding-top: 50px;

    }




    .grad_bluemod {
        background: #fdfdff; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Safari 5.1-6*/
        background: -o-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 49, 48)); /*Opera 11.1-12*/
        background: -moz-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Fx 3.6-15*/
        background: linear-gradient(to bottom, #fff, #b33b3f); /*Standard*/

        border: solid 2px white;
    }

    .xx {
        border-radius: 2px;
        height: 60px;
        padding: 10px;
        margin-bottom: 10px;
        margin-top: 10px;
        border: solid 1px white;

    }

    .oye {
        font-size: larger;
        text-align: right;
        font-weight: bolder;
    }

    .pman {
        margin: 1px;
        /*font-size: larger;*/
    }



    .panel-body {
       padding: 5px;
    }

</style>


<div class="page-title hidden">
    <div class="title_left">

    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
        <b><center><h3 style="margin-top: 1px;margin-bottom: 8px">&nbsp;
                    Monitoring Kinerja Keuangan
                </h3></center></b>
        <div class="col-md-12">
<!--            <h4 style="margin-top: 3px;margin-bottom: 3px">PAGU</h4>-->
            <div class="panel  col-md-6 col-xs-12 ">
                <div class="panel-heading ">Pagu
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('ttl')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
<!--                    <div class="col-md-6 col-xs-12" id="chart_ttl"></div>-->
                    <div class="col-md-7 col-xs-12" id="chart_ttlpie"></div>
                    <div class="col-md-5 col-xs-12">
                        <div class=" xx blue2-bg">
                            <div class="count oye" >1,000,000,000</div>
                            <div class="title">Pagu Harian</div>
                        </div>
                        <div class="xx biru-bg">
                            <div class="count oye" >1,000,000,000</div>
                            <div class="title">Realisasi</div>
                        </div>
                        <div class="xx ijo-bg">
                            <div class="count oye">1,000,000,000</div>
                            <div class="title">Sisa Pagu</div>
                        </div>
                    </div>
<!--                    <div class="col-md-3 col-xs-6">-->
<!--                        <span class="chart " style="margin-top: 40px;margin-left: 10px " id="donrealdp" data-percent="18">-->
<!--                        <span class="percent" id="donreal">40</span>-->
<!--                        <canvas height="220" width="220" style="height: 110px; width: 110px;"></canvas>-->
<!--                        </span>-->
<!--                        <h5 style="text-align: center">Realisasi</h5>-->
<!--                    </div>-->
<!--                    <div class="col-md-3 col-xs-6"> <span class="chart " id="donsisdp" style="margin-top: 40px;margin-left: 10px " data-percent="60">-->
<!--                        <span class="percent" id="donsis">60</span>-->
<!--                        <canvas height="220" width="220" style="height: 110px; width: 110px;"></canvas>-->
<!--                        </span>-->
<!--                        <h5 style="text-align: center">Sisa Pagu</h5>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="panel  col-md-6 col-xs-12 ">
                <div class="panel-heading">Jenis Belanja
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('jb')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_jenis_belanja"></div>
                </div>
            </div>
            <div class="panel  col-md-6 col-xs-12 ">
                <div class="panel-heading">Kewenangan
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('kwn')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_kewenangan"></div>
                </div>
            </div>
            <div class="panel  col-md-6 col-xs-12 ">
                <div class="panel-heading">Sumber Dana
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('sd')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_sd"></div>
                </div>
            </div>


        </div>
    </section>
</section>

<div id="modalsatker" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail</h4>
            </div>
            <div class="modal-body">
                <table id="data_user_table" class="table table-bordered responsive" width="100%" >
                <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>


<div id="modal_detail" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title titdet"> Detail</h4>
            </div>
            <div class="modal-body">
                <table id="table_detail" class="table table-bordered responsive" width="100%" >
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>




<script>



    $(document).ready(function () {


        //go();
        kepeg();
        $('#modalsatker').on('show.bs.modal', function (e)
        {
            $('#data_user_table').css('width', '');
        });
        $('#modalsatker').on('hidden.bs.modal', function (e)
        {
            var dTable = $('#data_user_table').dataTable();
            dTable.fnClearTable();
            dTable.fnDestroy();

        });

        $('#modal_detail').on('show.bs.modal', function (e)
        {
            $('#table_detail').css('width', '');
        });
        $('#modal_detail').on('hidden.bs.modal', function (e)
        {
            var dTable = $('#table_detail').dataTable();
            dTable.fnClearTable();
            dTable.fnDestroy();

        });







    });



    function showsatker(jchart) {
        if(jchart=='jb'){
            var tbhead='<tr><th rowspan="2">No</th>' +
                '<th rowspan="2">Satker</th>' +
                '<th colspan="2">Pegawai</th>' +
                '<th colspan="2">Barang</th>' +
                '<th colspan="2">Modal</th>' +
                '<th colspan="2">Bansos</th>' +
                '<th colspan="2">Hibah</th>' +
                '<th colspan="2">Transfer</th>' +
                '<th colspan="2">Subsidi</th></tr>' +
                '<tr><th>Pagu Harian</th><th>Realisasi</th>' +
                '<th>Pagu Harian</th><th>Realisasi</th>' +
                '<th>Pagu Harian</th><th>Realisasi</th>' +
                '<th>Pagu Harian</th><th>Realisasi</th>' +
                '<th>Pagu Harian</th><th>Realisasi</th>' +
                '<th>Pagu Harian</th><th>Realisasi</th>' +
                '<th>Pagu Harian</th><th>Realisasi</th></tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#data_user_table tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td></tr>');
            }
        }else if(jchart=='sd') {                             //======================== chart lain setelah data ada

            var tbhead='<tr><th >No</th>' +
                '<th >Sumber Dana</th>' +
                '<th>Pagu Harian</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa Pagu</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);
            $("#data_user_table tbody").html('<tr> <td>1</td>' +
                '<td>Rupiah Murni</td>' +
                '<td class="tdkanan"> 2,940,442,420,000</td><td class="tdkanan"> 798,585,614,944 </td>' +
                '<td class="tdkanan"> 2,141,856,805,056 </td></tr>' +
                ' <tr> <td>2</td>' +
                '<td>PNBP</td>' +
                '<td class="tdkanan"> 222,990,106,000</td><td class="tdkanan">30,194,765,160</td>' +
                '<td class="tdkanan">  192,795,340,840  </td></tr>' +
                '<tr> <td>3</td>' +
                '<td>BLU</td>' +
                '<td class="tdkanan"> 65,055,221,8000 </td><td class="tdkanan"> 61,843,475,797  </td>' +
                '<td class="tdkanan"> 588,708,742,203 </td></tr>');

        }else {                             //======================== chart lain setelah data ada
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Pagu Harian</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa Pagu</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#data_user_table tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>'+(10000 * i)+'</td><td>'+(5000 * i)+'</td>' +
                    '<td>'+(5000 * i)+'</td></tr>');
            }
        }




        $('#data_user_table').dataTable({
            pageLength:100,
        });
    }

    function kepeg() {

        $.ajax({
            url: '<?php echo base_url(); ?>dashboard/get_dashboard_keuangan',
            type: 'POST',
            success: function (response) {
                var respon = JSON.parse(response);
                console.log(respon);
                //==============================================PAGU
                    var pagutot =respon.total;
                var jb =respon.jenis_belanja;
                var kw =respon.kewenangan;
                    var catpagutot=["Pagu Harian","Realsisasi","Sisa Pagu" ];
                    var idcatpagutot=["Pagu Harian","Realsisasi","Sisa Pagu" ];    
                    var seriespagutot=[];
                    var data =[];
                    data.push(parseInt(pagutot[0].tot_pagu));
                    data.push(parseInt(pagutot[0].tot_realisasi));
                    data.push(parseInt(pagutot[0].tot_sisa));
                    var name="Jumlah";
                        seriespagutot.push({data:data,name:name});


                // ac_column_keu(seriespagutot,230,catpagutot,'chart_ttl','bar',undefined,idcatpagutot);

                $("#donrealdp").attr("data-percent",pagutot[0].persen_pagu);
                $("#donreal").html(pagutot[0].persen_pagu);
                $("#donsisdp").attr("data-percent",pagutot[0].persen_sisa);
                $("#donsis").html(pagutot[0].persen_sisa);

                //==============================================JB
                var series_jb=[];
                var data_pagu=[];
                var data_realisasi=[];
                var data_sisa=[];
                var catjb=[];
                var idcatjb=[];
                var name=["Pagu Harian","Realisasi","Sisa Pagu"];
                for(var i in jb){
                        catjb.push(jb[i].jb);
                        idcatjb.push(jb[i].jb);
                    data_pagu.push(parseInt(jb[i].bp_pagu));
                    data_realisasi.push(parseInt(jb[i].bp_realisasi));
                    data_sisa.push(parseInt(jb[i].bp_sisa));
                    }
                    series_jb.push({data:data_pagu,name:name[0]})
                series_jb.push({data:data_realisasi,name:name[1]})
                series_jb.push({data:data_sisa,name:name[2]})
                ac_column_keu(series_jb,230,catjb,'chart_jenis_belanja','bar',undefined,idcatjb);

                //==============================================Kewenangan
                var series_kw=[];
                var data_pagu=[];
                var data_realisasi=[];
                var data_sisa=[];
                var catkw=[];
                var idcatkw=[];
                var name=["Pagu Harian","Realisasi","Sisa Pagu"];
                for(var i in kw){
                    catkw.push(kw[i].kewenangan);
                    idcatkw.push(kw[i].kewenangan);
                    data_pagu.push(parseInt(kw[i].tot_pagu));
                    data_realisasi.push(parseInt(kw[i].tot_realisasi));
                    data_sisa.push(parseInt(kw[i].tot_sisa));
                }
                series_kw.push({data:data_pagu,name:name[0]})
                series_kw.push({data:data_realisasi,name:name[1]})
                series_kw.push({data:data_sisa,name:name[2]})
                ac_column_keu(series_kw,230,catkw,'chart_kewenangan','bar',undefined,idcatkw);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });


        var seriessd =[{
            name: 'Pagu Harian',
            data: [2940442420000,650552218000, 222990106000 ]
        },{
            name: 'Realisasi',
            data: [798585614944,61843475797,30194765160]
        },{
            name: 'Sisa Pagu',
            data: [2141856805056,588708742203,192795340840]
        }];
        var catsd=["Rupiah Murni", "BLU","PNBP"];
        var idcatsd=["RM", "BLU","PNBP"];
        ac_column_keu(seriessd,230,catsd,'chart_sd','bar',undefined,idcatsd);



    }


    function ac_column_keu(seri,hei,categ,idchart,types,colr,idcat) {


        if(colr == undefined){

            colr =['#2db327','#ff8943','#6857ff','#3ebaff','#ff1c28','#8f3270',]
        }else{
            colr =[colr]
        }

        Highcharts.setOptions({
            lang: {
                thousandsSep: ','
            }
        });
        Highcharts.chart(idchart, {
            chart: {
                type: 'column',
                height:(hei +20),
                // events: {
                //     click: function () {
                //         console.log(event);
                //         //show_modal_detail(idcat[this.x],idchart,categ[this.x]);
                //
                //     }
                // }
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 10,
                    depth: 50,
                    viewDistance: 25
                }
            },
            colors:colr,
            title:null,

            xAxis: {


                categories:categ,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah'
                },
                labels: {
                    formatter: function() {
                        var ret,
                            numericSymbols = ['Rb', 'Jt', 'M', 'T', 'P', 'E'],
                            i = 6;
                        if(this.value >=1000) {
                            while (i-- && ret === undefined) {
                                multi = Math.pow(1000, i + 1);
                                if (this.value >= multi && numericSymbols[i] !== null) {
                                    ret = (this.value / multi) + numericSymbols[i];
                                }
                            }
                        }
                        return  (ret ? ret : this.value);
                    }
                },
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0; text-align: right; "><b>{point.y:,.f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            plotOptions: {

                column: {
                    pointPadding: 0.1,
                    borderWidth: 0
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                // alert(categ[this.x]);
                                show_modal_detail(idcat[this.x],idchart,categ[this.x]);

                            }
                        }
                    }
                },

            },
            series: seri,
            legend: {
                itemStyle: {
                    color: '#000',
                    fontWeight: 'lighter',
                    borderWidth:1,
                }
            },

        });
    }
    function show_modal_detail(id,charts,serie) {
        $("#table_detail thead").html('');
         $("#table_detail tbody").html('');
        $("#modal_detail").modal();


        if(charts =='chart_ttl'){ //==============================DETAIL PAGU
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard/get_dashboard_keu_det_pagu',
                type: 'POST',
                data:{
                    id:id
                },
                success: function (response) {
                    var respon = JSON.parse(response);
                    console.log(respon);

                    var titdet = 'PAGU';
                    var tbhead='<tr><th >No</th>' +
                        '<th >Satker</th>' +
                        '<th>Pagu Harian</th>' +
                        '<th>Realisasi</th>' +
                        '<th>Sisa Pagu</th>' +
                        '</tr>';

                    $("#table_detail thead").html(tbhead);

                    for(var i in respon){
                        $("#table_detail tbody").append('<tr> <td>'+i+'</td> <td>'+respon[i].nm_satker+'</>' +
                            '<td class="tdkanan">'+number_format(respon[i].tot_pagu)+'</td>' +
                            '<td class="tdkanan">'+number_format(respon[i].tot_realisasi)+'</td><td class="tdkanan">'+number_format(respon[i].tot_sisa)+'</td>' +
                            '</tr>');
                    }
                    $('#table_detail').dataTable({
                        pageLength:100
                    });
                }
            });





        }else if(charts =='chart_jenis_belanja'){ //==============================JENIS BELANJA

            $.ajax({
                url: '<?php echo base_url(); ?>dashboard/get_dashboard_keu_det_jb',
                type: 'POST',
                data:{
                    id:id
                },
                success: function (response) {
                    var respon = JSON.parse(response);
                    console.log(respon);

                    var titdet = 'Belanja'+serie;
                    var tbhead='<tr><th rowspan="2">No</th>' +
                        '<th rowspan="2">Satker</th>' +
                        '<th colspan="3">'+serie+'</th></tr>' +
                        '<tr><th>Pagu Harian</th><th>Realisasi</th><th>Sisa Pagu</th></tr>';

                    $("#table_detail thead").html(tbhead);

                    for(var i in respon){
                        $("#table_detail tbody").append('<tr> <td>'+i+'</td>' +
                            '<td>'+respon[i].nm_satker+'</td>' +
                            '<td class="tdkanan">'+number_format(respon[i].pagu)+'</td><td class="tdkanan">'+number_format(respon[i].realisasi)+'</td><td class="tdkanan">'+number_format(respon[i].sisa)+'</td></tr>');
                    }
                    $('#table_detail').dataTable({
                        pageLength:100
                    });
                    $(".titdet").html("Detail " + titdet);
                }
            });


        }else if(charts =='chart_kewenangan'){//==============================DETAIL KEWENANGAN
            $.ajax({
                url: '<?php echo base_url(); ?>dashboard/get_dashboard_keu_det_kw',
                type: 'POST',
                data:{
                    id:id
                },
                success: function (response) {
                    var respon = JSON.parse(response);
                    console.log(respon);
                    var titdet = ' Kewenangan '+serie;
                    var tbhead='<tr><th >No</th>' +
                        '<th >Satker</th>' +
                        '<th>Pagu Harian</th>' +
                        '<th>Realisasi</th>' +
                        '<th>Sisa Pagu</th>' +
                        '</tr>';

                    $("#table_detail thead").html(tbhead);

                    for(var i in respon){
                        $("#table_detail tbody").append('<tr> <td>'+i+'</td>' +
                            '<td>'+respon[i].nm_satker+'</td>' +
                            '<td class="tdkanan">'+number_format(respon[i].tot_pagu)+'</td><td class="tdkanan">'+number_format(respon[i].tot_realisasi)+'</td><td class="tdkanan">'+number_format(respon[i].tot_sisa)+'</td></tr>');
                    }

                    $(".titdet").html("Detail " + titdet);
                    $('#table_detail').dataTable({
                        pageLength:100
                    });
                }
            });



        }else if(charts =='chart_sd') {//==============================DETAIL SUMBER DANA
            var titdet = ' Sumber Dana ' + serie;
            var tbhead = '<tr><th >No</th>' +
                '<th >Sumber Dana</th>' +
                '<th>Pagu Harian</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa Pagu</th>' +
                '</tr>';

            $("#table_detail thead").html(tbhead);
            $("#table_detail tbody").html('<tr> <td>1</td>' +
                '<td>Rupiah Murni</td>' +
                '<td class="tdkanan"> 2,940,442,420,000</td><td class="tdkanan"> 798,585,614,944 </td>' +
                '<td class="tdkanan"> 2,141,856,805,056 </td></tr>' +
                ' <tr> <td>2</td>' +
                '<td>PNBP</td>' +
                '<td class="tdkanan"> 222,990,106,000</td><td class="tdkanan">30,194,765,160</td>' +
                '<td class="tdkanan">  192,795,340,840  </td></tr>' +
                '<tr> <td>3</td>' +
                '<td>BLU</td>' +
                '<td class="tdkanan"> 65,055,221,8000 </td><td class="tdkanan"> 61,843,475,797  </td>' +
                '<td class="tdkanan"> 588,708,742,203 </td></tr>');


            $(".titdet").html("Detail " + titdet);
            $('#table_detail').dataTable({
                pageLength:100
            });
        }

     }






    Highcharts.setOptions({
        colors: ['#058DC7', '#50B432']
    });
    Highcharts.chart('chart_ttlpie', {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            },height:250
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: 5,
                    color:'white'
                },showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: [
                ['Realisasi', 45.0],
                {
                    name: 'Sisa Pagu',
                    y: 55.0,
                    sliced: true,
                    selected: true
                },

            ]
        }]
    });
</script>

<?php
$this->load->view('layout/footer');
?>
