<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Distributor
  | Date       : 02-05-2017
  =========================================================  -->

<?php
$this->load->view('layout/header_dash');
?>
<script>

    function daftar_pos() {
        $.ajax({
            url: '<?php echo base_url(); ?>pos/daftar_pos',
            type: 'POST',
            data: {nopd: $('#nopd').val()},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function add_pos() {
        $.ajax({
            url: '<?php echo base_url(); ?>pos/add_data_pos',
            type: 'POST',
            data: {nopd:$('#nopd').val()},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function edit_pos(pos_id) {
        $.ajax({
            url: '<?php echo base_url(); ?>pos/edit_data',
            type: 'POST',
            data: {pos_id: pos_id , nopd:$('#nopd').val()},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function delete_pos(pos_id, pos_name) {
        var cek = confirm('Apakah anda yakin ingin menghapus pos ?');
        if (cek) {
            $.post("<?php echo base_url(); ?>pos/delete_data", {pos_id: pos_id}, function (data) {
                if (data !== '') {
                    $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                    $('#msg').fadeOut(5000);
                    return;
                }
                daftar_pos();
                // window.location = "<?php //echo base_url(); ?>pos";
            });
        }
    }
    $(document).ready(function () {

        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);
        var table;
        table = $('#outlet_table').dataTable(
            {   'oLanguage': {'sSearch': 'Pencarian',
                    "oPaginate": {
                        "sPrevious": "Sebelum",
                        "sNext": "Lanjut",
                        "sLast": "<<",
                        "sFirst": ">>"
                    }
                },
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url(); ?>pos/get_data_paging_detail",
                    "data":{nopd:"<?php //echo $nopd; ?>"},
                    "type": "GET"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        "targets": [0], //first column / numbering column
                        "orderable": false //set not orderablen
                    }
                ]
            }
        );
    });
    function cancel() {
        window.location = "<?php echo base_url(); ?>dashboard";
    }

    $('#outlet_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#outlet_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

</script>
<style>
    #outlet_table .dropdown-menu {
        position: relative;
        width: 100%;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> Agkasa Pura 1 <?php //echo $tittle; ?></h4>

            </div>
                        <div class="col-sm-2" id="buttonadd"><button onclick="javascript:backtoheader()" class="btn btn-primary btn-xs button-right">Back</button></div>
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
<div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
    <div id="msgidx" style="display:none">
        <?php
        $error = $this->session->flashdata('error');
        $info = $this->session->flashdata('info');
        if (!empty($error)):
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b> <?php echo $error; ?>
            </div>
        <?php
        endif;
        if (!empty($info)):
            ?>
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Alert!</b> <?php echo $info; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="panel">
        <div class="panel-body">


            <form id="form" data-parsley-validate class="form-horizontal form-label-left">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Proyek
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" readonly maxlength="255"id="opd_name" name="opd_name" value="<?php //echo $form_opd['nopd'] ;?>"  class="form-control col-md-7 col-xs-12 "/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Durasi Proyek
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" readonly maxlength="255"id="opd_name" name="opd_name" value="<?php //echo $form_opd['nopd'] ;?>"  class="form-control col-md-7 col-xs-12 "/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status Proyek
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" readonly maxlength="255"id="opd_name" name="opd_name" value="<?php //echo $form_opd['nopd'] ;?>"  class="form-control col-md-7 col-xs-12 "/>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal mulai
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" readonly id="opd_address" maxlength="255" name="opd_address" value="<?php //echo $form_opd['opd_name'] ;?>" class="form-control col-md-7 col-xs-12"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal selesai
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" readonly id="opd_address" maxlength="255" name="opd_address" value="<?php //echo $form_opd['opd_name'] ;?>" class="form-control col-md-7 col-xs-12"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">PIC Proyek
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" readonly id="opd_address" maxlength="255" name="opd_address" value="<?php //echo $form_opd['opd_name'] ;?>" class="form-control col-md-7 col-xs-12"/>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
    <div class="panel">
<!--        <header class="panel-heading" style="padding-top: 5px;">-->
<!--            <h4>--><?php ////echo $tittle; ?>
<!--                <button onclick="add_pos()" class="btn btn-xs btn-primary button-right"> Tambah </button> <button onclick="cancel()" class="btn btn-xs btn-success button-right"> Kembali </button>-->
<!--            </h4>-->
<!---->
<!--        </header>-->
        <div class="panel-body">
            <input id="nopd" type="hidden" value="<?php //echo $nopd;?>">

            <div class="col-sm-12" style="border-bottom: #cacaca solid 1px;">
                &nbsp;
            </div>
            <!--                <div class="col-sm-12" style="border-bottom: #cacaca solid 1px;"></div>-->

            <table id="outlet_table" class="table table-bordered table-responsive responsive">
                <thead>
                <tr>
                    <th style="text-align:center">No</th>
                    <th>Nama WBS</th>
                    <th>Durasi</th>
                    <th>Mulai</th>
                    <th>Selesai</th>
                    <th>PIC</th>
                    <th>Status</th>
                    <th style="text-align:center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
        </div>





