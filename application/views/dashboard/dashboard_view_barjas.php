    <?php
$this->load->view('layout/header_dash');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->

<style>

    .tdkanan {
        text-align: right;
    }

    .opd_tit {
        text-align: center;
        font-size: 12px;
    }

    .opd_jm2 {
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
    }

    .opd_tit2 {
        text-align: center;
        font-size: 14px;
    }

    @media only screen and (max-width: 600px) {

        .opd_tit {
            text-align: center;
            font-size: 10px;
        }
        /*.dchart{*/
        /*max-height: 150px;*/
        /*font-size: 10px;*/

        /*}*/
    }

    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 480px)
    and (-webkit-min-device-pixel-ratio: 2) {
        .width-cols {
            color: red;
            height: 100px;
            width: 50%;
        }
    }



    .mhgh {
        height: 250px;
        padding-top: 50px;

    }




    .grad_bluemod {
        background: #fdfdff; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Safari 5.1-6*/
        background: -o-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 49, 48)); /*Opera 11.1-12*/
        background: -moz-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Fx 3.6-15*/
        background: linear-gradient(to bottom, #fff, #b33b3f); /*Standard*/

        border: solid 2px white;
    }

    .xx {
        border-radius: 2px;
        /*height: 65px;*/
        padding: 1px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .xx2 {
        border-radius: 5px;
        height: 65px;
        padding: 5px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .pman {
        margin: 1px;
        /*font-size: larger;*/
    }



    .panel-body {
       padding: 5px;
    }

</style>


<div class="page-title hidden">
    <div class="title_left">

    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
        <b><center><h3 style="margin-top: 1px;margin-bottom: 8px">&nbsp;
                    Monitoring Pengadaan Barang dan jasa
                </h3></center></b>
        <div class="col-md-12">
<!--            <h4 style="margin-top: 3px;margin-bottom: 3px">PAGU</h4>-->
            <div class="panel  col-md-4 col-xs-12 ">
                <div class="panel-heading "> TOTAL
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('ttl')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px;text-align: center;padding: 30px">
                    <div class=" col-xs-12 twitter-bg" style="padding: 5px;margin-top: 10px" >
                      <h4>Jumlah Pengadaan</h4>
                        <h3 style="font-weight: bolder">300</h3>
                    </div>
                    <div class=" col-xs-12 red-bg" style="padding: 5px;margin-top: 10px">
                        <h4>Jumlah Pagu</h4>
                        <h3 style="font-weight: bolder">167,000,000,000</h3>
                    </div>

                </div>
            </div>
            <div class="panel  col-md-4 col-xs-12 ">
                <div class="panel-heading">Jumlah Paket Pengadaan
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('jml')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_jenis_belanja"></div>
                </div>
            </div>
            <div class="panel  col-md-4 col-xs-12 ">
                <div class="panel-heading">Nilai Pengadaan
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('nom')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_kewenangan"></div>
                </div>
            </div>

            <div class="panel  col-md-12 col-xs-12 ">
                <div class="panel-heading">Rincian Per Satker
                    <div style="    float: right;
    margin-right: 15px;
    height: 40px;
    font-size: 14px;">
                        <select name="pilsat" id="pilsat">
                            <option value="PUSDIK SDM KESEHATAN">PUSDIK SDM KESEHATAN</option>
                            <option value="SEKRETARIAT KONSIL KTKI">SEKRETARIAT KONSIL KTKI</option>
                            <option value="PUSLAT SDM KESEHATAN">PUSLAT SDM KESEHATAN</option>
                            <option value="PUSRENGUN SDM KESEHATAN">PUSRENGUN SDM KESEHATAN</option>
                            <option value="SEKRETARIAT BADAN PPSDM KES">SEKRETARIAT BADAN PPSDM KES</option>

                        </select>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <table id="data_table" class="table table-bordered responsive" width="100%" >
                        <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                            <th>Satker</th>
                            <th>Nama Pengadaan</th>
                            <th>No Kontrak</th>
                            <th>Pagu</th>
                            <th>Hps</th>
                            <th>Kontrak</th>
                            <th>Realisasi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="text-align:center">1</td>
                            <td>PUSDIK SDM KESEHATAN</td>
                            <td class="">Pengadaan Mobil dinas</td>
                            <td class="">11/SPK/11</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">390,000,000</td>
                            <td class="tdkanan">390,000,000</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">2</td>
                            <td>PUSDIK SDM KESEHATAN</td>
                            <td class="">Pengadaan alat peraga</td>
                            <td class="">11/SPK/12</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">390,000,000</td>
                            <td class="tdkanan">390,000,000</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">3</td>
                            <td>PUSDIK SDM KESEHATAN</td>
                            <td class="">Pengadaan Meja dan kursi</td>
                            <td class="">11/SPK/13</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">390,000,000</td>
                            <td class="tdkanan">0</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">4</td>
                            <td>PUSDIK SDM KESEHATAN</td>
                            <td class="">Pengadaan Peralatan Kantor</td>
                            <td class="">11/SPK/14</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">400,000,000</td>
                            <td class="tdkanan">390,000,000</td>
                            <td class="tdkanan">0</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">5</td>
                            <td>PUSDIK SDM KESEHATAN</td>
                            <td class="">Pengadaan Pengaman jaringan</td>
                            <td class="">-</td>
                            <td class="tdkanan">300,000,000</td>
                            <td class="tdkanan">300,000,000</td>
                            <td class="tdkanan">0</td>
                            <td class="tdkanan">0</td>
                        </tr>
                        <tr>
                            <td style="text-align:center">6</td>
                            <td>PUSDIK SDM KESEHATAN</td>
                            <td class="">Pengadaan Aplikasi Dokumen Managemen</td>
                            <td class="">-</td>
                            <td class="tdkanan">600,000,000</td>
                            <td class="tdkanan">600,000,000</td>
                            <td class="tdkanan">0</td>
                            <td class="tdkanan">0</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>



        </div>
    </section>
</section>

<div id="modalsatker" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail</h4>
            </div>
            <div class="modal-body">
                <table id="data_user_table" class="table table-bordered responsive" width="100%" >
                    <thead>
                    <tr>
                        <th style="text-align:center">No</th>
                        <th>Satker</th>
                        <th>20-30</th>
                        <th>30-35</th>
                        <th>35-40</th>
                        <th>40-45</th>
                        <th> >45 </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>


<div id="modal_detail" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title titdet"></h4>
            </div>
            <div class="modal-body">
                <table id="table_detail" class="table table-bordered responsive" width="100%" >
                    <thead>
                    <tr>
                        <th style="text-align:center">No</th>
                        <th>Satker</th>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>




<script>



    $(document).ready(function () {


        $("#data_table").dataTable();
        kepeg();
        $('#modalsatker').on('show.bs.modal', function (e)
        {
            $('#data_user_table').css('width', '');
        });
        $('#modalsatker').on('hidden.bs.modal', function (e)
        {
            var dTable = $('#data_user_table').dataTable();
            dTable.fnClearTable();
            dTable.fnDestroy();

        });


        init_EasyPieChart();



    });



    function showsatker(jchart) {
        if(jchart=='jb'){
            var tbhead='<tr><th rowspan="2">No</th>' +
                '<th rowspan="2">Satker</th>' +
                '<th colspan="2">Pegawai</th>' +
                '<th colspan="2">Barang</th>' +
                '<th colspan="2">Modal</th>' +
                '<th colspan="2">Bansos</th>' +
                '<th colspan="2">Hibah</th>' +
                '<th colspan="2">Transfer</th>' +
                '<th colspan="2">Subsidi</th></tr>' +
                '<tr><th>Pagu</th><th>Realisasi</th>' +
                '<th>Pagu</th><th>Realisasi</th>' +
                '<th>Pagu</th><th>Realisasi</th>' +
                '<th>Pagu</th><th>Realisasi</th>' +
                '<th>Pagu</th><th>Realisasi</th>' +
                '<th>Pagu</th><th>Realisasi</th>' +
                '<th>Pagu</th><th>Realisasi</th></tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#data_user_table tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td>' +
                    '<td>1</td><td>1</td></tr>');
            }
        }else {                             //======================== chart lain setelah data ada
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Pagu</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#data_user_table tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>'+(10000 * i)+'</td><td>'+(5000 * i)+'</td>' +
                    '<td>'+(5000 * i)+'</td></tr>');
            }
        }



        $('#data_user_table').dataTable({
            pageLength:100,
        });
    }

    function go() {

        tahun = $("#tahun").val();


        $.ajax({
            url: '<?php echo base_url(); ?>dashboard/get_dashboard',
            type: 'POST',
            data:{
                tahun : tahun
            },
            success: function (response) {
                var respon = JSON.parse(response);
                console.log(respon);
                var tr = respon.tr;

                var lspro = respon.lspro;

                var lsprotop = respon.lsprotop;

                if(lspro == 0){

                    $("#chart2").html('');
                    $("#chart1").html('');
                    $("#targetall").html('Data Kosong');
                    $("#realisasiall").html('Data kosong');
                    $("#loc-wid").html('');
                }else{

                    var persen =tr[0].persen;
                    var catpro=[];
                    var datapro=[];

                    for (var i in lsprotop){
                        catpro.push(lsprotop[i].pro_client);
                        datapro.push(lsprotop[i].pro_real_val);
                    }
                    $("#targetall").html('Target <br/>'+number_format(tr[0].target));
                    $("#realisasiall").html('Realisasi <br/>'+number_format(tr[0].realisasi));
                    //chart persen ====================================chart persen ====================================chart persen ====================================
                    var options = {
                        chart: {
                            height: 280,
                            type: "radialBar",
                        },

                        series: [persen],
                        colors: ["#06f103"],
                        plotOptions: {
                            radialBar: {
                                hollow: {
                                    margin: 0,
                                    size: "70%",
                                    background: "#293450"
                                },
                                track: {
                                    dropShadow: {
                                        enabled: true,
                                        top: 2,
                                        left: 0,
                                        blur: 4,
                                        opacity: 0.15
                                    }
                                },
                                dataLabels: {
                                    name: {
                                        offsetY: -10,
                                        color: "#fff",
                                        fontSize: "13px"
                                    },
                                    value: {
                                        color: "#fff",
                                        fontSize: "30px",
                                        show: true
                                    }
                                }
                            }
                        },
                        fill: {
                            type: "gradient",
                            gradient: {
                                shade: "dark",
                                type: "vertical",
                                gradientToColors: ["#ff0000"],
                                stops: [0, 100]
                            }
                        },
                        stroke: {
                            lineCap: "round"
                        },
                        labels: ["Realisasi "]
                    };

                    var chart = new ApexCharts(document.querySelector("#chart1"), options);

                    chart.render();

                    // chart top 5 ====================================chart top 5 ====================================chart top 5 ====================================
                    var options = {
                        series: [{
                            // data: [400, 430, 448, 470, 540],
                            data:datapro,

                        }],
                        chart: {
                            type: 'bar',
                            height: 250
                        },
                        color:['#06f103'],
                        title: {
                            text: 'Top 5 Project',
                            align: 'center',
                            floating: true
                        },
                        // subtitle: {
                        //     text: 'Category Names as DataLabels inside bars',
                        //     align: 'center',
                        // },
                        plotOptions: {
                            bar: {
                                horizontal: true,
                            }
                        },
                        fill: {
                            type: "gradient",
                            gradient: {
                                shade: "dark",
                                type: "horizontal",
                                gradientToColors: ["#ff0000"],
                                stops: [0, 100]
                            }
                        },
                        dataLabels: {
                            enabled: true
                        },
                        xaxis: {
                            // categories: ['Angkasa Pura 1', 'Sidoarjo', 'Enseval', 'Banyuwangi', 'Italy'
                            categories:catpro,
                            //],
                        }
                    };

                    var chart = new ApexCharts(document.querySelector("#chart2"), options);
                    chart.render();
                    // lIST PRO ====================================
                    $("#loc-wid").html('');
                    for(var i in lspro){
                        $("#loc-wid").append('<div class="col-md-3   widget widget_tally_box grad_bluemod">\n' +
                            '                <div class="x_panel ui-ribbon-container fixed_height_390">\n' +
                            '                    <!--                    <div class="x_panel  fixed_height_390">-->\n' +
                            '\n' +
                            '                    <div class="x_title">\n' +
                            '                        <h2 style="font-weight: bolder">'+lspro[i].pro_client+'</h2><a href="#" onclick="test('+lspro[i].pro_id+')"\n' +
                            '                                                                     class="btn btn-primary pull-right"\n' +
                            '                                                                     style="z-index: 999"><i\n' +
                            '                                    class="icon_zoom-in pull-right"></i></a>\n' +
                            '                        <div class="clearfix"></div>\n' +
                            '                    </div>\n' +
                            '                    <div class="x_content">\n' +
                            '                        <div style="text-align: center; margin-bottom: 7px">\n' +
                            '                        <span class="chart" data-percent="'+lspro[i].persen+'">\n' +
                            '                        <span class="percent">'+lspro[i].persen+'</span>\n' +
                            '                        <canvas height="220" width="220" style="height: 110px; width: 110px;"></canvas></span>\n' +
                            '                        </div>\n' +
                            '                        <h3 class="name_title" style="color: firebrick"><b>'+number_format(lspro[i].pro_real_val)+'</b></h3>\n' +
                            '                        <p>Target: '+number_format(lspro[i].pro_target_val)+'</p>\n' +
                            '                        <div class="divider"></div>\n' +
                            '                        <div class="col-md-12 col-xs-12  xx blue-bg">\n' +
                            '                            <div class="opd_tit" style="min-height: 50px"><p class="pman">'+lspro[i].pro_name+'</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '                        <div class="col-md-3 col-xs-3 xx twitter2-bg">\n' +
                            '                            <div class="opd_tit"><p class="pman">Status</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '                        <div class="col-md-9 col-xs-9 xx red-bg">\n' +
                            '                            <div class="opd_tit"><p class="pman">'+lspro[i].pro_status+'</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '                        <div class="col-md-3 col-xs-3 xx twitter2-bg">\n' +
                            '                            <div class="opd_tit"><p class="pman">Durasi</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '                        <div class="col-md-9 col-xs-9 xx blue-bg">\n' +
                            '                            <div class="opd_tit"><p class="pman">'+lspro[i].durasi+'</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                        <div class="col-md-3 col-xs-3 xx twitter2-bg">\n' +
                            '                            <div class="opd_tit"><p class="pman">PIC</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '                        <div class="col-md-9 col-xs-9 xx blue-bg">\n' +
                            '                            <div class="opd_tit"><p class="pman">'+lspro[i].pic+'</p></div>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '            </div>');

                    }
                    init_EasyPieChart();



                }







            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });







    }



    function test(id){
        var tahun = $("#tahun").val();
        window.location = "<?php echo base_url(); ?>dashboard/dashboard_detail?tahun="+tahun+"&pro_id="+id;
    }

    function kepeg() {
        var seriesgol =[{
            name: 'Proses tender',
            data: [150]
            },{
                name: 'Pemenang berkontrak',
                data: [150,]
            },{
                name: 'Realisasi pekerjaan',
                data: [90,]
        }];
        var catgol=["Jumlah"];
        var idcatgol=[51, ];
        ac_column_keu(seriesgol,230,catgol,'chart_jenis_belanja','bar',undefined,idcatgol);

        var seriespend =[{
                name: 'Pagu',
                data: [300000000000,]
            },{
                name: 'HPS',
                data: [140000000000]
            },{
            name: 'Nilai Kontrak',
            data: [138000000000]
        },{
            name: 'Realisasi Pembayaran',
            data: [80000000000]
        }];
        var catpend=["Jumlah"];
        var idcatpend=["A",];
        ac_column_keu(seriespend,230,catpend,'chart_kewenangan','bar',undefined,idcatpend);




    }

    function ac_column_keu(seri,hei,categ,idchart,types,colr,idcat) {


        if(colr == undefined){

            colr =['#2db327','#ff8943','#6857ff','#3ebaff','#ff1c28','#8f3270',]
        }else{
            colr =[colr]
        }


        Highcharts.chart(idchart, {
            chart: {
                type: 'column',
                height:(hei +20),
                // events: {
                //     click: function () {
                //         console.log(event);
                //         //show_modal_detail(idcat[this.x],idchart,categ[this.x]);
                //
                //     }
                // }
            },
            colors:colr,
            title:null,

            xAxis: {


                categories:categ,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah'
                },
                labels: {
                    formatter: function() {
                        var ret,
                            numericSymbols = ['Rb', 'Jt', 'M', 'T', 'P', 'E'],
                            i = 6;
                        if(this.value >=1000) {
                            while (i-- && ret === undefined) {
                                multi = Math.pow(1000, i + 1);
                                if (this.value >= multi && numericSymbols[i] !== null) {
                                    ret = (this.value / multi) + numericSymbols[i];
                                }
                            }
                        }
                        return  (ret ? ret : this.value);
                    }
                },
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            plotOptions: {

                column: {
                    pointPadding: 0.1,
                    borderWidth: 0
                },
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                // alert(categ[this.x]);
                                show_modal_detail(idcat[this.x],idchart,categ[this.x]);

                            }
                        }
                    }
                },

            },
            series: seri,
            legend: {
                itemStyle: {
                    color: '#000',
                    fontWeight: 'lighter',
                    borderWidth:1,
                }
            },

        });
    }
     function show_modal_detail(id,charts,serie) {
        $("#table_detail thead").html('');
         $("#table_detail tbody").html('');
        $("#modal_detail").modal();


        if(charts =='chart_ttl'){ //==============================DETAIL PAGU
            var titdet = 'PAGU';
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Pagu</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa</th>' +
                '</tr>';

            $("#table_detail thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#table_detail tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>'+(10000 * i)+'</td><td>'+(5000 * i)+'</td>' +
                    '<td>'+(5000 * i)+'</td></tr>');
            }


        }else if(charts =='chart_jenis_belanja'){ //==============================JENIS BELANJA
            var titdet = 'Belanja'+serie;
            var tbhead='<tr><th rowspan="2">No</th>' +
                '<th rowspan="2">Satker</th>' +
                '<th colspan="3">'+serie+'</th></tr>' +
                '<tr><th>Pagu</th><th>Realisasi</th><th>Sisa</th></tr>';

            $("#table_detail thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#table_detail tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>'+(10000 * i)+'</td><td>'+(5000 * i)+'</td><td>'+(5000 * i)+'</td></tr>');
            }

        }else if(charts =='chart_kewenangan'){//==============================DETAIL KEWENANGAN
            var titdet = ' Kewenangan '+serie;
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Pagu</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa</th>' +
                '</tr>';

            $("#table_detail thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#table_detail tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>'+(10000 * i)+'</td><td>'+(5000 * i)+'</td>' +
                    '<td>'+(5000 * i)+'</td></tr>');
            }

        }else if(charts =='chart_sd'){//==============================DETAIL SUMBER DANA
        var titdet = ' Sumber Dana '+serie;
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Pagu</th>' +
                '<th>Realisasi</th>' +
                '<th>Sisa</th>' +
                '</tr>';

            $("#table_detail thead").html(tbhead);

            for(var i =1;i <20 ;i++){
                $("#table_detail tbody").append('<tr> <td>'+i+'</td>' +
                    '<td>Nama satker '+i+'</td>' +
                    '<td>'+(10000 * i)+'</td><td>'+(5000 * i)+'</td>' +
                    '<td>'+(5000 * i)+'</td></tr>');
            }

         }



        $(".titdet").html("Detail "+titdet);

     }

</script>

<?php
$this->load->view('layout/footer');
?>
