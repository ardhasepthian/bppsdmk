<?php
$this->load->view('layout/header_dash');
?>
<style media="screen">
    .same-height {
        height: 417px;
        max-height: 417px;
        overflow: auto;
    }
</style>
<!--<script src="--><?php //echo base_url(); ?><!--assets/script/highchart/highcharts.js"></script>-->

<style>
    table thead th{
        text-align: center;
    }
    .tdkanan {
        text-align: right;
    }

    .opd_tit {
        text-align: center;
        font-size: 12px;
    }

    .opd_jm2 {
        text-align: center;
        font-size: 20px;
        font-weight: bolder;
    }

    .opd_tit2 {
        text-align: center;
        font-size: 14px;
    }

    @media only screen and (max-width: 600px) {

        .opd_tit {
            text-align: center;
            font-size: 10px;
        }
        /*.dchart{*/
        /*max-height: 150px;*/
        /*font-size: 10px;*/

        /*}*/
    }

    @media only screen
    and (min-device-width: 320px)
    and (max-device-width: 480px)
    and (-webkit-min-device-pixel-ratio: 2) {
        .width-cols {
            color: red;
            height: 100px;
            width: 50%;
        }
    }



    .mhgh {
        height: 250px;
        padding-top: 50px;

    }




    .grad_bluemod {
        background: #fdfdff; /* For browsers that do not support gradients */
        background: -webkit-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Safari 5.1-6*/
        background: -o-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 49, 48)); /*Opera 11.1-12*/
        background: -moz-linear-gradient(bottom, rgba(255, 0, 0, 0), rgb(179, 59, 63)); /*Fx 3.6-15*/
        background: linear-gradient(to bottom, #fff, #b33b3f); /*Standard*/

        border: solid 2px white;
    }

    .xx {
        border-radius: 2px;
        /*height: 65px;*/
        padding: 1px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .xx2 {
        border-radius: 5px;
        height: 65px;
        padding: 5px;
        margin-bottom: 2px;
        border: solid 1px white;

    }

    .pman {
        margin: 1px;
        /*font-size: larger;*/
    }



    .abangtext {
        color: #ffffff;
        background-color: #b40000;
    }

</style>


<div class="page-title hidden">
    <div class="title_left">

    </div>

    <div class="title_right" style="float:right;">

    </div>
</div>


<section id="main-content">
    <section class="wrapper">
            <b><center><h3 style="margin-top: 3px;margin-bottom: 3px">&nbsp;
            Monitoring Kepegawaian
            </h3></center></b>
        <div class="col-md-12">
            <h4 style="margin-top: 3px;margin-bottom: 3px">Profil SDM</h4>
            <div class="panel col-md-4 col-xs-12 ">
                <div class="panel-heading">
                    <div class="panel-actions">
<!--                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('usia')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
                    </div>
                </div>
                <div class="panel-body" style="min-height: 230px">
                    <div style="text-align: center;padding: 40px; background-color: #269463;color: white;">
                        <h2>Jumlah Pegawai</h2>
                        <h1 style="font-size: 60px; font-weight: bolder">1,000</h1>
                    </div>
                </div>
            </div>
            <div class="panel col-md-4 col-xs-12 ">
            <div class="panel-heading">Usia
                <div class="panel-actions">
                    <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('usia')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                </div>
            </div>
            <div class="panel-body" style="min-height: 200px">
                <div class="" id="chart_usia"></div>
            </div>
            </div>
            <div class="panel col-md-4 col-xs-12 ">
                <div class="panel-heading">Golongan
                    <div class="panel-actions">
                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('golongan')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_golongan"></div>
                </div>
            </div>
            <div class="panel col-md-4 col-xs-12 ">
                <div class="panel-heading">Pendidikan
                    <div class="panel-actions">
                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('pendidikan')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_pendidikan"></div>
                </div>
            </div>
            <div class="panel col-md-4 col-xs-12 ">
                <div class="panel-heading">Jenis Kelamin
                    <div class="panel-actions">
                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('jk')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_jk"></div>
                </div>
            </div>
            <div class="panel col-md-4 col-xs-12 ">
                <div class="panel-heading">Jabatan
                    <div class="panel-actions">
                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('jabatan')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_pangkat"></div>
                </div>
            </div>
            <div class="panel col-md-4 col-xs-12 ">
                <div class="panel-heading">Jabatan Profesi
                    <div class="panel-actions">
                        <a href="#" data-toggle="modal" data-target="#modalsatker" onclick="showsatker('profesi')" class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_profesi"></div>
                </div>
            </div>


<!--        </div>-->
<!--        <div class="col-md-12">-->
<!--            <h4 style="margin-top: 3px;margin-bottom: 3px">Kehadiran</h4>-->
            <div class="panel panel-info col-md-4 col-xs-12 ">
                <div class="panel-heading">Presensi Bulan ini
                    <div class="panel-actions">
                        <a href="#"  data-toggle="modal" data-target="#modalsatker" onclick="showsatker('hdrbulan')"  class="btn-setting"><i class="fa fa-external-link-square"></i></a>
                    </div>
                </div>
                <div class="panel-body" style="min-height: 200px">
                    <div class="" id="chart_hdr_total"></div>
                </div>
            </div>
<!--            <div class="panel panel-info col-md-4 col-xs-12 ">-->
<!--                <div class="panel-heading">Presensi Per Satker-->
<!--                    <div class="panel-actions">-->
<!--                        <a href="#"  data-toggle="modal" data-target="#modalsatker" onclick="showsatker('hdrsat')"  class="btn-setting"><i class="fa fa-external-link-square"></i></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="panel-body" style="min-height: 200px">-->
<!--                    <div class="" id="chart_hdr_bulan"></div>-->
<!--                </div>-->
<!--            </div>-->

        </div>
<!--        <div class="col-md-12">Penegakan disiplin-->
<!--        </div>-->
 </section>
</section>

<div id="modalsatker" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail</h4>
            </div>
            <div class="modal-body">
                <table id="data_user_table" class="table table-bordered responsive" width="100%" >
                    <thead>
                    <tr>
                        <th style="text-align:center">No</th>
                        <th>Satker</th>
                        <th>20-30</th>
                        <th>30-35</th>
                        <th>35-40</th>
                        <th>40-45</th>
                        <th> >45 </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>




<script>



    $(document).ready(function () {




       // go();
        kepeg();
        $('#modalsatker').on('show.bs.modal', function (e)
        {
            $('#data_user_table').css('width', '');
        });
        $('#modalsatker').on('hidden.bs.modal', function (e)
        {
            var dTable = $('#data_user_table').dataTable();
            dTable.fnClearTable();
            dTable.fnDestroy();

        });


        init_EasyPieChart();



    });



    function showsatker(jchart) {
        $.ajax({
            url: '<?php echo base_url(); ?>dashboard/get_data_satker',
            type: 'POST',
            success: function (response) {
                var respon = JSON.parse(response);
                console.log(respon);
                var satker =respon;

        if(jchart=='usia'){
            var tbhead='<tr><th style="text-align:center">No</th>\n' +
                '                        <th>Satker</th>\n' +
                '                        <th>20-30</th>\n' +
                '                        <th>30-35</th>\n' +
                '                        <th>35-40</th>\n' +
                '                        <th>40-45</th>\n' +
                '                        <th> >45 </th></tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">61</td><td class="tdkanan">40</td>' +
                    '<td class="tdkanan">20</td><td class="tdkanan">10</td>' +
                    '<td class="tdkanan">5</td></tr>');
            }
        }else if(jchart=='golongan') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>GOL I</th>' +
                '<th>GOL II</th>' +
                '<th>GOL III</th>' +
                '<th>GOL IV</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td>' +
                    '<td class="tdkanan">10</td><td class="tdkanan">10</td></tr>');
            }
        }else if(jchart=='pendidikan') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>DI/SMA</th>' +
                '<th>D3</th>' +
                '<th>D4</th>' +
                '<th>S1</th>' +
                '<th>S2</th><th>S3</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td>' +
                    '<td class="tdkanan">10</td><td class="tdkanan">10</td><td class="tdkanan">10</td><td class="tdkanan">10</td></tr>');
            }
        }else if(jchart=='jk') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Laki Laki</th>' +
                '<th>Perempuan</th></tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td></tr>');
            }
        }else if(jchart=='jabatan') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Struktural</th>' +
                '<th>Fungsional</th></tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td></tr>');
            }
        }else if(jchart=='profesi') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Perawat</th>' +
                '<th>Bidan</th>' +
                '<th>Dr Umum</th>' +
                '<th>Dr Spc</th>' +
                '<th>Lainya</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td>' +
                    '<td class="tdkanan">10</td><td class="tdkanan">10</td><td class="tdkanan">10</td></tr>');
            }
        }else if(jchart=='hdrbulan') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Dinas Luar</th>' +
                '<th>Alpha</th>' +
                '<th>Izin</th>' +
                '<th>Sakit</th>' +
                '<th>Pendidikan</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td>' +
                    '<td class="tdkanan">10</td><td class="tdkanan">10</td><td class="tdkanan">10</td></tr>');
            }
        }
        else if(jchart=='hdrsat') {
            var tbhead='<tr><th >No</th>' +
                '<th >Satker</th>' +
                '<th>Dinas Luar</th>' +
                '<th>Alpha</th>' +
                '<th>Izin</th>' +
                '<th>Sakit</th>' +
                '<th>Pendidikan</th>' +
                '</tr>';

            $("#data_user_table thead").html(tbhead);

            for(var i in satker){
                $("#data_user_table tbody").append('<tr> <td>'+(i*1 +1)+'</td>' +
                    '<td>'+respon[i].nm_satker+'</td>' +
                    '<td class="tdkanan">30</td><td class="tdkanan">20</td>' +
                    '<td class="tdkanan">10</td><td class="tdkanan">10</td><td class="tdkanan">10</td></tr>');
            }
        }



        $('#data_user_table').dataTable({
            pageLength:100,
        });

            }
        });

    }







    function test(id){
        var tahun = $("#tahun").val();
        window.location = "<?php echo base_url(); ?>dashboard/dashboard_detail?tahun="+tahun+"&pro_id="+id;
    }

    function kepeg() {
        var seriesusia =[{
                name: 'jumlah',
                data: [44, 55, 57, 56,30]
            }];
        var catusia=["20-30", "30-40", "40-50", "50-55", "> 55"];
        var color='#7fdd86';
        ac_column_kepeg(seriesusia,180,catusia,'chart_usia','bar',color);
        // alert(color);

        var seriesgol =[{
            name: 'jumlah',
            data: [44, 55, 15, 10]
        }];
        var catgol=["GOL I", " GOL II", " GOL III", "GOL IV"];
        ac_column_kepeg(seriesgol,180,catgol,'chart_golongan','bar','#dd816b');

        var seriespend =[{
            name: 'jumlah',
            data: [44, 55,50,60, 15, 10]
        }];
        var catpend=["D1", "D3", "D4", "S1","S2","S3"];
        ac_column_kepeg(seriespend,180,catpend,'chart_pendidikan','bar','#915fdd');

        var seriesjk =[{
            name: 'jumlah',
            data: [44, 55,]
        }];
        var catjk=["Laki laki", "Perempuan"];
        ac_column_kepeg(seriesjk,180,catjk,'chart_jk','bar','#ddc23c');

        var seriespang =[{
            name: 'jumlah',
            data: [ 55,40]
        }];
        var catpang=["Struktural","Fungsional",];
        ac_column_kepeg(seriespang,180,catpang,'chart_pangkat','bar','#21a9dd');
        var seriespro =[{
            name: 'jumlah',
            data: [44, 55,50,60, 70]
        }];
        var catpro=["Perawat", "Bidan", "Dr umum", "Dr sp","Pegawai"];
        ac_column_kepeg(seriespro,180,catpro,'chart_profesi','bar','#b33130');



        // var serieshdb =[{
        //     name: 'Dinas Luar',
        //     data: [11, 12,10,8,7 , 4,]
        // },{
        //     name: 'Alpha',
        //     data: [3, 8,4,6, 8, 4,]
        // },{
        //     name: 'Izin',
        //     data: [3, 7,4,4,5,6,]
        // },{
        //     name: 'Sakit',
        //     data: [11, 12,10,8,7 , 7,]
        // },{
        //     name: 'Diklat',
        //     data: [1, 1,1,1, 1, 2,]
        // }
        // ];
        // var cathdb=["Sat 1", "Sat 2", "sat 3", "sat 4","sat 5","sat 6"];
        // ac_column_kepeg(serieshdb,180,cathdb,'chart_hdr_bulan','bar',);

        var serieshdb =[
            {
            name: 'Dinas Luar',
            data: [7]
        },{
            name: 'Alpha',
            data: [3]
        },{
            name: 'Izin',
            data: [5]
        },{
            name: 'Sakit',
            data: [4]
        },{
            name: 'Diklat',
            data: [6]
        }

        ];
        var cathdb=["Total"];
        ac_column_kepeg(serieshdb,180,cathdb,'chart_hdr_total','bar',);
    }

</script>

<?php
$this->load->view('layout/footer');
?>
