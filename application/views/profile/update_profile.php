<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Distributor
  | Date       : 02-05-2017
  =========================================================  -->

<?php
$this->load->view('layout/header');
?>
<style>
	#distributor_table .dropdown-menu {
	position: relative;
	width: 100%;
	}   
	</style>
<div  class="row">
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class=panel>
            <div class="x_title">
                <h2><?php echo $tittle; ?></h2>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <form id="form" data-parsley-validate class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama distributor<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" autocomplete="off" id="kd_distributor" name="kd_distributor" value="<?php echo $form['kd_distributor']; ?>"/>
                            <input type="text" autocomplete="off" maxlength="100" id="nm_distributor" name="nm_distributor" class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['nm_distributor']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat distributor<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" maxlength="255" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['almt_distributor']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Provinsi</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off"  id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['nm_prov']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kab/Kota</label>
                        <div id="kabkot" class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['nm_kabkot']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kecamatan<span class="required">*</span>
                        </label>
                        <div id="kecamatan" class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['nm_kecamatan']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kelurahan/Desa<span class="required">*</span>
                        </label>
                        <div id="keldes" class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['nm_kecamatan']; ?>" readonly="readonly"/>
                        </div>
                    </div>                
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" maxlength="100" id="email_distributor" name="email_distributor"  class="form-control col-md-7 col-xs-12  email " value="<?php echo $form['email_distributor']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No HP<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" maxlength="15" id="msisdn_distributor" name="msisdn_distributor" class="form-control col-md-7 col-xs-12 required number" value="<?php echo $form['msisdn_distributor']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No telp kantor<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" maxlength="15" id="no_telp_distributor" name="no_telp_distributor" class="form-control col-md-7 col-xs-12 required number" value="<?php echo $form['no_telp_distributor']; ?>"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No NPWP
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" maxlength="20" id="npwp_distributor" name="npwp_distributor" class="form-control col-md-7 col-xs-12  special " value="<?php echo $form['npwp_distributor']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">No SIUP
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" maxlength="20" id="siup_distributor" name="siup_distributor" class="form-control col-md-7 col-xs-12  special" value="<?php echo $form['siup_distributor']; ?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori<span class="required">*</span>
                        </label>
                        <div id="keldes" class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['nm_ctg_distributor']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis<span class="required">*</span>
                        </label>
                        <div id="keldes" class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo $form['jns_distributor']; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cabang<span class="required">*</span>
                        </label>
                        <div id="keldes" class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" autocomplete="off" id="almt_distributor" name="almt_distributor"  class="form-control col-md-7 col-xs-12 required special" value="<?php echo strtolower($form['sts_cabang']) === 'p' ? 'PUSAT' : 'CABANG'; ?>" readonly="readonly"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
                            <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <table class="table  table-striped table-hover table-responsive" id="list-gudang" style="background: #fff;margin-bottom: 20px;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th class="text-center">Nama Gudang</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="gudang">
                        <?php
                            foreach ($gudang as $key => $val) {
                                $no = $key + 1;

                                echo '<tr id="data-list-'.$no.'"><td>'.$no.'</td><td>'.$val['nm_gudang'].'</td><td class="text-center"><div class="edit-gudang btn btn-danger btn-xs" data-kd="'.$val['kd_gudang'].'" data-nama="'.$val['nm_gudang'].'">Edit</div> </td></tr>';
                            }
                        ?>
                    </tbody>
                </table>
                <div class="pull-right"><button type="submit" class="btn btn-danger add-gudang">Tambah</button></div>
            </div>
        </div>
    </div>
</div>

<div id="modal-gudang" class="modal fade" role="dialog">
    <div id="msgid-info" style="display:none">
        <div class="alert alert-info alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> <span class="info-message"></span>
        </div>
    </div>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
			<form id="form-gudang">
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Gudang *
                    </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="hidden" id="kd_gudang" name="kd_gudang"/>
                        <input type="text" autocomplete="off" id="nm_gudang" maxlength="100" name="nm_gudang" class="form-control col-md-7 col-xs-12 required special"/>
                    </div>
                </div>
            </div>
			</form>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger save" >Simpan</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo $this->config->item('text_btn_close');?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>

<script>
    function cancel() {
        window.location = "<?php echo base_url(); ?>";
    }
    
    $(window).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);

        $('#form').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) { 
                e.preventDefault();
                return false;
            }
        });

        $("#form").validate({
            submitHandler: function (form) {
                $.post("<?php echo base_url(); ?>profile/update", {
                    msisdn_distributor: $('#msisdn_distributor').val().trim(),
                    no_telp_distributor: $('#no_telp_distributor').val().trim(),
                    npwp_distributor: $('#npwp_distributor').val().trim(),
                    siup_distributor: $('#siup_distributor').val().trim(),
                    email_distributor: $('#email_distributor').val().trim(),
                }, function (data) {
                    if (data === 'success') {
                        window.location = "<?php echo base_url(); ?>profile";
                    }
                });
            }
        });

        $(document).on('click', '.edit-gudang', function () {
            $('.modal-title').html('Edit Gudang');
            $('#kd_gudang').val($(this).data('kd'));
            $('#nm_gudang').val($(this).data('nama'));
            $('#modal-gudang').modal();
        });

        $(document).on('click', '.add-gudang', function () {
            $('.modal-title').html('Tambah Gudang');
            $('#kd_gudang').val("");
            $('#nm_gudang').val("");
            $('#modal-gudang').modal();
        });

        $(document).on('click', '.save', function () {
			if($("#form-gudang").valid()){
            $.post("<?php echo base_url(); ?>profile/update_gudang", {
                kd_gudang: $('#kd_gudang').val().trim(),
                nm_gudang: $('#nm_gudang').val().trim(),
            }, function (data) {
                $('.info-message').html(data);
                $('#msgid-info').show();
                $('#msgid-info').fadeOut(5000);

                $.get("<?php echo base_url(); ?>profile/get_gudang", {}, function (data) {
                    var gudang = JSON.parse(data);

                    var table = '';
                    $.each(gudang, function(key, val) {
                        var no = key + 1;

                        table = table + '<tr id="data-list-'+ no +'"><td>'+ no +'</td><td>' + val.nm_gudang + '</td><td class="text-center"><div class="edit-gudang btn btn-danger btn-xs" data-kd="' + val.kd_gudang + '" data-nama="' + val.nm_gudang + '">Edit</div> </td></tr>';
                    });

                    $('.gudang').html(table);
                    $('#modal-gudang').modal("hide");
                });
            });
			}
        });
    });
</script>

<?php
$this->load->view('layout/footer');
?>
