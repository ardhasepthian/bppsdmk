

<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>

<script>
    $(document).ready(function () {
        $("#kategori_name").val('<?php echo $form['kategori_name']?>');
        $("#kategori_id").val('<?php echo $form['kategori_id']?>');

        $("#buttonadd").hide();

        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {


                $.post("<?php echo base_url(); ?>kategori/update_data", {
                        kategori_name: $('#kategori_name').val().trim(),
                        kategori_id: $('#kategori_id').val().trim(),

                    },
                    function (data) {
                        if (data !== '') {
                            // $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                            $('#msg').fadeOut(5000);
                            return;
                        }
                        alert('Data akan di proses');
                        window.location = "<?php echo base_url(); ?>kategori";
                    });
            }
        });

    });



    function cancel() {
        window.location = "<?php echo base_url(); ?>kategori";
    }



</script>
<div class="panel">
    <header class="panel-heading">
        Ubah Kategori
    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
        <div class="panel-body">

            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori <span class="required">*</span>
                    </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" autocomplete="off" id="kategori_name" maxlength="20" name="kategori_name"  class="form-control col-md-7 col-xs-12 required special"/>
                    </div>
                    <input type="hidden" id="kategori_id" />
                </div>

            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
            <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
        </div>
    </form>
</div>

