<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Distributor
  | Date       : 02-05-2017
  =========================================================  -->

<?php
$this->load->view('layout/header');
?>
<script>
    function add_kategori() {
        $.get("<?php echo base_url(); ?>kategori/add_data", function (data) {
            $("#maincontent").html(data);
        });
    }
    function edit_kategori(kategori_id) {
        $.ajax({
            url: '<?php echo base_url(); ?>kategori/edit_data',
            type: 'POST',
            data: {kategori_id: kategori_id
            },
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
    function delete_kategori(kategori_id) {
        if (confirm('Apakah anda yakin akan menghapus data ini ?')) {
            $.ajax({
                url: '<?php echo base_url(); ?>kategori/delete_data',
                type: 'POST',
                data: {kategori_id: kategori_id},
                success: function (data) {
                    if (data !== '') {
                        $('#dangermsg').html('Data ' + nama + ' tidak kosong !!');
                        $('#dangermsg').show();
                        $('#dangermsg').fadeOut(5000);
                    } else {
                        window.location = "<?php echo base_url(); ?>kategori";
                    }
                }
            });
        }
    }





    $(document).ready(function () {
       // $('body').popover({selector: '[data-toggle="popover"]'});
        $("#buttonadd").show();
        $('#msgidx').show();
        $('#msgidx').fadeOut(50000);
        var table;
        table = $('#outlet_table').dataTable(
                {
                    'oLanguage': {'sSearch': 'Pencarian',
                "oPaginate": {
                "sPrevious": "Sebelum",
                "sNext": "Lanjut",
                "sLast": "<<",
                "sFirst": ">>"
                            }
		},
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.   
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>kategori/get_data_paging",
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderablen
                        }
                    ],
                    fnDrawCallback : function() {
                        $('[data-toggle="popover"]').popover();
                    }
                }
        );
    });
    
    $('#outlet_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#outlet_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

</script>
<style>
	#outlet_table .dropdown-menu {
	position: relative;
	width: 100%;
	}   
	</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> <?php echo $tittle; ?></h4>

            </div>
            <div class="col-sm-2" id="buttonadd" style="display: none;" ><button onclick="javascript:add_kategori()" class="btn btn-primary btn-xs button-right">Tambah</button></div>
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="panel">
           <div class="panel-body">

                <table id="outlet_table" class="table table-bordered responsive">
                    <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                            <th>Kategori</th>
                            <th style="text-align:center"><?php echo $this->config->item('text_btn_action');?></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Distributor Detail Modal -->
<div id="detail_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail Perusahaan</h4>
            </div>
            <div class="modal-body">

                <form class="form-horizontal col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">NPWPD
                        </label>
                        <input id="kategori_id" name="kategori_id" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Nama
                        </label>
                        <input id="kategori_name" name="kategori_name" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Alamat
                        </label>
                        <textarea id="kategori_address" name="kategori_address" class="col-md-8 col-sm-5 col-xs-12 special" disabled></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Provinsi
                        </label>
                        <input id="nm_prov" name="nm_prov" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Kabupaten
                        </label>
                        <input id="nm_kabkot" name="nm_kabkot" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Kecamatan
                        </label>
                        <input id="nm_kecamatan" name="nm_kecamatan" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    </form>

                    <form class="form-horizontal col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Kelurahan
                            </label>
                            <input id="nm_keldes" name="nm_keldes" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                        </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Email Wp
                        </label>
                        <input id="kategori_mail" name="kategori_mail" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">No Telepon 1
                        </label>
                        <input id="kategori_msisdn_1" name="kategori_msisdn_1" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">no Akta Perusahaan
                        </label>
                        <input id="kategori_no_akta" name="kategori_no_ktp" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                        <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">No KTP
                        </label>
                        <input id="kategori_no_ktp" name="kategori_no_ktp" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                        </div>
                </form>



            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <br/>
                <center><h4> <b>Tenant</b></h4></center>
                <table id="tabopdmodal" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Tenant</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kecamatan</th>
                        <th>Jenis</th>
                        <th>Klasifikasi</th>
                    </tr>
                    </thead>
                    <tbody></tbody>

                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close');?></button>
            </div>
        </div>

    </div>
</div>

<?php
$this->load->view('layout/footer');
?>
