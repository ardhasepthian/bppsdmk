<?php
$this->load->view('layout/header');
?>
<script>
    function add_user() {
        $.get("<?php echo base_url(); ?>user/add_data", function (data) {
            $("#maincontent").html(data);
        });
    }
    function edit_user(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>user/edit_data',
            type: 'POST',
            data: {id: id},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
    function delete_user(id, nama) {
        if (confirm('Apakah anda yakin akan menghapus data ' + nama + ' ?')) {
            $.ajax({
                url: '<?php echo base_url(); ?>user/delete_data',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    if (data !== '') {
                        $('#dangermsg').html('Data ' + nama + ' tidak kosong !!');
                        $('#dangermsg').show();
                        $('#dangermsg').fadeOut(5000);
                    } else {
                        window.location = "<?php echo base_url(); ?>user";
                    }
                }
            });
        }
    }
    //=====================================================test dropdown on datatable ======================
    $('#user_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#user_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

    //============================================================================================================
    $(document).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(90000);
        var table;
        table = $('#user_table').dataTable(
                 {'oLanguage': {'sSearch': 'Pencarian',
                "oPaginate": {
                "sPrevious": "Sebelum",
                "sNext": "Lanjut",
                "sLast": "<<",
                "sFirst": ">>"
                            }
		},
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.   
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>user/get_paging",
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderable
                        }
                    ]
                }
        );
   //     $("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');

    });

    function reset_user(id,nama, kode) {
        if (confirm('Apakah anda yakin akan mereset password  user ' + nama + ' ?')) {
            $.ajax({
                url: '<?php echo base_url(); ?>user/reset_data',
                type: 'POST',
                data: {id: id,
                    kode: kode},
                success: function (response) {
                    alert('Data akan di proses !');
                    window.location = "<?php echo base_url(); ?>user";
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
    }


</script>
<style>
	#user_table .dropdown-menu {
	position: relative;
	width: 100%;
	}   
	</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> <?php echo $tittle; ?></h4>

            </div>
            <div class="col-sm-2" id="buttonadd"><button onclick="javascript:add_user()" class="btn btn-primary btn-xs button-right">Tambah</button></div>
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="dangermsg" style="display:none" class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
            <b>Alert!</b> 
        </div>
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="panel">


            <div class="panel-body">
                <table id="user_table" class="table table-bordered responsive" width="100%">
                    <thead>
                        <tr>
                            <th width="10%" >No</th>
                            <th width="25%" >Username</th>
                            <th width="25%" >Nama</th>
                            <th width="25%" >User Group</th>
                            <th  width="15%" style="text-align:center"><?php echo $this->config->item('text_btn_action');?></th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    </section>
</section>
<?php
$this->load->view('layout/footer');
?>
