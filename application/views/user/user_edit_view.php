<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>
<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">

<script>

    
    $(document).ready(function () {
        $("#buttonadd").hide();


        $('#id_user').val('<?php echo $form['id_user']; ?>');
        $('#kode').val('<?php echo $form['kode']; ?>');
        $('#wpd_name').val('<?php echo $form['nm_pegawai']; ?>');
        $('#username').val('<?php echo $form['username']; ?>');
        $('#group').val('<?php echo $form['id_group'].'||'.$form['nm_group']; ?>').prop('selected', true);
        var nm_group ='<?php echo $form['nm_group']; ?>';
        //===================================================================================
        $(".opsi").chosen({
            no_results_text: "Data tidak ditemukan!", search_contains: true
        });
        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {
                var id_user = $('#id_user').val().trim();
                var group = $("#group").val();
                var groups = group.split('||');
                if (groups[0] == '') {
                    $('#msg').show().html('Form belum lengkap');
                    $('#msg').fadeOut(5000);
                    return;
                }
                if(nm_group !== groups[1]){
                    if(nm_group === 'WAJIB PAJAK' &&  groups[1] ==='OBJEK PAJAK'){
                        alert('Usergroup tidak  di perbolehkan untuk user ini!');return;
                    }else if(nm_group === 'WAJIB PAJAK' &&  groups[1] ==='SUPER ADMINISTRATOR'){
                        alert('Usergroup tidak  di perbolehkan untuk user ini!');return;
                    }else if(nm_group === 'WAJIB PAJAK' &&  groups[1] ==='FISKUS'){
                        alert('Usergroup tidak  di perbolehkan untuk user ini!');return;
                    }else if(nm_group === 'OBJEK PAJAK' &&  groups[1] ==='WAJIB PAJAK'){
                        alert('Usergroup tidak  di perbolehkan untuk user ini!');return;
                    }else if(nm_group === 'OBJEK PAJAK' &&  groups[1] ==='SUPER ADMINISTRATOR'){
                        alert('Usergroup tidak  di perbolehkan untuk user ini!');return;
                    }else if(nm_group === 'OBJEK PAJAK' &&  groups[1] ==='FISKUS'){
                        alert('Usergroup tidak  di perbolehkan untuk user ini!');return;
                    }

                }

                $.blockUI({ message: 'Mohon tunggu' });
                $.post("<?php echo base_url(); ?>user/update_data", {
                    id_group: groups[0],
                    id_user: id_user
                },
                        function (data) {
                            if (data !== '') {
                                $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                                $('#msg').fadeOut(5000);
                                return;
                            }
                            $("#myModal").modal('hide');
                            alert('Data akan di proses');
                            window.location = "<?php echo base_url(); ?>user";
                        });
            }
        });

    });

    function show_data_user() {
        //$('#data_user_table').css('width','');
        $('#data_user_table').dataTable(
            {
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo base_url(); ?>user/get_paging_data_user",
                    "type": "GET"
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        "targets": [0], //first column / numbering column
                        "orderable": false //set not orderable
                    }
                ]
            }
        );
    }

    function cancel() {
        window.location = "<?php echo base_url(); ?>user";
    }
    
</script>
<div class="panel">
    <header class="panel-heading">
        <h4>Ubah User</h4>
    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">

            <input type="hidden" id="id_user" value=""/>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Usergroup<span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <select id="group" name="group"  class="form-control opsi col-md-7 col-xs-12 required">
                        <option value="" > Pilih </option>
                        <?php
                        foreach ($usergroup as $unit) {
                            echo '<option value="' . $unit['id_group'] . '||' . $unit['nm_group'] . '">' . $unit['nm_group'] . ' </option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kode<span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-6 col-xs-12 ">
                    <input type="text" autocomplete="off" id="kode" name="kode" readonly   class="form-control col-md-7 col-xs-12 required"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama *
                </label>
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <input type="text" autocomplete="off" id="wpd_name" name="wpd_name" readonly maxlength="240" class="form-control col-md-7 col-xs-12 required "/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username*
                </label>
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <input type="text" autocomplete="off" id="username" name="username" readonly  maxlength="100" class="form-control col-md-7 col-xs-12 required special"/>
                </div>
            </div>





    </div>
    <div class="panel-footer">
            <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
            <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
    </div>
    </form>
</div>
