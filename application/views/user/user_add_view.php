<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>
<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">

<script>


    $(document).ready(function () {
        $("#buttonadd").hide();

        $('#myModal').on('show.bs.modal', function (e)
        {
            $('#data_user_table').css('width', '');
        });
        $('#myModal').on('hidden.bs.modal', function (e)
        {
            var dTable = $('#data_user_table').dataTable();
            dTable.fnClearTable();
            dTable.fnDestroy();

        });
        $(".opsi").chosen();
        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {

                var group = $("#group").val();
                var groups = group.split('||');
                var id_group = groups[0];
                var kode = $('#kode').val().trim();
                var username = $('#username').val().trim();
                var password = $('#password').val().trim();
                $.blockUI({ message: 'Mohon tunggu' });
                $.post("<?php echo base_url(); ?>user/insert_data", {
                    id_group: id_group,
                    kode: kode,
                    username: username,
                    password: password

                },
                        function (data) {
                            if (data !== '') {
                                $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                                $('#msg').fadeOut(5000);
                                return;
                            }
                            alert('Data akan di proses');
                            window.location = "<?php echo base_url(); ?>user";
                        });
            }
        });

    });
    function show_data_user() {
        //$('#data_user_table').css('width','');
        var group =$("#group_name").val();
        $('#data_user_table').dataTable(
                {
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.   
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>user/get_paging_data_user",
                        "data":{group : group},
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderable
                        }
                    ]
                }
        );
    }
    function add_data_user(kode, nama, source_data) {
        $("#kode").val(kode);
        $("#wpd_name").val(nama);

        $("#source_data").val(source_data);
        var group =$("#group_name").val();
        if(group == 'SUPER ADMINISTRATOR' || group == 'FISKUS'){
            $("#username").val(kode).prop("disabled",true);
        }else{
            $("#username").val(kode).prop("disabled",true);
        }

    }

    function group_change() {

        var group = $("#group").val();
        var groups = group.split('||');
        var nm_group = groups[1];
        $("#kode").val('');
        $("#wpd_name").val('');

        $("#username").val('').prop("disabled",false);

        if(nm_group !==''){
            $("#btn_cari").show();
            $("#group_name").val(nm_group);
        }else{
            $("#btn_cari").hide();
            $("#group_name").val('');
        }
    }

    function cancel() {
        window.location = "<?php echo base_url(); ?>user";
    }
</script>
<div class="panel">
    <header class="panel-heading">
        <h4>Tambah User</h4>
    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">
        <input type="hidden" id="group_name" value=""/>

            <div class="form-horizontal form-label-left col-md-8" >

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Usergroup<span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                        <select id="group" name="group"  onchange="group_change()"  class="form-control opsi col-md-7 col-xs-12 required">
                            <option value="" > Pilih </option>
                            <?php
                            foreach ($usergroup as $unit) {
                                echo '<option value="' . $unit['id_group'] . '||' . $unit['nm_group'] . '">' . $unit['nm_group'] . ' </option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">KODE<span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12 ">
                        <div class="input-group">
                            <input type="text" autocomplete="off" id="kode" name="kode" readonly   class="form-control col-md-7 col-xs-12 required"/>
                            <span class="input-group-btn">
                                <button type="button" id="btn_cari" style="display: none;"  class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="show_data_user()">
                                    <i class="fa fa-search-plus"></i> Cari
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama *
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" autocomplete="off" id="wpd_name" name="wpd_name" readonly maxlength="240" class="form-control col-md-7 col-xs-12 required "/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username*
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" autocomplete="off" id="username" name="username"  maxlength="100" class="form-control col-md-7 col-xs-12 required special"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password*
                    </label>
                    <div class="col-md-9 col-sm-6 col-xs-12">
                        <input type="text" autocomplete="off" id="password" name="password"  maxlength="32" class="form-control col-md-7 col-xs-12 required special"/>
                    </div>
                </div>
            </div>


    </div>
    <div class="panel-footer">

                <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel'); ?></button>
                <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit'); ?></button>

        </div>
</form>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Daftar  Perusahaan</h4>
            </div>
            <div class="modal-body">
                <table id="data_user_table" class="table table-bordered responsive" width="100%" >
                    <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th style="text-align:center"><?php echo $this->config->item('text_btn_action'); ?></th>
                        </tr>
                    </thead>
                    <tbody>


                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close'); ?></button>
            </div>
        </div>

    </div>
</div>
