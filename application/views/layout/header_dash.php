
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Dashboasrd BPPSDMK">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png">

    <title>Dashboard BPPSDMK </title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css" rel="stylesheet" />
<!--    <link href="--><?php //echo base_url(); ?><!--assets/css/font-awesome.min.css" rel="stylesheet" />-->
    <link href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/widgets.css" rel="stylesheet">
<!--    <link href="--><?php //echo base_url()?><!--assets/css/all.css" rel="stylesheet">-->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>assets/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/datatables.net-bs/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">




    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->

    <style>
        .toolbar {
            float: left;
        }
        .kanan{
            text-align: right;
        }
        table th{
            text-align: center;
        }

        .panel .panel-heading {
            line-height: 34px;
            font-weight: bold;
            padding-right: 15px;
            padding-left: 15px;
            padding-top: 0px;
            padding-bottom: 0px;
            min-height: 34px;
            border-top: 1px solid #ccc;
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
            -webkit-border-radius: 0px;
            -moz-border-radius: 0px;
            border-radius: 0px;
            position: relative;
            /* box-shadow: inset 0 -2px 0 rgba(0,0,0,.05); */
            /* -moz-box-shadow: inset 0 -2px 0 rgba(0,0,0,.05); */
            /* -webkit-box-shadow: inset 0 -2px 0 rgba(0,0,0,.05); */
        }

    </style>
    <script>

    </script>
</head>

<body style="color:#000">

<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg" >
<!--        <div class="toggle-nav">-->
<!--            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>-->
<!--        </div>-->

        <img src="<?php echo base_url()?>assets/img/logo.png" width="120px" class="logo" style="margin-top: 5px">


        <div class="nav search-row" id="top_menu">

        </div>

        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->

            <!--                                dropdwon tahun-->


            <?php
            $ci =&get_instance();
            $usgn =$ci->session->userdata('efiskus_nm_group');

            $ci->load->model('login_model');
            $load = '';
            ?>
            <?php if($load==''){
            ?>
                    <ul class="nav pull-right top-menu" style="color:#3e2107;">

                        <li id="alert_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

<!--                                <i class="icon-bell-l"></i>-->
<!--                                <span class="badge bg-important">0</span>-->


                            </a>
                            <ul class="dropdown-menu extended notification">
                                <div class="notify-arrow notify-arrow-blue"></div>
                                <li>
                                    <p class="blue"> notifikasi baru</p>

                                </li>
            <?php
            }else{
            if($usgn =='WAJIB PAJAK' || $usgn =='OBJEK PAJAK' ){
            ?>

            <ul class="nav pull-right top-menu" style="color:#3e2107;">

                <li id="alert_notificatoin_bar" class="dropdown">

                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">

<!--                        <i class="icon-bell-l"></i>-->
<!--                        <span class="badge bg-important">--><?php //  echo sizeof($load); ?><!--</span>-->
                        <!-- ini untuk nomor badge  -->

                    </a>
                    <ul class="dropdown-menu extended notification">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue"><?php echo sizeof($load);?> notifikasi baru</p>
                            <!-- ini untuk nomor badge  -->
                        </li>

                        <?php for ($i = 0; $i < count($load); $i++) {
                            echo ' <li><a href="#" onclick="test_himbauan(\'' . $load[$i]['sptpd_code'] . '\')">
                                <span> Himbauan untuk SPTPD <br/>Tahun ' . $load[$i]['sptpd_tahun'] . ' Bulan ' . $load[$i]['sptpd_masa'] . ' <br/>pada NOPD ' . $load[$i]['nopd'] . '</span>
                                <span class="pull-right label-danger" style="color:#fff;padding:2px; "> Penting</span>
                            </a>
                        </li>';
                        }

                        } else { ?>
                        <ul class="nav pull-right top-menu" style="color:#3e2107;">

                            <li id="alert_notificatoin_bar" class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">

    <!--                                    <i class="icon-bell-l"></i>-->
    <!--                                    <span class="badge bg-important"></span>-->
    <!--                                    <!-- ini untuk nomor badge  -->-->

                                </a>
                                <ul class="dropdown-menu extended notification">
                                    <div class="notify-arrow notify-arrow-blue"></div>
                                    <li>
                                        <p class="blue"> notifikasi baru</p>
                                        <!-- ini untuk nomor badge  -->
                                    </li>



                        <?php }
            } ?>



                    </ul>


                </li>
                <!-- alert notification end-->
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="<?php echo base_url()?>assets/img/avatar1_small.jpg">
                            </span>
<!--                        <span class="username" style="color:#3e2107;">--><?php //echo ucfirst($this->session->userdata('efiskus_name'))?><!--</span>-->
                        <b style="color:#3e2107;" class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li class="eborder-top">
                            <a href="#" onclick="login()"><i class="icon_profile"></i>Login</a>
<!--                            <input type="text" placeholder="username"/>-->
<!--                            <input type="text" placeholder="password"/>-->

                        </li>


                    </ul>
                </li>
                <!-- user login dropdown end -->

            </ul>
<!--                        <div class="nav pull-right top-menu" style="padding-top: 5px;" id="headtahun"  >-->
<!--                            <select class="form-control" id="tahun" onchange="go()" >-->
<!--                                --><?php
//                                for($i = date('Y',strtotime('-1 years')) ; $i <= date('Y',strtotime('+1 years')); $i++){
//                                    if($i == date('Y') ){
//                                        $sele="selected";
//                                    }else{
//                                        $sele="";
//                                    }
//
//                                    echo "<option ".$sele." value='".$i."'>".$i."</option>";
//                                }
//                                ?>
<!--                            </select>-->
<!--                        </div>-->
            <!-- notificatoin dropdown end-->

                        <a href="<?php echo base_url()?>dashboard"><div class="nav pull-right top-menu" style="padding-top: 10px;"><i class="fa fa-home" style="font-size: 24px; color:#fff"></i></div></a>
                        <input type="hidden" value="2020" id="tahun" />
        </div>
    </header>
    <!--header end-->

    <!--sidebar start ====================================================================================MENU-->
    <aside>
        <div id="sidebar" class="nav-collapse " style="display: none">
            <!-- sidebar menu start-->


            <ul class="sidebar-menu">
                <li class="active">
                    <a class="" href="#">
                        <i class="icon_profile"></i>
<!--                        <span>--><?php //echo strtoupper($this->session->userdata('efiskus_nm_group'));?><!--</span>-->
                    </a>
                </li>
                <li class="active">
                    <a class="" href="<?= base_url('dashboard') ?>">
                        <i class="icon_house_alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
<!--                --><?php
//                $group =strtoupper($this->session->userdata('efiskus_name'));
//
//                $arr['id_group'] = $this->session->userdata('efiskus_id_group');
//                $arrlistmenuparent = $this->util_model->get_list_parent($arr);
//                if ($arrlistmenuparent != FALSE) {
//                    foreach ($arrlistmenuparent as $row):
//                        $arr['menu_parent_id'] = $row['menu_id'];
//                        $cek = $this->util_model->cek_child($arr);
//                        // var_dump($arr); exit;
//                        if ($cek > 0) {
//                            echo '<li class="sub-menu"><a href="javascript:;" class=""><i class="icon_' . $row['menu_icon'] . '"></i> <span> ' . $row['menu_desc'] . ' </span><span class="menu-arrow arrow_carrot-right"></span></a>';
//                            echo '<ul class="sub">';
//                            $arrlistmenuchild = $this->util_model->get_list_child($arr);
//                            foreach ($arrlistmenuchild as $rowchild):
//                                echo '<li><a href="' . base_url() . $rowchild['menu_controller'] . '">' . $rowchild['menu_desc'] . '</a></li>';
//                            endforeach;
//                            echo '</ul>';
//                            echo '</li>';
//                        }else {
//                            echo '<li><a href="' . base_url() . $row['menu_controller'] . '"><i class="icon_' . $row['menu_icon'] . '"></i><span> ' . $row['menu_desc'] . ' </span><span class="fa"></span></a></li>';
//                        }
//                    endforeach;
//                    //   }
//                }
//                ?>






            </ul>
            <!-- sidebar menu end-->

            <!-- <div id="footermenu" style="position: absolute; bottom: 10px; width: 100%"> -->

<!--            <img id="gmbfot" src="--><?php //echo base_url()?><!--assets/img/logo-synchro.png" width="100px" style="display: block; background-color:#7c4b1d;  margin-left: auto; margin-right: auto; position: absolute; bottom: 10px; width: 100%">-->
            <!-- </div> -->
        </div>
    </aside>

    <script src="<?php echo base_url()?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-1.8.3.min.js"></script>

    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.rateit.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.customSelect.min.js"></script>
    <script src="<?php echo base_url()?>assets/assets/chart-master/Chart.js"></script>



    <script src="<?php echo base_url()?>assets/js/jquery.autosize.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/gdp-data.js"></script>
        <script src="<?php echo base_url()?>assets/js/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/script/blockui/jquery.blockUI.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/select2/js/select2.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/script/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/input_mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/jquery.number.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/moment.js"></script>
<!--    <script src="--><?php //echo base_url(); ?><!--assets/script/apex/apexcharts.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/script/highchart/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/highchart/highcharts-3d.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/jquery.easypiechart.min.js"></script>
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/apex/styles.css" rel="stylesheet">
    <script src="<?php echo base_url()?>assets/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datatables.net-bs/js/dataTables.responsive.min.js"></script>


    <script type="text/javascript">

        function logout() {
            if (confirm("Apakah anda ingin keluar dari aplikasi?")) {
                window.location = "<?php echo base_url(); ?>login/signout";
            }
        }

        function login() {
            window.location = "<?php echo base_url(); ?>login/signin";
        }
        function changepass() {
            window.location = "<?php echo base_url(); ?>change_password";

        }

        function number_format (number, decimals, decPoint, thousandsSep) {

            number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
            var n = !isFinite(+number) ? 0 : +number
            var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
            var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
            var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
            var s = ''
            var toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec)
                return '' + (Math.round(n * k) / k)
                    .toFixed(prec)
            }
            // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || ''
                s[1] += new Array(prec - s[1].length + 1).join('0')
            }
            return s.join(dec)
        }
        //function profile() {
        //    window.location = "<?php //echo base_url(); ?>//profile";
        //
        //}


        function init_EasyPieChart() {

            if (typeof ($.fn.easyPieChart) === 'undefined') {
                return;
            }
            console.log('init_EasyPieChart');

            $('.chart').easyPieChart({
                easing: 'easeOutElastic',
                delay: 3000,
                barColor: '#b3182e',
                trackColor: '#ffc7f0',
                scaleColor: false,
                lineWidth: 20,
                trackWidth: 16,
                lineCap: 'butt',
                onStep: function (from, to, percent) {
                    // $(this.el).find('.percent').text(Math.round(percent));
                    $(this.el).find('.percent').text(percent);
                }
            });
            var chart = window.chart = $('.chart').data('easyPieChart');
            $('.js_update').on('click', function () {
                chart.update(Math.random() * 200 - 100);
            });

            //hover and retain popover when on popover content
            var originalLeave = $.fn.popover.Constructor.prototype.leave;
            $.fn.popover.Constructor.prototype.leave = function (obj) {
                var self = obj instanceof this.constructor ?
                    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
                var container, timeout;

                originalLeave.call(this, obj);

                if (obj.currentTarget) {
                    container = $(obj.currentTarget).siblings('.popover');
                    timeout = self.timeout;
                    container.one('mouseenter', function () {
                        //We entered the actual popover – call off the dogs
                        clearTimeout(timeout);
                        //Let's monitor popover content instead
                        container.one('mouseleave', function () {
                            $.fn.popover.Constructor.prototype.leave.call(self, self);
                        });
                    });
                }
            };

            $('body').popover({
                selector: '[data-popover]',
                trigger: 'click hover',
                delay: {
                    show: 50,
                    hide: 400
                }
            });

        };


        function ac_column_kepeg_old(seri,hei,categ,idchart,types,colr){
            var typ ='bar';
            if(types !==''){
                typ=types;
            }

            var options = {
                series: seri,

                chart: {
                    //type: 'bar',
                    type: typ,
                    // height: 350
                    height: hei
                },
                plotOptions: {
                    bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        // endingShape: 'rounded'
                    },
                },
                dataLabels: {
                    enabled: false,
                    formatter: function (val) {
                        return val + "";
                    },
                    offsetY: +20,
                    style: {
                        fontSize: '12px',
                        colors: ["#304758"]
                    }
                },
                stroke: {
                    show: true,
                    width: 4,
                    colors: ['transparent']
                },
                xaxis: {
                    // categories: ['Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct'],
                    categories: categ,
                },
                yaxis: {
                    title: {
                        text: 'Jumlah'
                    }
                },
                fill: {
                    colors:colr,
                    opacity: 1
                },
                tooltip: {
                    y: {
                        formatter: function (val) {
                            return " " + val + " "
                        }
                    }
                },
                responsive: [
                    {
                        breakpoint: 600,
                        options: {
                            chart: {
                                type: 'bar',
                                height: 200
                            }
                        }
                    }
                ],

            };

            var chart = new ApexCharts(document.querySelector("#"+idchart), options);
            chart.render();
        }




        function ac_column_kepeg(seri,hei,categ,idchart,types,colr) {


            if(colr == undefined){
               // alert('ok')
                colr =['#2db327','#ff8943','#6857ff','#3ebaff','#ff1c28','#8f3270',]
            }else{
                colr =[colr]
            }


            Highcharts.chart(idchart, {
                chart: {
                    type: 'column',
                    height:(hei +20)
                },
                colors:colr,
                title:null,
                // subtitle: {
                //     text: 'Source: WorldClimate.com'
                // },
                xAxis: {
                    // categories: [
                    //     'Jan',
                    //     'Feb',
                    //     'Mar',
                    //     'Apr',
                    //     'May',
                    //     'Jun',
                    //     'Jul',
                    //     'Aug',
                    //     'Sep',
                    //     'Oct',
                    //     'Nov',
                    //     'Dec'
                    // ],

                    categories:categ,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah'
                    },
                    labels: {
                        formatter: function() {
                            var ret,
                                numericSymbols = ['Rb', 'Jt', 'M', 'T', 'P', 'E'],
                                i = 6;
                            if(this.value >=1000) {
                                while (i-- && ret === undefined) {
                                    multi = Math.pow(1000, i + 1);
                                    if (this.value >= multi && numericSymbols[i] !== null) {
                                        ret = (this.value / multi) + numericSymbols[i];
                                    }
                                }
                            }
                            return  (ret ? ret : this.value);
                        }
                    },
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.1,
                        borderWidth: 0
                    },
                     // series:{color:['#8cceff','#ff75ce','#b5ff56']}
                },
                series: seri,
                legend: {
                    itemStyle: {
                        color: '#000',
                        fontWeight: 'lighter',
                        borderWidth:1,
                    }
                },

            });
        }


    </script>