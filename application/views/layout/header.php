
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Paradigma Proyek">
    <meta name="author" content="Paradigma">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png">

    <title>Bppsdmk Admin</title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" />
<!--    <link href="--><?php //echo base_url(); ?><!--assets/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />-->
<!--    <link href="--><?php //echo base_url(); ?><!--assets/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />-->
<!--    <link href="--><?php //echo base_url(); ?><!--assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />-->
<!--    <link rel="stylesheet" href="--><?php //echo base_url()?><!--assets/css/owl.carousel.css" type="text/css">-->
<!--    <link href="--><?php //echo base_url()?><!--assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">-->
<!--    <link rel="stylesheet" href="--><?php //echo base_url()?><!--assets/css/fullcalendar.css">-->
    <link href="<?php echo base_url()?>assets/css/widgets.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/all.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/style-responsive.css" rel="stylesheet" />
<!--    <link href="--><?php //echo base_url()?><!--assets/css/xcharts.min.css" rel=" stylesheet">-->
    <link href="<?php echo base_url()?>assets/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/select2/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/datatables.net-bs/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
    <!-- =======================================================
      Theme Name: NiceAdmin
      Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
      Author: BootstrapMade
      Author URL: https://bootstrapmade.com
    ======================================================= -->

    <style>
        .toolbar {
            float: left;
        }
        .kanan{
            text-align: right;
        }
        table th{
            text-align: center;
        }
    </style>
    <script>

    </script>
</head>

<body>

<!-- container section start -->
<section id="container" class="">


    <header class="header dark-bg" >
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
        </div>

        <img src="<?php echo base_url()?>assets/img/logo.png" width="120px" class="logo" style="margin-top: 5px">


        <div class="nav search-row" id="top_menu">

        </div>

        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->

            <?php
            $ci =&get_instance();
            $usgn =$ci->session->userdata('efiskus_nm_group');

            $ci->load->model('login_model');
            $load = '';
            ?>
            <?php if($load==''){
            ?>
                    <ul class="nav pull-right top-menu" style="color:#3e2107;">

                        <li id="alert_notificatoin_bar" class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

<!--                                <i class="icon-bell-l"></i>-->
<!--                                <span class="badge bg-important">0</span>-->


                            </a>
                            <ul class="dropdown-menu extended notification">
                                <div class="notify-arrow notify-arrow-blue"></div>
                                <li>
                                    <p class="blue"> notifikasi baru</p>

                                </li>
            <?php
            }else{
            if($usgn =='WAJIB PAJAK' || $usgn =='OBJEK PAJAK' ){
            ?>

            <ul class="nav pull-right top-menu" style="color:#3e2107;">

                <li id="alert_notificatoin_bar" class="dropdown">

                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">

<!--                        <i class="icon-bell-l"></i>-->
<!--                        <span class="badge bg-important">--><?php //  echo sizeof($load); ?><!--</span>-->
                        <!-- ini untuk nomor badge  -->

                    </a>
                    <ul class="dropdown-menu extended notification">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue"><?php echo sizeof($load);?> notifikasi baru</p>
                            <!-- ini untuk nomor badge  -->
                        </li>

                        <?php for ($i = 0; $i < count($load); $i++) {
                            echo ' <li><a href="#" onclick="test_himbauan(\'' . $load[$i]['sptpd_code'] . '\')">
                                <span> Himbauan untuk SPTPD <br/>Tahun ' . $load[$i]['sptpd_tahun'] . ' Bulan ' . $load[$i]['sptpd_masa'] . ' <br/>pada NOPD ' . $load[$i]['nopd'] . '</span>
                                <span class="pull-right label-danger" style="color:#fff;padding:2px; "> Penting</span>
                            </a>
                        </li>';
                        }

                        } else { ?>
                        <ul class="nav pull-right top-menu" style="color:#3e2107;">

                            <li id="alert_notificatoin_bar" class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">

    <!--                                    <i class="icon-bell-l"></i>-->
    <!--                                    <span class="badge bg-important"></span>-->
    <!--                                    <!-- ini untuk nomor badge  -->-->

                                </a>
                                <ul class="dropdown-menu extended notification">
                                    <div class="notify-arrow notify-arrow-blue"></div>
                                    <li>
                                        <p class="blue"> notifikasi baru</p>
                                        <!-- ini untuk nomor badge  -->
                                    </li>



                        <?php }
            } ?>

                    </ul>
                </li>
                <!-- alert notification end-->
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img alt="" src="<?php echo base_url()?>assets/img/avatar1_small.jpg">
                            </span>
                        <span class="username" style="color:#3e2107;"><?php echo ucfirst($this->session->userdata('efiskus_name'))?></span>
                        <b style="color:#3e2107;" class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li class="eborder-top">
                            <a href="#"><i class="icon_profile"></i>Profil</a>
                        </li>
                        <li>
                            <a href="#" onclick="changepass()"><i class="icon_key_alt"></i> Reset Password</a>
                        </li>
                        <li>
                            <a href="#" onclick="logout()"><i class="icon_key_alt"></i> Log Out</a>
                        </li>

                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
            <!-- notificatoin dropdown end-->
        </div>
    </header>
    <!--header end-->

    <!--sidebar start ====================================================================================MENU-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->


            <ul class="sidebar-menu">
                <li class="active">
                    <a class="" href="#">
                        <i class="icon_profile"></i>
                        <span><?php echo strtoupper($this->session->userdata('efiskus_nm_group'));?></span>
                    </a>
                </li>
                <li class="active">
                    <a class="" href="<?= base_url('dashboard2') ?>">
                        <i class="icon_house_alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php
                $group =strtoupper($this->session->userdata('efiskus_name'));

                $arr['id_group'] = $this->session->userdata('efiskus_id_group');
                $arrlistmenuparent = $this->util_model->get_list_parent($arr);
                if ($arrlistmenuparent != FALSE) {
                    foreach ($arrlistmenuparent as $row):
                        $arr['menu_parent_id'] = $row['menu_id'];
                        $cek = $this->util_model->cek_child($arr);
                        // var_dump($arr); exit;
                        if ($cek > 0) {
                            echo '<li class="sub-menu"><a href="javascript:;" class=""><i class="icon_' . $row['menu_icon'] . '"></i> <span> ' . $row['menu_desc'] . ' </span><span class="menu-arrow arrow_carrot-right"></span></a>';
                            echo '<ul class="sub">';
                            $arrlistmenuchild = $this->util_model->get_list_child($arr);
                            foreach ($arrlistmenuchild as $rowchild):
                                echo '<li><a href="' . base_url() . $rowchild['menu_controller'] . '">' . $rowchild['menu_desc'] . '</a></li>';
                            endforeach;
                            echo '</ul>';
                            echo '</li>';
                        }else {
                            echo '<li><a href="' . base_url() . $row['menu_controller'] . '"><i class="icon_' . $row['menu_icon'] . '"></i><span> ' . $row['menu_desc'] . ' </span><span class="fa"></span></a></li>';
                        }
                    endforeach;
                    //   }
                }
                ?>






            </ul>
            <!-- sidebar menu end-->

            <!-- <div id="footermenu" style="position: absolute; bottom: 10px; width: 100%"> -->

<!--            <img id="gmbfot" src="--><?php //echo base_url()?><!--assets/img/logo-synchro.png" width="100px" style="display: block; background-color:#7c4b1d;  margin-left: auto; margin-right: auto; position: absolute; bottom: 10px; width: 100%">-->
            <!-- </div> -->
        </div>
    </aside>

    <script src="<?php echo base_url()?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery-1.8.3.min.js"></script>
<!--    <script type="text/javascript" src="--><?php //echo base_url()?><!--assets/js/jquery-ui-1.9.2.custom.min.js"></script>-->

    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--    <script src="--><?php //echo base_url()?><!--assets/assets/jquery-knob/js/jquery.knob.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/jquery.sparkline.js" type="text/javascript"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/owl.carousel.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/fullcalendar.min.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/calendar-custom.js"></script>-->
    <script src="<?php echo base_url()?>assets/js/jquery.rateit.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.customSelect.min.js"></script>
    <script src="<?php echo base_url()?>assets/assets/chart-master/Chart.js"></script>


    <script src="<?php echo base_url()?>assets/js/scripts.js"></script>
<!--    <script src="--><?php //echo base_url()?><!--assets/js/sparkline-chart.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/easy-pie-chart.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/jquery-jvectormap-1.2.2.min.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/jquery-jvectormap-world-mill-en.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/xcharts.min.js"></script>-->
    <script src="<?php echo base_url()?>assets/js/jquery.autosize.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.placeholder.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/gdp-data.js"></script>
<!--    <script src="--><?php //echo base_url()?><!--assets/js/morris.min.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/sparklines.js"></script>-->
<!--    <script src="--><?php //echo base_url()?><!--assets/js/charts.js"></script>-->
        <script src="<?php echo base_url()?>assets/js/jquery.slimscroll.min.js"></script>


    <script src="<?php echo base_url(); ?>assets/script/blockui/jquery.blockUI.js"></script>
    <!--                            <script src="--><?//= base_url('assets/script/jqueryui/jquery-ui.min.js') ?><!--" charset="utf-8"></script>-->
    <!--                            <script src="--><?php //echo base_url(); ?><!--assets/script/bootstrap/dist/js/bootstrap.min.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/script/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/select2/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker.id.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/datatables.net-bs/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/input_mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/script/jquery.number.js"></script>


    <script type="text/javascript">

        function logout() {
            if (confirm("Apakah anda ingin keluar dari aplikasi?")) {
                window.location = "<?php echo base_url(); ?>login/signout";
            }
        }
        function changepass() {
            window.location = "<?php echo base_url(); ?>change_password";

        }

        function number_format (number, decimals, decPoint, thousandsSep) {

            number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
            var n = !isFinite(+number) ? 0 : +number
            var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
            var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
            var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
            var s = ''
            var toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec)
                return '' + (Math.round(n * k) / k)
                    .toFixed(prec)
            }
            // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || ''
                s[1] += new Array(prec - s[1].length + 1).join('0')
            }
            return s.join(dec)
        }
        //function profile() {
        //    window.location = "<?php //echo base_url(); ?>//profile";
        //
        //}
    </script>