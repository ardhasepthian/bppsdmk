<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>
<link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/script/chosen.jquery.min.js"></script>
<!--add by fira 21/08/2017-->
<script src="<?php echo base_url(); ?>assets/script/bootstrap-multiselected.js"></script>
<!--end add by fira-->

<script>
    $(document).ready(function () {

        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {
                $.post("<?php echo base_url(); ?>usergroup/update_usergroup_menu", {
                    id_group: $('#id_group').val(),
                    arr_menu: $('#arr_menu').val()
                },
                        function (data) {
                            if (data !=='') {
                                $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                                $('#msg').fadeOut(5000);
                                return;
                            }
                            alert('Data akan di proses');
                            window.location = "<?php echo base_url(); ?>usergroup";
                        });
            }
        });

        //comment by fira 21/08/2017
//        $(".opsi").chosen({
//            no_results_text: "Data tidak ditemukan!", search_contains: true
//        });
        // add by fira 21/08/2017
        $('.multiselected').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            includeSelectAllOption: true,
            maxHeight: 300,
            buttonWidth: '550px',
            numberDisplayed: 1
        });
        //end add by fira
    });
    function cancel() {
        window.location = "<?php echo base_url(); ?>usergroup";
    }
</script>
<!--add by fira 21/08/2017-->
<style>
    ul.multiselect-container {
        width: 100% !important;
        position: relative !important;
        padding: 10px !important;
    }
</style>
<!--end add by fira -->

<div class="panel">
    <header class="panel-heading">
        <h4>Form Pengaturan Menu</h4>

    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">


            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">User Group<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" autocomplete="off" id="nm_usergroup" name="nm_usergroup" value="<?php echo $nm_group['nm_group']; ?>" readonly class="form-control col-md-7 col-xs-12 required special" style="width: 550px;"/>
                    <input type="hidden" autocomplete="off" id="id_group" value="<?php echo $nm_group['id_group']; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Daftar Menu<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <!--comment by fira 21/08/2017 -->
                    <!--<select id="arr_menu" name="arr_menu" class="opsi special required" multiple="true" style="width: 550px;" data-placeholder="Pilih Menu...">-->
                    <!--add by fira 21/08/2017-->
                    <select id="arr_menu" name="arr_menu" class="opsi multiselected special required" multiple="true" style="width: 550px; display:none;" data-placeholder="Pilih Menu...">
                        <!--end add by fira -->
                        <?php
                        $list_combo_menu_selected_val = null;
                        $menu_selected_count = count($list_combo_menu_selected);
                        for ($b = 0; $b < $menu_selected_count; $b++) {
                            $list_combo_menu_selected_val[$b] = $list_combo_menu_selected[$b]['menu_id'];
                        }

                        if (isset($list_combo_menu) && $list_combo_menu != FALSE) :
                            $selector = "";
                            foreach ($list_combo_menu as $menu) :
                                ?>
                                <option value="<?php echo $menu['menu_id']; ?>" <?php echo (in_array($menu['menu_id'], $list_combo_menu_selected_val)) ? ' selected' : ''; ?>><?php echo $menu['menu_desc']; ?></option>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </select>
                </div>
            </div>



    </div>
    <div class="panel-footer">

        <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
        <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>

    </div>

    </form>
</div>
