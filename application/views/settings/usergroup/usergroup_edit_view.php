<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>

<script>
    $(document).ready(function () {
        $("#buttonadd").hide();
        $(".opsi").chosen({
            no_results_text: "Data tidak ditemukan!", search_contains: true
        });
        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {
                $.post("<?php echo base_url(); ?>usergroup/update_data", {
                    nm_group: $('#nm_group').val().trim(),
                    id_group: $('#id_group').val().trim()
                }, function (data) {
                    if (data !=='') {
                        $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                        $('#msg').fadeOut(5000);
                        return;
                    }
                    alert('Data akan di proses');
                    window.location = "<?php echo base_url(); ?>usergroup";
                });
            }
        });

    });
    function cancel() {
        window.location = "<?php echo base_url(); ?>usergroup";
    }
</script>
<div class="panel">
    <header class="panel-heading">
        <h4>Ubah User Group</h4>

    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">
        <div class="alert alert-danger alert-dismissable" id="msg" style="display:none">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
            <b>Alert!</b>
        </div>


            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">User Group<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="nm_group" maxlength="80" value="<?php echo $form['nm_group']; ?>" class="form-control col-md-7 col-xs-12 required special"/>
                    <input type="hidden" autocomplete="off"  id="id_group" name="id_group" value="<?php echo $form['id_group']; ?>"/>
                </div>
            </div>



    </div>

        <div class="panel-footer">
            <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
            <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
        </div>

    </form>
</div>
