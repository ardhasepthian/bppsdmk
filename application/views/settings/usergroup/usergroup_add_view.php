<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>

<script>
    $(document).ready(function () {
        $("#buttonadd").hide();
        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {
                $.post("<?php echo base_url(); ?>usergroup/insert_data", {
                    nm_group: $('#nm_group').val().trim(),
                },
                function (data) {
                    if (data !=='') {
                        $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                        $('#msg').fadeOut(5000);
                        return;
                    }
                    alert('Data akan di proses');
                    window.location = "<?php echo base_url(); ?>usergroup";

                });
            }
        });

    });
    function cancel() {
        window.location = "<?php echo base_url(); ?>usergroup";
    }
</script>
<div class="panel">
    <header class="panel-heading">
        <h4>Tambah Usergroup</h4>

    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">User Group<span class="required">*</span>
                </label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <input type="text" autocomplete="off" id="nm_group" maxlength="80" class="form-control col-md-7 col-xs-12 required special"/>
                </div>
            </div>


    </div>
    <div class="panel-footer">

        <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
        <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>

    </div>

    </form>
</div>
