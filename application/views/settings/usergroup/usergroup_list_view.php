<?php
$this->load->view('layout/header');
?>
<link rel="stylesheet" href="<?= base_url('assets/script/pnotify/dist/pnotify.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/script/pnotify/dist/pnotify.buttons.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/script/pnotify/dist/pnotify.nonblock.css') ?>">
<script src="<?= base_url('assets/script/pnotify/dist/pnotify.js') ?>" charset="utf-8"></script>
<script src="<?= base_url('assets/script/pnotify/dist/pnotify.buttons.js') ?>" charset="utf-8"></script>
<script src="<?= base_url('assets/script/pnotify/dist/pnotify.nonblock.js') ?>" charset="utf-8"></script>
<script src="<?= base_url('assets/script/pnotify/dist/pnotify.confirm.js') ?>" charset="utf-8"></script>
<script>
    var table;
    function add_usergroup() {
        $.get("<?php echo base_url(); ?>usergroup/add_data", function (data) {
            $("#maincontent").html(data);
        });
    }

    function edit_usergroup(id_group) {
        $.ajax({
            url: '<?php echo base_url(); ?>usergroup/edit_data',
            type: 'POST',
            data: {id_group: id_group},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function delete_usergroup(id_group, nm_group) {
        var cek = confirm('Apakah anda yakin ingin menghapus Usergroup ' + nm_group + ' ?');
        if (cek) {
            $.post("<?php echo base_url(); ?>/usergroup/delete_data", {id_group: id_group}, function (data) {
                if (data) {
                    $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                    $('#msg').fadeOut(5000);
                    return;
                }
                window.location = "<?php echo base_url(); ?>usergroup";
            });

        }
    }

    function detail_usergroup(id_group) {
        $.post("<?php echo base_url(); ?>usergroup/detail_data", {id_group: id_group},
                function (data) {
                    var datas = JSON.parse(data);
                    var usergroup_detail = datas.usergroup_detail;
                    var list_menu = datas.list_menu;
                    var menu = "";
                    var menunama = "";
                    $('#m_nm_group').val(usergroup_detail.nm_group);
                    $('#m_usergroup_note').val(usergroup_detail.usergroup_note);
                    for (var i in list_menu) {
                        menunama = list_menu[i].menu_desc.replace(/&nbsp;/g, " ");
                        menu += menunama + '  \n';
                    }
                    $('#m_usergroup_menu').val(menu);

                });
    }


    function pengaturan_menu(id_group) {
        $.ajax({
            url: '<?php echo base_url(); ?>usergroup/edit_usergroup_menu',
            type: 'POST',
            data: {id_group: id_group},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

    }

    $(document).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);

        table = $('#usergroup_table').dataTable(
                {
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>usergroup/get_data_paging",
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderable
                        }
                    ]
                }
        );
    });

    //=====================================================test dropdown on datatable ======================
    $('#usergroup_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#usergroup_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

    //============================================================================================================

</script>
<style>
    #usergroup_table .dropdown-menu {
        position: relative;
        width: 100%;
    }
</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> <?php echo $tittle; ?></h4>

            </div>
            <div class="col-sm-2" id="buttonadd"><button onclick="javascript:add_usergroup()" class="btn btn-primary btn-xs button-right">Tambah</button></div>
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class=panel>
<!--            <header class="panel-heading">-->
<!--                <h4>--><?php //echo $tittle; ?><!--</h4>-->
<!--            </header>-->
            <div class="panel-body">

                <table id="usergroup_table" class="table table-bordered responsive" width="100%">
                    <thead>
                        <tr>
                            <th width="5%" style="text-align:center">No</th>
                            <th width="80%">User Group</th>
                            <th width="15%" style="text-align:center"><?php echo $this->config->item('text_btn_action');?></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

    </section>
</section>
<!--=======================================add modal by ardha ================================================================-->
<div class="modal fade bs-example-modal-lg" id="myModal"  role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Detail User Group</h4>
            </div>
            <form id="form_update" data-parsley-validate class="form-horizontal form-label-left">
                <div class="modal-body">
                    <!--=================================== modal content =============================== -->
                    <div class="form-horizontal form-label-left col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">User Group
                            </label>
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <input type="text" autocomplete="off" readonly id="m_nm_group" maxlength="18" class="form-control col-md-7 col-xs-12 required special"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Keterangan
                            </label>
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <textarea autocomplete="off" id="m_usergroup_note" readonly class="form-control col-md-7 col-xs-12 "> </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Menu User Group
                            </label>
                            <div class="col-md-9 col-sm-6 col-xs-12">
                                <textarea  id="m_usergroup_menu" readonly class="form-control col-md-7 col-xs-12" rows="10" > </textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <div class="">
                    <button class="btn btn-default" data-dismiss="modal" type="button"><?php echo $this->config->item('text_btn_close');?></button>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
$this->load->view('layout/footer');
?>
