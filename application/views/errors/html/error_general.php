
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Akses Ditolak - SIAP</title>

    <!-- Bootstrap -->
    <link href="<?= base_url('assets/script/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url('assets/script/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?= base_url('assets/css/custom.min.css') ?>" rel="stylesheet">

    <style media="screen">
      body {
        color: #f6f7f9;
      }
    </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="error-number"><?php echo $status_code; ?></h1>
							<?php if ($status_code == 403): ?>
								<img width="10%" class="img-responsive center-block" src="<?= base_url('assets/images/error_403.png')?>" alt="Error 403">
							<?php endif; ?>
              <h2><?php echo $heading; ?></h2>

              <p>	<?php echo $message; ?><a href="<?= base_url('dashboard') ?>">Kembali Ke Beranda?</a></p>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url('assets/script/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap -->
		<script src="<?= base_url('assets/script/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- Custom Theme Scripts -->
		<script src="<?= base_url('assets/script/custom.js') ?>"></script>
  </body>
</html>
