<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Distributor
  | Date       : 02-05-2017
  =========================================================  -->

<?php
$this->load->view('layout/header');
?>
<script>


    function aktiv(kdsatker) {
        var cek = confirm('Apakah anda yakin ingin mengaktivkan data ini ?');
        if (cek) {
            $.post("<?php echo base_url(); ?>satker/aktiv", {kdsatker: kdsatker}, function (data) {
                if (data !== '') {
                    $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                    $('#msg').fadeOut(5000);
                    return;
                }
                window.location = "<?php echo base_url(); ?>satker";
            });
        }
    }
    function inaktiv(kdsatker) {
        var cek = confirm('Apakah anda yakin ingin menonaktivkan data ini ?');
        if (cek) {
            $.post("<?php echo base_url(); ?>satker/inaktiv", {kdsatker: kdsatker}, function (data) {
                if (data !== '') {
                    $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                    $('#msg').fadeOut(5000);
                    return;
                }
                window.location = "<?php echo base_url(); ?>satker";
            });
        }
    }

    $(document).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);
        var table;
        table = $('#satker_table').dataTable(
                {   'oLanguage': {'sSearch': 'Pencarian',
                "oPaginate": {
                "sPrevious": "Sebelum",
                "sNext": "Lanjut",
                "sLast": "<<",
                "sFirst": ">>"
                            }
		},
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.   
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>satker/get_data_paging",
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderable
                        }
                    ]
                }
        );
    });
    
    $('#employe_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#employe_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

</script>
<style>
	#satker_table .dropdown-menu {
	position: relative;
	width: 100%;
	}   
	</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> <?php echo $tittle; ?></h4>

            </div>
<!--                       <div class="col-sm-2" id="buttonadd"><button onclick="javascript:add_satker()" class="btn btn-primary btn-xs button-right">Tambah</button></div>-->
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class=panel>

            <div class="panel-body">

                <table id="satker_table" class="table table-bordered responsive">
                    <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                            <th>Kode</th>
                            <th>Satker</th>
                            <th>Kewenangan</th>
                            <th style="text-align:center"><?php echo $this->config->item('text_btn_action');?></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<?php
$this->load->view('layout/footer');
?>
