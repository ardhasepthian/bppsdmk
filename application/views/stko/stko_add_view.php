<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : outlet
  | Date       : 02-05-2017
  =========================================================  -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>

<script>

    
    $(document).ready(function () {
    $(".opsi").chosen();
    $("#buttonadd").hide();
    $.validator.setDefaults({ignore: ":hidden:not(select)"});
    $("#form").validate({
            submitHandler: function (form) {
                $.post("<?php echo base_url(); ?>stko/insert_data", {
                    name: $('#name').val().trim(),
                    tittle: $('#tittle').val().trim(),
                    email: $('#email').val().trim(),
                        pid: $('#pid').val().trim(),



                },
                        function (data) {
                            if (data !== '') {
                                $('#msg').show().html(data);
                                $('#msg').fadeOut(5000);
                                return;
                            }
                            alert('Data akan diproses!');
                            window.location="<?php echo base_url()?>stko";

                        });
            }
        });

    });
    
    function cancel() {
        window.location="<?php echo base_url()?>stko";
    }
</script>

<div class="panel">
    <header class="panel-heading" style="padding-top: 5px;">
        <h4>Tambah Struktur</h4>
    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jabatan<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" autocomplete="off" maxlength="100" id="tittle" name="tittle"  class="form-control col-md-7 col-xs-12 required special"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Atasan<span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
<!--                    <input type="text" autocomplete="off" maxlength="50" id="pid" name="pid"  class="form-control col-md-7 col-xs-12  required"/>-->
                    <select class="form-control" name="pid" id="pid">
                        <?php
                       // print_r($pid);
                        foreach ($pid as $k => $v){
                           echo '<option value="'.$v['id'].'">'.$v['tittle'].'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" autocomplete="off" maxlength="50" id="name" name="name"  class="form-control col-md-7 col-xs-12  required"/>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" autocomplete="off" maxlength="60" id="email" name="email"  class="form-control col-md-7 col-xs-12  required"/>
            </div>
        </div>

    </div>
    <div class="panel-footer">
        <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
        <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
    </div>
    </form>
</div>

