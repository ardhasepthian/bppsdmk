<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Distributor
  | Date       : 02-05-2017
  =========================================================  -->


<script>

    function daftar_pos() {
        $.ajax({
            url: '<?php echo base_url(); ?>pos/daftar_pos',
            type: 'POST',
            data: {nopd: $('#nopd').val()},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function add_pos() {
        $.ajax({
            url: '<?php echo base_url(); ?>pos/add_data_pos',
            type: 'POST',
            data: {nopd:$('#nopd').val()},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function edit_pos(pos_code) {
        $.ajax({
            url: '<?php echo base_url(); ?>pos/edit_data',
            type: 'POST',
            data: {pos_code: pos_code , nopd:$('#nopd').val()},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function delete_pos(pos_code, pos_name) {
        var cek = confirm('Apakah anda yakin ingin menghapus pos ' + pos_name + ' ?');
        if (cek) {
            $.post("<?php echo base_url(); ?>pos/delete_data", {pos_code: pos_code}, function (data) {
                if (data !== '') {
                    $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                    $('#msg').fadeOut(5000);
                    return;
                }
                daftar_pos();
               // window.location = "<?php //echo base_url(); ?>pos";
            });
        }
    }
    $(document).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);
        var table;
        table = $('#outlet_table').dataTable(
                {   'oLanguage': {'sSearch': 'Pencarian',
                "oPaginate": {
                "sPrevious": "Sebelum",
                "sNext": "Lanjut",
                "sLast": "<<",
                "sFirst": ">>"
                            }
		},
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.   
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>pos/get_data_paging_detail",
                        "data":{nopd:"<?php echo $nopd; ?>"},
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderablen
                        }
                    ]
                }
        );
    });
    function cancel() {
        window.location = "<?php echo base_url(); ?>pos";
    }
    
    $('#outlet_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#outlet_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

</script>
<style>
	#outlet_table .dropdown-menu {
	position: relative;
	width: 100%;
	}   
	</style>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="panel">
            <div class="panel-body">


                    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">NOPD
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" autocomplete="off" readonly maxlength="255"id="opd_name" name="opd_name" value="<?php echo $form_opd['nopd'] ;?>"  class="form-control col-md-7 col-xs-12 "/>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Objek Pajak
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" autocomplete="off" readonly id="opd_address" maxlength="255" name="opd_address" value="<?php echo $form_opd['opd_name'] ;?>" class="form-control col-md-7 col-xs-12"/>
                                </div>
                            </div>

                        </div>

                    </form>

            </div>
        </div>
        <div class="panel">
            <header class="panel-heading" style="padding-top: 5px;">
                <h4><?php echo $tittle; ?>
                    <button onclick="add_pos()" class="btn btn-xs btn-primary button-right"> Tambah </button> <button onclick="cancel()" class="btn btn-xs btn-success button-right"> Kembali </button>
                </h4>

            </header>
            <div class="panel-body">
                 <input id="nopd" type="hidden" value="<?php echo $nopd;?>">

                <div class="col-sm-12" style="border-bottom: #cacaca solid 1px;">
                    &nbsp;
                </div>
<!--                <div class="col-sm-12" style="border-bottom: #cacaca solid 1px;"></div>-->

                <table id="outlet_table" class="table table-bordered responsive">
                    <thead>
                        <tr>
                            <th style="text-align:center">No</th>

                            <th>Nama POS</th>
                            <th>NOPD</th>
                            <th>Node Id</th>
                            <th style="text-align:center"><?php echo $this->config->item('text_btn_action');?></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>





