<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Distributor
  | Date       : 02-05-2017
  =========================================================  -->

<?php
$this->load->view('layout/header');
?>
<script>
    function add_stko() {
        $.get("<?php echo base_url(); ?>stko/add_data", function (data) {
            $("#maincontent").html(data);
        });
    }

    function edit(id) {
        $.ajax({
            url: '<?php echo base_url(); ?>stko/edit_data',
            type: 'POST',
            data: {id: id},
            success: function (response) {
                $("#maincontent").html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function hapus(id) {
        var cek = confirm('Apakah anda yakin ingin menghapus ?');
        if (cek) {
            $.post("<?php echo base_url(); ?>stko/delete_data", {id: id}, function (data) {
                if (data !== '') {
                    $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                    $('#msg').fadeOut(5000);
                    return;
                }
                window.location = "<?php echo base_url(); ?>employee";
            });
        }
    }




    $(document).ready(function () {
        $('#msgidx').show();
        $('#msgidx').fadeOut(5000);
        var table;
        table = $('#outlet_table').dataTable(
                {   'oLanguage': {'sSearch': 'Pencarian',
                "oPaginate": {
                "sPrevious": "Sebelum",
                "sNext": "Lanjut",
                "sLast": "<<",
                "sFirst": ">>"
                            }
		},
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.   
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url(); ?>stko/get_data_paging",
                        "type": "GET"
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": false //set not orderablen
                        }
                    ]
                }
        );
    });
    
    $('#outlet_table .dropdown-menu').parent().on('shown.bs.dropdown', function () {
        var $menu = $("ul", this);
        offset = $menu.offset();
        position = $menu.position();
        $('body').append($menu);
        $menu.show();
        $menu.css('position', 'absolute');
        $menu.css('top', (offset.top) + 'px');
        $menu.css('left', (offset.left) + 'px');
        $(this).data("myDropdownMenu", $menu);
    });
    $('#outlet_table .dropdown-menu').parent().on('hide.bs.dropdown', function () {
        $(this).append($(this).data("myDropdownMenu"));
        $(this).data("myDropdownMenu").removeAttr('style');

    });

</script>
<style>
	#outlet_table .dropdown-menu {
	position: relative;
	width: 100%;
	}   
	</style>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> POS pada <?php echo $tittle; ?></h4>

            </div>
            <div class="col-sm-2" id="buttonadd"><button onclick="javascript:add_stko()" class="btn btn-primary btn-xs button-right">Tambah</button></div>
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
    <div id="maincontent" class="col-md-12 col-sm-12 col-xs-12">
        <div id="msgidx" style="display:none">
            <?php
            $error = $this->session->flashdata('error');
            $info = $this->session->flashdata('info');
            if (!empty($error)):
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $error; ?>
                </div>
                <?php
            endif;
            if (!empty($info)):
                ?>
                <div class="alert alert-success alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Alert!</b> <?php echo $info; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class=panel>

            <div class="panel-body">

                <table id="outlet_table" class="table table-bordered responsive">
                    <thead>
                        <tr>
                            <th style="text-align:center">No</th>
                            <th>Jabatan</th>
                            <th>Atasan</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th style="text-align:center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Distributor Detail Modal -->
<div id="outlet_detail_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detail outlet</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal col-xs-6">
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Kode outlet
                        </label>
                        <input id="kd_outlet" name="kd_outlet" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Nama outlet
                        </label>
                        <input id="nm_outlet" name="nm_outlet" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Alamat
                        </label>
                        <textarea id="alamat_outlet" name="alamat_outlet" class="col-md-8 col-sm-5 col-xs-12 special" disabled></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Provinsi
                        </label>
                        <input id="nm_prov" name="nm_prov" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Kabupaten
                        </label>
                        <input id="nm_kabkot" name="nm_kabkot" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Kecamatan
                        </label>
                        <input id="nm_kecamatan" name="nm_kecamatan" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Kelurahan
                        </label>
                        <input id="nm_keldes" name="nm_keldes" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    
                    </form>
                    <form class="form-horizontal col-xs-6">
                        
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">No Telepon
                        </label>
                        <input id="msisdn_outlet" name="msisdn_outlet" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">RT
                        </label>
                        <input id="rt" name="rt" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">RW
                        </label>
                        <input id="rw" name="rw" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                        <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Longitude
                        </label>
                        <input id="longitude" name="longitude" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                        <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4 col-xs-12">Latitude
                        </label>
                        <input id="latitude" name="latitude" type="text" class="control-label col-md-8 col-sm-5 col-xs-12" style="text-align:left;" disabled>
                    </div>
                    
                </form>
                
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->config->item('text_btn_close');?></button>
            </div>
        </div>

    </div>
</div>

<?php
$this->load->view('layout/footer');
?>
