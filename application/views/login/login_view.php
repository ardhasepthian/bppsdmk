<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Paradigma Proyek">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">


    <title>ParaPro</title>


    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" />

    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet" />

    <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
<!--    <script src="--><?php //echo base_url(); ?><!--assets/js/respond.min.js"></script>-->

</head>

<body class="login-img3-body">

<div class="container">

    <form class="login-form" action="<?php echo base_url(); ?>login/auth" method="post">
        <div class="login-wrap">
            <p class="login-img">
                <img src="<?php echo base_url(); ?>assets/img/logo.png" width="190px">
                <!-- <i class="icon_lock_alt"></i> -->
            </p>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input type="text" id="username" name="usn" class="form-control" placeholder="Username" autofocus>
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" id="password" name="pwd" class="form-control" placeholder="Password">
            </div>
<!--            <label class="checkbox">-->
<!--                <input type="checkbox" value="remember-me"> Remember me-->
<!--                <span class="pull-right"> <a href="#"> Forgot Password?</a></span>-->
<!--            </label>-->
            <button class="btn  btn-lg btn-block" style="background-color: #b40000; color:#ffffff" type="submit">Login</button>
            <!-- <button class="btn btn-info btn-lg btn-block" type="submit">Signup</button> -->
<!--            <p class="login-img">-->
<!--            <img src="--><?php //echo base_url(); ?><!--assets/img/logobdki.png" width="190px">-->
<!--            </p>-->
        </div>
        <?php
        $error = $this->session->flashdata('error');
        $info = $this->session->flashdata('info');
        if (!empty($error) || !empty($info)):
            ?>
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <b>Alert!</b> <?php echo $error; ?>
            </div>
        <?php endif; ?>
    </form>
    <div class="text-right">
        <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version form: https://bootstrapmade.com/buy/?theme=NiceAdmin
            -->
            <!-- Powered by <a>Synchro</a> -->
        </div>
    </div>
</div>


</body>







</html>
