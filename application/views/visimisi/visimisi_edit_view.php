<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : outlet
  | Date       : 02-05-2017
  =========================================================  -->
<?php
$this->load->view('layout/header');
?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/summernote.min.js"></script>
<link href="<?php echo base_url(); ?>assets/js/summernote.min.css" rel="stylesheet">

<script>
    $(document).ready(function () {
        $('.smnt').summernote({
            placeholder: '',
            tabsize: 2,
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                // ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });

        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {

                $.post("<?php echo base_url(); ?>visimisi/update_data", {
                        visimisi: $('#visimisi').summernote('code'),
                       renstra: $('#renstra').summernote('code')

                },
                        function (data) {
                            if (data !== '') {
                                $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                                $('#msg').fadeOut(5000);
                                return;
                            }
                            alert('Data akan diproses!');
                            alert('Data Berhasil di ubah!');
                            window.location = "<?php echo base_url(); ?>visimisi";
                        });
            }
        });

    });
    
    function cancel() {
        window.location = "<?php echo base_url(); ?>employee";
    }
    
    
</script>
<section id="main-content">
    <section class="wrapper">
        <div class="row garis-header">
            <div class="col-sm-10">
                <h4 class="page-header"><i class="fa fa-laptop"></i> <?php echo $tittle; ?></h4>

            </div>
            <div class="col-sm-2" id="buttonadd"><button onclick="javascript:add_employee()" class="btn btn-primary btn-xs button-right">Tambah</button></div>
        </div>

        <div  class="row">
            <div class="alert alert-danger alert-dismissable" style="display:none" id="msg">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
                <b>Alert!</b>
            </div>
            <div class="main_content">
<div class="panel">

    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">

        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12">Visi Misi <span class="required">*</span>
                </label>
                <div class="col-md-11 col-sm-11 col-xs-12">
                    <textarea class="form-control smnt" name="visimisi" id="visimisi"  rows="20"><?php echo $form['visimisi']?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-1 col-sm-1 col-xs-12">Renstra <span class="required">*</span>
                </label>
                <div class="col-md-11 col-sm-11 col-xs-12">
                    <textarea class="form-control smnt" name="renstra" id="renstra"  rows="20"><?php echo $form['renstra']?></textarea>
                </div>
            </div>






        </div>


    </div>
    <div class="panel-footer">
        <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
        <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
    </div>
    </form>
</div>
            </div>
        </div>