<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : outlet
  | Date       : 02-05-2017
  =========================================================  -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate.js"></script>

<script>
    //function load_combo_kab() {
    //    load_combo_kec();
    //    load_combo_keldes();
    //    var kd_prov = $("#kd_prov").val().trim();
    //    $.ajax({
    //        url: '<?php //echo base_url(); ?>//employee/load_combo_kabkot',
    //        type: 'GET',
    //        data: {kd_prov: kd_prov},
    //        success: function (response) {
    //            var datas = JSON.parse(response);
    //            var combo_kecamatan = '<select id="kd_kabkot" name="kd_kabkot" onchange="load_combo_kec()" class="form-control opsi col-md-6 col-sm-6 col-xs-12 required">';
    //            combo_kecamatan += '<option value="" > Pilih </option>';
    //            for (var i in datas) {
    //                combo_kecamatan += '<option value="' + datas[i].kd_kabkot + '">' + datas[i].nm_kabkot + '</option>';
    //            }
    //            combo_kecamatan += '</select>';
    //            $("#kabkot").html(combo_kecamatan);
    //            $(".opsi").chosen();
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            console.log(textStatus, errorThrown);
    //        }
    //    });
    //}
    //
    //
    //function load_combo_kec() {
    //    load_combo_keldes();
    //    var kd_kabkot = $("#kd_kabkot").val().trim();
    //    $.ajax({
    //        url: '<?php //echo base_url(); ?>//employee/load_combo_kecamatan',
    //        type: 'GET',
    //        data: {kd_kabkot: kd_kabkot},
    //        success: function (response) {
    //            var datas = JSON.parse(response);
    //            var combo_kecamatan = '<select id="kd_kecamatan" name="kd_kecamatan" onchange="load_combo_keldes()" class="form-control opsi col-md-6 col-sm-6 col-xs-12 required">';
    //            combo_kecamatan += '<option value="" > Pilih </option>';
    //            for (var i in datas) {
    //                combo_kecamatan += '<option value="' + datas[i].kd_kecamatan + '">' + datas[i].nm_kecamatan + '</option>';
    //            }
    //            combo_kecamatan += '</select>';
    //            $("#kecamatan").html(combo_kecamatan);
    //            $(".opsi").chosen();
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            console.log(textStatus, errorThrown);
    //        }
    //    });
    //}
    //
    //function load_combo_keldes() {
    //    var kd_kecamatan = $("#kd_kecamatan").val().trim();
    //    if(kd_kecamatan ==''){
    //        var combo_subcateg = '<select id="kd_keldes" nm="kd_keldes" class="form-control opsi col-md-6 col-sm-6 col-xs-12 required">';
    //        combo_subcateg += '<option value="" > Pilih </option></select>';
    //        $("#keldes").html(combo_subcateg);
    //            $(".opsi").chosen();
    //        return;
    //    }
    //    $.ajax({
    //        url: '<?php //echo base_url(); ?>//employee/load_combo_keldes',
    //        type: 'GET',
    //        data: {kd_kecamatan: kd_kecamatan},
    //        success: function (response) {
    //            var datas = JSON.parse(response);
    //            var combo_subcateg = '<select id="kd_keldes" nm="kd_keldes" class="form-control opsi col-md-6 col-sm-6 col-xs-12 required">';
    //            combo_subcateg += '<option value="" > Pilih </option>';
    //            for (var i in datas) {
    //                combo_subcateg += '<option value="' + datas[i].kd_keldes + '">' + datas[i].nm_keldes + '</option>';
    //            }
    //            combo_subcateg += '</select>';
    //            $("#keldes").html(combo_subcateg);
    //            $(".opsi").chosen();
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            console.log(textStatus, errorThrown);
    //        }
    //    });
    //}
    //function load_combo_loc() {
    //    $.ajax({
    //        url: '<?php //echo base_url(); ?>//wpd/load_combo_loc',
    //        type: 'GET',
    //        success: function (response) {
    //            var datas = JSON.parse(response);
    //            var combo_loc = '<select id="loc_ids" name="loc_ids"  class="form-control opsi col-md-6 col-sm-6 col-xs-12 required">';
    //            combo_loc += '<option value="" > Pilih </option>';
    //            combo_loc += '<option value="0##PUSAT" > PUSAT </option>';
    //            for (var i in datas) {
    //                combo_loc += '<option value="' + datas[i].loc_id +'##'+ datas[i].loc_name +'">' + datas[i].loc_name + '</option>';
    //            }
    //            combo_loc += '</select>';
    //            $("#combo_loc").html(combo_loc);
    //            $(".opsi").chosen();
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            console.log(textStatus, errorThrown);
    //        }
    //    });
    //}


    $(document).ready(function () {
        // load_combo_loc()
        $("#buttonadd").hide();
    $(".opsi").chosen();
        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {
               // $.blockUI({ message: 'Mohon tunggu' });
               //  var xx = $("#loc_ids").val();
               //  var loc = xx.split("##");

                $.post("<?php echo base_url(); ?>employee/insert_data", {
                        nm_pegawai: $('#nm_pegawai').val().trim(),
                        nip: $('#nip').val().trim(),
                        // loc_id :loc[0],
                        // loc_name :loc[1],
                        tlp_pegawai: $('#tlp_pegawai').val().trim(),
                        email_pegawai: $('#email_pegawai').val().trim()
                },
                        function (data) {
                            if (data !== '') {
                                $.blockUI({ message: 'Mohon tunggu' });$('#msg').show().html(data);
                                $('#msg').fadeOut(5000);
                                return;
                            }
                            alert('Data akan di proses');
                            window.location = "<?php echo base_url(); ?>employee";
                        });
            }
        });

    });
    
    function cancel() {
        window.location = "<?php echo base_url(); ?>employee";
    }
</script>
<div class="panel">
    <header class="panel-heading">
        <h4>Tambah Pegawai</h4>

    </header>
    <form id="form" data-parsley-validate class="form-horizontal form-label-left">
    <div class="panel-body">

            <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">NIP <span class="required">*</span>
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" autocomplete="on" id="nip" maxlength="30" name="nip"  class="form-control col-md-7 col-xs-12 required special"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama <span class="required">*</span>
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" autocomplete="on" id="nm_pegawai" maxlength="100" name="nm_pegawai"  class="form-control col-md-7 col-xs-12 required special"/>
                </div>
            </div>
<!--                <div class="form-group">-->
<!--                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Bandara</label>-->
<!--                    <div  id="combo_loc" class="col-md-8 col-sm-8 col-xs-12">-->
<!--                        <select id="loc_ids"  name="loc_ids" class="form-control opsi col-md-7 col-xs-12 required">-->
<!--                            <option value="" > Pilih </option>-->
<!---->
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->





    </div>
    <div class="col-sm-6">


            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" autocomplete='email' id="email_pegawai" maxlength="100" name="email_pegawai"  class="form-control col-md-7 col-xs-12 email "/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">No Hp <span class="required">*</span>
                </label>
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" autocomplete="on" id="tlp_pegawai" maxlength="15" name="tlp_pegawai" class="form-control col-md-7 col-xs-12 required number"/>
                </div>
            </div>

            </div>

    </div>
    <div class="panel-footer">
        <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
        <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
    </div>
    </form>
</div>

