<!-- =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : distributor
  | Date       : 02-05-2017
  =========================================================  -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.validate_1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/script/jquery.blockUI.js"></script>
<script>
    function refreshss() {
        $.get("<?php echo base_url(); ?>pasar/upload_excel", function (data) {
            $("#maincontent").html(data);
        });
    }
    function load_combo_kab() {
        var kd_prov = $("#kd_prov").val().trim();
        $.ajax({
            url: '<?php echo base_url(); ?>pasar/load_combo_kabkot',
            type: 'GET',
            data: {kd_prov: kd_prov},
            success: function (response) {
                var datas = JSON.parse(response);
                var combo_kecamatan = '<select id="kd_kabkot" name="kd_kabkot" onchange="load_combo_kec()" class="form-control opsi col-md-6 col-sm-6 col-xs-12 required">';
                combo_kecamatan += '<option value="" > Pilih </option>';
                for (var i in datas) {
                    combo_kecamatan += '<option value="' + datas[i].kd_kabkot + '">' + datas[i].nm_kabkot + '</option>';
                }
                combo_kecamatan += '</select>';
                $("#kabkot").html(combo_kecamatan);
                $(".opsi").chosen();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }
    
    $(document).ready(function () {
    $(".opsi").chosen();
        $.validator.setDefaults({ignore: ":hidden:not(select)"});
        $("#form").validate({
            submitHandler: function (form) {
                $.blockUI({ message :'<img src="<?php echo base_url(); ?>assets/images/loader.gif" width="50px">' });
                   var kd_prov = $('#kd_prov').val().trim();
                   var kd_kabkot= $('#kd_kabkot').val().trim();
                   
                var fd = new FormData();
                var file_data = $('input[type="file"]')[0].files[0];
                fd.append("file", file_data);
                var other_data = $('form').serializeArray();
                $.each(other_data, function (key, input) {
                    fd.append(input.name, input.value);
                });
                
                
                fd.append("kd_prov",kd_prov);
                fd.append("kd_kabkot", kd_kabkot);
                
                
                $.ajax({
            url: '<?php echo base_url(); ?>pasar/do_upload',
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                if (data !== '') {
                    $.unblockUI();
                    $("#msg").html(data).show();
                    $('#msg').fadeOut(5000);
                    return;
                }
                $.unblockUI();
                alert('Data akan diproses !');
                cancel();
            }
        });
            }
        });

    });
    
    function cancel() {
        window.location = "<?php echo base_url(); ?>pasar";
    }
</script>
<div class=panel>
    <div class="x_title">
        <h2>Upload Excel Tambah Distributor</h2>
        <ul class="nav navbar-right panel_toolbox">
            <!--<button onclick="refreshss()">rfs</button>-->
            <li><a class="collapse-link" href="<?php echo base_url()?>distributor/dload_file">
                            <button class="btn btn-sm btn-danger">Contoh File</button>
                </a>
            </li>
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
        </ul>
        <div class="clearfix"></div>
        <div class="alert alert-danger alert-dismissable" id="msg" style="display:none">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
            <b>Alert!</b>
        </div>
    </div>
    <div class="panel-body">
        <form id="form" data-parsley-validate class="form-horizontal form-label-left">
           
            <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Provinsi *</label>
                    <div  class="col-md-6 col-sm-6 col-xs-12">
                        <select id="kd_prov" onchange="load_combo_kab()" name="kd_prov" class="form-control opsi col-md-7 col-xs-12 required">
                            <option value="" > Pilih </option>
                            <?php
                            foreach ($combo_prov as $unit) {
                                echo '<option value="' . $unit['kd_prov'] . '">' . $unit['nm_prov'] . ' </option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kab/Kota *</label>
                    <div  id="kabkot" class="col-md-6 col-sm-6 col-xs-12">
                        <select id="kd_kabkot"  name="kd_kabkot" class="form-control opsi col-md-7 col-xs-12 required">
                            <option value="" > Pilih </option>
                            
                        </select>
                    </div>
            </div>
            
            
            <div class="form-group" id="upload">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Upload File<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file"  id="fupload_file"  accept=".xls,.xlsx,.csv"  name="upload_file" class="required"/>
                    </div>
            </div>
            
            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <button class="btn btn-success" onclick="javasrcipt:cancel();" type="button"><?php echo $this->config->item('text_btn_cancel');?></button>
                    <button type="submit" class="btn btn-danger"><?php echo $this->config->item('text_btn_submit');?></button>
                </div>
            </div>

        </form>
    </div>
</div>
