<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : Muhamad Ircham
  | Email      :
  | Class name : My_Controller
  | Date       : 09-04-2017 15:41:18
  ========================================================= */

/**
 * @property Util_model $util_model Optional description
 * @property Rk_tahunan_head_model $rk_tahunan_head_model
 * @property Pelaporan_audit_model $pelaporan_audit_model
 * @property Pelaporan_audit_approval_model $pelaporan_audit_approval_model
 * @property Notifikasi_model $notifikasi_model
 * @property Dashboard_model $dashboard_model
 * @property Kegiatan_non_pengawasan_model $kegiatan_non_pengawasan_model
 * @property Monitoring_tindak_lanjut_model $monitoring_tindak_lanjut_model
 * @property Tl_eks_monitoring_model $tl_eks_monitoring_model
 * @property Profile_model $profile_model
 * @property Report_absensi_model $report_absensi_model
 * @property Report_tlhp_model $report_tlhp_model
 * @property Surat_tugas_model $surat_tugas_model
 * @property Perencanaan_evaluasi_model $perencanaan_evaluasi_model
 * @property Tl_int_approval_folup_model $tl_int_approval_folup_model
 * @property Tl_eks_approval_satker_actionplan_model $tl_eks_approval_satker_actionplan_model
 * @property Tl_eks_send_folup_model $tl_eks_send_folup_model
 * @property Tl_eks_bpk_simulasi_model $tl_eks_bpk_simulasi_model
 * @property Tl_eks_actionplan_model $tl_eks_actionplan_model
 * @property Pegawai_local_model $pegawai_local_model
 * @property Pegawai_model $pegawai_model
 * @property Inbox_model $inbox_model
 * @property Profres_satker_model $profres_satker_model
 * @property Pertanggung_jawaban_spt_model $pertanggung_jawaban_spt_model
 * @property Monitoring_pertanggung_jawaban_spt_model $monitoring_pertanggung_jawaban_spt_model
 * @property Kendali_mutu_audit_model $kendali_mutu_audit_model
 * @property Monitoring_pembuatan_surat_tugas_model $monitoring_pembuatan_surat_tugas_model
 * @property Perencanaan_tindak_lanjut_model $perencanaan_tindak_lanjut_model
 * @property Pelaksanaan_tindak_lanjut_model $pelaksanaan_tindak_lanjut_model Optional description
 * @property Pelaporan_tindak_lanjut_model $pelaporan_tindak_lanjut_model Optional description
 * @property Pelaporan_tindak_lanjut_approval_model $pelaporan_tindak_lanjut_approval_model Optional description
 * @property Login_model $login_model Optional description
 * @property Sequence_code_model $sequence_code_model Optional description
 * @property Sbm_model $sbm_model Optional description
 * @property Perencanaan_reviu_model $perencanaan_reviu_model Optional description
 * @property Perencanaan_audit_model $perencanaan_audit_model Optional description
 * @property Perencanaan_lainnya_model $perencanaan_lainnya_model Optional description
 * @property Monitoring_draft_kegiatan_model $monitoring_draft_kegiatan_model Optional description
 * @property Pelaporan_lainnya_model $pelaporan_lainnya_model Optional description
 * @property Pelaporan_lainnya_approval_model $pelaporan_lainnya_approval_model Optional description
 */
class MY_Controller extends CI_Controller {

	protected $PROFILER = false;

    protected $controller = '';
    protected $function = '';
    protected $ip = '';
    protected $browser = '';
    protected $bypass_controller = array(
        'forum'
    );

    public function __construct() {
        parent::__construct();

        if ($this->session->userdata('efiskus_sessionid') != NULL) {
            $this->load->library('user_agent');

            // get controller
            $this->controller = strtolower($this->router->class);
            $this->function = $this->router->method;
            $this->ip = $this->input->ip_address();

            if ($this->agent->is_browser()) {
                $agent = $this->agent->browser() . ' - versi ' . $this->agent->version();
            } elseif ($this->agent->is_robot()) {
                $agent = $this->agent->robot();
            } elseif ($this->agent->is_mobile()) {
                $agent = $this->agent->mobile();
            } else {
                $agent = 'Unidentified User Agent';
            }

            $this->browser = $agent . " | " . $this->agent->platform();

            if ($this->session->userdata('efiskus_sessionid') == NULL) {
                redirect(base_url() . 'login/signout');
            }

            if (ENVIRONMENT === 'development' && !$this->input->is_ajax_request()) {
	            $this->output->enable_profiler($this->PROFILER);
            }

            if(!$this->session->userdata('efiskus_ip') && !$this->session->userdata('efiskus_browser')) {
                $this->session->set_userdata(array(
                    'efiskus_ip' => $this->ip,
                    'efiskus_browser' => $this->browser,
                ));
            }


			$this->load->model('util_model');
            $id_group = $this->session->userdata('efiskus_id_group');
            $arr['menu_controller'] = $this->controller;
            $arr['id_group'] = $id_group;
            $resultcek = $this->util_model->cek_bypass_controller($this->controller);
            if(intval($resultcek) == 0) {
                $cek = $this->util_model->cek_modul_authorize($arr);
                if ($cek == 0) {
                    show_error("Nampaknya kamu tidak mempunyai akses kehalaman ini, silahkan hubungi admin atau", 403, "Akses Ditolak");
                }
            }

        } else {
            redirect(base_url() . 'login/signout');
        }
    }

    public function generate_audittrail($param) {
        $this->load->model('util_model');
        $arr['nip'] = $this->session->userdata('efiskus_nip');
        $arr['controller'] = strtolower(trim($param['controller']));
        $arr['function'] = strtolower(trim($param['function']));
        $arr['ip'] = $this->ip;
        $arr['browser'] = $this->browser;
        $arr['audittrail_sts'] = $param['audittrail_sts'];
        $this->util_model->insert_audittrail($arr);
    }

    public function get_menu_gps($controller) {

        $this->load->model('util_model');
        $result = $this->util_model->getmenugps($controller);

        return $result;
    }

    /**
     * @param $plain_text
     * @return string
     */
    public function encrypt_code($plain_text) {
        $this->load->library('encryption');
        $this->encryption->initialize(
            array(
                'cipher' => 'aes-256',
                'mode' => 'ctr',
                'key' => 'sadSAsa1194gjasJu398FDds12'
            )
        );
        $ciphertext = $this->encryption->encrypt($plain_text);
        $replace = strtr(
            $ciphertext,
            array(
                '!' => '_',
                '+' => '.',
                '=' => '-',
                '/' => '~'
            )
        );

        return $replace;
    }

    /**
     * @param $ciphertext
     * @return mixed
     */
    public function decrypt_code($ciphertext) {
        $this->load->library('encryption');
        $this->encryption->initialize(
            array(
                'cipher' => 'aes-256',
                'mode' => 'ctr',
                'key' => 'sadSAsa1194gjasJu398FDds12'
            )
        );
        $unreplace = strtr(
            $ciphertext,
            array(
                '_' => '!',
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        );

        return $this->encryption->decrypt($unreplace);
    }

    /**
     * do_upload_multiple_files - Multiple Methods
     * @param $fieldName
     * @param $options
     * @return array
     */
    public function do_upload_multiple_files($fieldName, $options, $filename) {

        $response = array();
        $files = $_FILES;
        $cpt = count($_FILES[$fieldName]['name']);
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES[$fieldName]['name']= $files[$fieldName]['name'][$i];
            $_FILES[$fieldName]['type']= $files[$fieldName]['type'][$i];
            $_FILES[$fieldName]['tmp_name']= $files[$fieldName]['tmp_name'][$i];
            $_FILES[$fieldName]['error']= $files[$fieldName]['error'][$i];
            $_FILES[$fieldName]['size']= $files[$fieldName]['size'][$i];

            $options['file_name'] = $filename . '-' . $_FILES[$fieldName]['name'];

            $this->load->library('upload');
            $this->upload->initialize($options);

            //upload the image
            if (!$this->upload->do_upload($fieldName)) {
                $response['error'][] = $this->upload->display_errors();
            } else {
                $response['result'][] = $this->upload->data();
            }
        }

        return $response;
    }

    public function get_spt($spt_code, $reset = NULL, $nip = NULL) {
        if ($this->session->userdata('efiskus_spt') == NULL || $reset != NULL) {
            $this->load->model('util_model');

            if($nip) {
                $spt_data = $this->util_model->get_spt($spt_code, $nip);
            } else {
                $spt_data = $this->util_model->get_spt($spt_code);
            }
            $this->session->set_userdata(array(
                'efiskus_spt' => $spt_data
            ));
        } else {
            $spt_data = $this->session->userdata('efiskus_spt');
        }

        return $spt_data;
    }

    public function replace_content($content, $arrvar) {

        foreach ($arrvar as $k => $v) {
            $pos = stripos($content, $k); // $pos = 7, not 0

            if ($pos > 0) {

                $content = str_replace($k, $v, $content);
            }
        }

        return $content;
    }

    public function clean_special($string) {
        return strtr(
            strtolower($string),
            array(
                ' ' => '-',
                '+' => '-',
                '=' => '-',
                '/' => '-',
                '(' => '-',
                ')' => '-',
                '&' => '-',
                '%' => '-',
                '#' => '-',
                '!' => '-',
                '@' => '-',
                '$' => '-',
                '^' => '-',
                '*' => '-',
                '.' => '-',
                '`' => '-'
            )
        );
    }

    public function clean_url($string) {
        return strtr(
            strtoupper($string),
            array(
                ' ' => '-',
                '+' => '-',
                '=' => '-',
                '/' => '-',
                '(' => '-',
                ')' => '-',
                '&' => '-',
                '%' => '-',
                '#' => '-',
                '!' => '-',
                '@' => '-',
                '$' => '-',
                '^' => '-',
                '*' => '-',
                '`' => '-'
            )
        );
    }

    public function upload_data($file_post, $upload_path, $filename, $allowed_types = NULL) {
        $config['upload_path'] = $upload_path;
        if(!file_exists($config['upload_path'])) {
            mkdir($config['upload_path']);
        }
        $config['allowed_types'] = 'xls|xlsx|csv|doc|docx|pdf|png|jpg|jpeg|ppt|pptx';
        $config['max_size'] = $this->config->item('max_upload');
        $config['overwrite'] = false;
        $config['file_name'] = $filename;

        if($allowed_types) {
            $config['allowed_types'] = $allowed_types;
        }

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($file_post)) {
            $result['error'] = $this->upload->display_errors();
        } else {
            $result = $this->upload->data();
        }

        return $result;
    }

    public function response($data = [], $code = 200){
        // Disable Debuggin for response JSON
        $this->output->enable_profiler(false);
        $this->output->set_status_header($code);
        $this->output->set_content_type('application/json')
                                 ->set_output(json_encode($data));
    }

    protected function dd($data, $dump = true){
			echo "<pre>";
			if($dump){
				var_dump($data);
			}else{
				print_r($data);

			}
			echo "</pre>";
			exit;
		}

    protected function time_diff($time1, $time2)
    {
        $time1 = strtotime($time1);
        $time2 = strtotime($time2);

        return date("H:i:s", strtotime("1980-01-01 00:00:00") + ($time1 - $time2));

    }
}
