<?php

/**
 * Created by PhpStorm.
 * User: ororimac
 * Date: 4/26/17
 * Time: 7:27 AM
 */
class MY_Model extends CI_Model
{

    protected $_table_name = '';
    protected $_primary_key = '';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    protected $_order_type = '';
    protected $_rules = array();
    protected $_field = array();
    protected $_timestamps = FALSE;
    protected $_controller = '';
    protected $_function = '';
    protected $_nip = '';
    protected $_now = '';

    function __construct() {
        parent::__construct();
        $this->_nip = $this->session->userdata('simpati_nip');
        $this->_now = date('Y-m-d H:i:s');
    }

    public function get_list($search = NULL, $limit = NULL, $offset = NULL, $field = NULL, $where = NULL)
    {

        $this->db->where('is_active', true);

        if (!empty($search) && count($search) > 0) {
            $this->db->where("upper($field) like upper('%$search%')");
        }
        if($where != NULL) {
            $this->db->where($where);
        }
        if (!empty($limit)) {
            $this->db->limit($limit, $offset);
        }
        $this->db->from($this->_table_name);
        $this->db->order_by($this->_order_by, $this->_order_type);

//        echo $this->db->get_compiled_select();

        return $this->db->get()->result_array();
    }

    public function get_count($search, $field)
    {
        $this->db->where('is_active', true);

        if (!empty($search) && count($search) > 0) {
            $this->db->where("upper($field) like upper('%$search%')");
        }
        $this->db->from($this->_table_name);
        $this->db->order_by($this->_order_by, $this->_order_type);

        return $this->db->count_all_results();
    }

    public function count($where)
    {
        $this->db->from($this->_table_name);
        $this->db->where($where);

        return $this->db->count_all_results();
    }

    public function save_data($data, $id = NULL, $serial = NULL, $audit = NULL, $info = NULL)
    {
        $now = $this->_now;
        $nip = $this->_nip;

        // Insert
        if ($id === NULL) {
            if($serial != NULL) {
                $sequence_number = $this->get_sequence_number($this->_table_name);
                $where = array($this->_primary_key => $sequence_number);

                while (intval($this->count($where)) > 0) {
                    $sequence_number = $this->get_sequence_number($this->_table_name);
                }

                $data[$this->_primary_key] = $sequence_number;
            }

            if($audit != NULL) {
                $id = 0;

                $param_audit['nip'] = $this->_nip;
                $param_audit['controller'] = strtolower(trim($this->_controller));
                $param_audit['function'] = strtolower(trim($this->_function));
                $param_audit['ip'] = $this->session->userdata('simpati_ip');
                $param_audit['browser'] = $this->session->userdata('simpati_browser');

                $cek_data = $this->count($data);

                if (intval($cek_data) > 0) {
                    $param_audit['audittrail_sts'] = 0;
                    $this->session->set_flashdata('error', $info . ' sudah terdaftar');
                } else {
                    $param_audit['audittrail_sts'] = 1;

                    $data['create_date'] = $now;
                    $data['create_by'] = $nip;

                    $this->db->set($data);
                    $this->db->insert($this->_table_name);

                    $id = $this->db->insert_id();

                    $this->session->set_flashdata('info', 'Data berhasil disimpan!!');
                }

                $this->generate_audittrail($param_audit);
            } else {
                $data['create_date'] = $now;
                $data['create_by'] = $nip;

                $this->db->set($data);
                $this->db->insert($this->_table_name);

                $id = $this->db->insert_id();
            }
        }
        // Update
        else {
            $data['update_date'] = $now;
            $data['update_by'] = $nip;

            $filter = $this->_primary_filter;
            if($filter == 'intval') {
                $id = $filter($id);
            }
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name);
        }

        return $id;
    }

    public function update_data($data, $where = NULL)
    {
        $now = date('Y-m-d H:i:s');
        $nip = $this->session->userdata('simpati_nip');

        $data['update_date'] = $now;
        $data['update_by'] = $nip;

        $this->db->set($data);

        if($where !== NULL) {
            $this->db->where($where);
        }
        $this->db->update($this->_table_name);

        return $this->db->error();
    }

    public function delete_data($id)
    {
        $now = date('Y-m-d H:i:s');
        $nip = $this->session->userdata('simpati_nip');

        $data['is_active'] = false;
        $data['update_date'] = $now;
        $data['update_by'] = $nip;

        $filter = $this->_primary_filter;
        $id = $filter($id);
        $this->db->set($data);
        $this->db->where($this->_primary_key, $id);
        $this->db->update($this->_table_name);
    }

    public function get($id = NULL, $single = FALSE, $where = NULL, $limit = NULL, $offset = NULL, $select = NULL, $join = NULL, $order_by = NULL, $dump = NULL)
    {
        if ($id != NULL) {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
        }

        if($where != NULL) {
            $this->db->where($where);
        }

        if($single == TRUE) {
            $method = 'row_array';
        }
        else {
            $method = 'result_array';
        }

        if($limit != NULL) {
            $this->db->limit($limit, $offset);
        }

        if ($select != NULL) {
            $this->db->select($select);
        }

        if($join != NULL){
            $this->db->from($this->_table_name, FALSE);

            if(is_array($join['table'])){
                foreach ($join['table'] as $key => $value) {
                    $tabel = $value;
                    $selector = $join['condition'][$key];
                    $type = $join['type'][$key];

                    $this->db->join($tabel, $selector, $type);
                }
                if($order_by != NULL) {
                    $order_field = $this->_order_by;
                    $order_type = $this->_order_type;

                    $exp_field = explode(',', $order_field);
                    $exp_type = explode(',', $order_type);
//                    var_dump($exp_field);

                    foreach ($exp_field as $key => $item) {
                        $this->db->order_by($item, $exp_type[$key]);
                    }
//                    $this->db->order_by($this->_order_by, $this->_order_type);
                }
            }else{
                $this->db->join($join['table'], $join['condition'], $join['type']);
            }
//            if($this->_table_name == 'forum_mst_thread_reply') echo $this->db->get_compiled_select() . "<br>";

            if($dump != NULL) {
                echo $this->db->get_compiled_select();
            }

            return $this->db->get()->$method();
        }else{
            if($order_by != NULL) {
                $this->db->order_by($this->_order_by, $this->_order_type);
            }

            if($dump != NULL) {
                echo $this->db->get_compiled_select();
            }

            return $this->db->get($this->_table_name)->$method();
        }
    }

    public function get_last_insert() {
        $this->db->limit(1);
        $this->db->order_by($this->_order_by, 'DESC');

//        echo $this->db->get_compiled_select();
        return $this->db->get($this->_table_name)->row_array();
    }

    public function get_sequence_number($tablename)
    {
        $tablename = strtoupper($tablename);
        $sequence = 0;
        $this->db->trans_begin();
        $sql = "select (sequence_number) sequence_number
                from simpati_mst_sequence
                where 1 = 1
                and upper(sequence_table) = '$tablename'
                and sequence_sts = 1";
        $result = $this->db->query($sql)->row();
        $sequence = $result->sequence_number;

        if (!empty($sequence)) {
            $sequence = intval($sequence) + 1;
            $sql_sequence = "update simpati_mst_sequence set sequence_number = $sequence
                    where upper(sequence_table) = '$tablename'";
            $this->db->query($sql_sequence);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        return $sequence;
    }

    public function generate_code($column)
    {
        $code = "";
        $column = strtoupper(trim($column));
        $this->db->trans_begin();
        $sql = "select key_id, key_code, key_table, key_column, key_prefix, key_seq, key_length from simpati_mst_code_generator where key_column = '$column' and key_sts = 1 ";
        $result = $this->db->query($sql)->row_array();

        if (!empty($result)) {
            $sequence = intval($result['key_seq']) + 1;

            $sql = "update simpati_mst_code_generator set key_seq = $sequence
                    where key_column = '$column'";
            $this->db->query($sql);

            $prefix = $result['key_prefix'];
            $length = intval($result['key_length']);

            $sufix = "";

            if (strlen($sequence) < $length) {
                $sufix = str_pad($sequence, $length, '0', 0);
            } else {
                $sufix = $sequence;
            }
            $code = $prefix . $sufix;
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return $code;
    }

    public function generate_audittrail($arr){
        $sql = "insert into simpati_mst_audittrail (audittrail_date, nip, controller, function, audittrail_sts, ip, browser) values
                (now(), '" . $arr['nip'] . "', '" . $arr['controller'] . "', '" . $arr['function'] . "', " . $arr['audittrail_sts'] . ", '" . $arr['ip'] . "', '" . $arr['browser'] . "')";

        $this->db->query($sql);
    }

    public function insert_batch($data) {
        return $this->db->insert_batch($this->_table_name, $data);
    }

    public function get_data($table_name, $where, $single = NULL, $select = NULL) {
        if($select != NULL) {
            $this->db->select($select);
        }
        if($single == TRUE) {
            $method = 'row_array';
        }
        else {
            $method = 'result_array';
        }
        $this->db->where($where);
        return $this->db->get($table_name)->$method();
    }

    public function edit_data($table_name, $data, $where) {
        $nip = $this->session->userdata('simpati_nip');
        $now = date('Y-m-d H:i:s');

        $data['update_date'] = $now;
        $data['update_by'] = $nip;

        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table_name);

        return $this->db->error();
    }

    public function insert_data($table_name, $data) {
        $nip = $this->session->userdata('simpati_nip');
        $now = date('Y-m-d H:i:s');

        $data['create_date'] = $now;
        $data['create_by'] = $nip;

        $this->db->set($data);
        $this->db->insert($table_name);

        return $this->db->insert_id();
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->_table_name;
    }

    /**
     * @param string $table_name
     */
    public function setTableName($table_name)
    {
        $this->_table_name = $table_name;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->_primary_key;
    }

    /**
     * @param string $primary_key
     */
    public function setPrimaryKey($primary_key)
    {
        $this->_primary_key = $primary_key;
    }

    /**
     * @return string
     */
    public function getPrimaryFilter()
    {
        return $this->_primary_filter;
    }

    /**
     * @param string $primary_filter
     */
    public function setPrimaryFilter($primary_filter)
    {
        $this->_primary_filter = $primary_filter;
    }

    /**
     * @return string
     */
    public function getOrderBy()
    {
        return $this->_order_by;
    }

    /**
     * @param string $order_by
     */
    public function setOrderBy($order_by)
    {
        $this->_order_by = $order_by;
    }

    /**
     * @return string
     */
    public function getOrderType()
    {
        return $this->_order_type;
    }

    /**
     * @param string $order_type
     */
    public function setOrderType($order_type)
    {
        $this->_order_type = $order_type;
    }

    /**
     * @return array
     */
    public function getField()
    {
        return $this->_field;
    }

    /**
     * @param array $field
     */
    public function setField($field)
    {
        $this->_field = $field;
    }

    /**
     * @param string $controller
     */
    public function setController($controller)
    {
        $this->_controller = $controller;
    }

    /**
     * @param string $function
     */
    public function setFunction($function)
    {
        $this->_function = $function;
    }

    /**
     * @param array $table
     */
    public function setTable($table)
    {
        $this->_table_name  = $table[0];
        $this->_primary_key = $table[1];
        $this->_order_by    = $table[2];
        $this->_order_type  = $table[3];
    }
}