<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon/favicons.ico" />

        <title>EFISKUS</title>

        <link href="<?php echo base_url(); ?>assets/script/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/script/font-awesome-4/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/script/select2/css/select2.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/script/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet">
        <?php if ($this->session->userdata('efiskus_theme')) { ?>
            <link href="<?php echo base_url(); ?>assets/css/theme-<?php echo $this->session->userdata('efiskus_theme'); ?>.css" rel="stylesheet">
        <?php } else { ?>
            <link href="<?php echo base_url(); ?>assets/css/theme-darkblue.css" rel="stylesheet">
        <?php } ?>
        <link rel="stylesheet" href="<?= base_url('assets/css/siap.css') ?>">
        <link href="<?php echo base_url(); ?>assets/css/profiler.css" rel="stylesheet">

        <script id="fr-fek">
            try {
                (function (k) {
                    localStorage.FEK = k;
                    t = document.getElementById('fr-fek');
                    t.parentNode.removeChild(t);
                })('eQZMe1NJGC1HTMVANU==')
            } catch (e) {

            }
        </script>
        <style>
            .notif .dropdown-menu li a {
                width: 100%;
                padding: 12px 0px !important;
            }
        </style>

        <!-- New Notification, see custom.js for reference -->
        <link rel="stylesheet" href="<?= base_url('assets/script/pnotify/dist/pnotify.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/script/pnotify/dist/pnotify.buttons.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/script/pnotify/dist/pnotify.nonblock.css') ?>">
    </head>
    <?php
    $nama = $this->session->userdata('efiskus_nama');
    ?>
    <body class="nav-md footer_fixed">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">

                                <a href="#" class="site_title">
                                    <h1>&nbsp;&nbsp;&nbsp;&nbsp; Efiskus</h1>
<!--                                    <img src="--><?php //echo base_url(); ?><!--assets/images/logo1.png" width="150px" alt="" class="img-md " style="margin-top:5px;">-->
<!---->
<!--                                    <img src="--><?php //echo base_url(); ?><!--assets/images/user.png" width="150px" alt="" class="img-sm sembunyi img-responsive">-->
                                </a>

                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="<?php echo base_url(); ?>assets/images/user.jpg" alt="..." class="img-circle profile_img" >
                            </div>
                            <div class="profile_info">
                                <span>Hai,</span>
                                <h2>
                                    <b>
                                        <?php echo ucfirst($this->session->userdata('efiskus_nama')); ?>
                                    </b>
                                </h2>
                                <p><?php echo date('j F Y'); ?></p>
                                <p><?php echo $this->session->userdata('efiskus_kode'); ?></p>
                            </div>
                        </div>

                        <!--====================================sidebar menu ======================================== -->
                        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" >
                            <div class="menu_section" >
                                <ul class="nav side-menu" >
                                  <li><a href="<?= base_url('dashboard') ?>"><i class="fa fa-home"></i>Home</span></a></li>
                                    <!-- start of dynamic menu  -->
                                    <?php
                                      $group =strtoupper($this->session->userdata('efiskus_nm_group'));

                                      if ($group == 'SUPER ADMINISTRATOR' || $group == 'ADMIN' || $group == 'PIMPINAN' || explode(' ', $group)[0] == 'PIMPINAN' || explode(' ', $group)[0] == 'ADMIN') {
                                        $arr['id_group'] = $this->session->userdata('efiskus_id_group');
                                        $arrlistmenuparent = $this->util_model->get_list_parent($arr);
                                        if ($arrlistmenuparent != FALSE) {
                                            foreach ($arrlistmenuparent as $row):
                                                $arr['menu_parent_id'] = $row['menu_id'];
                                                $cek = $this->util_model->cek_child($arr);
                                                // var_dump($arr); exit;
                                                if ($cek > 0) {
                                                    echo '<li><a><i class="fa fa-' . $row['menu_icon'] . '"></i> ' . $row['menu_desc'] . ' <span class="fa fa-chevron-down"></span></a>';
                                                    echo '<ul class="nav child_menu">';
                                                    $arrlistmenuchild = $this->util_model->get_list_child($arr);
                                                    foreach ($arrlistmenuchild as $rowchild):
                                                        echo '<li><a href="' . base_url() . $rowchild['menu_controller'] . '"><i class="fa fa-'.$rowchild['menu_icon'].'"></i>' . $rowchild['menu_desc'] . '</a></li>';
                                                    endforeach;
                                                    echo '</ul>';
                                                    echo '</li>';
                                                }else {
                                                    echo '<li><a href="' . base_url() . $row['menu_controller'] . '"><i class="fa fa-' . $row['menu_icon'] . '"></i> ' . $row['menu_desc'] . ' <span class="fa"></span></a></li>';
                                                }
                                            endforeach;
                                        }
                                      }
                                    ?>
                                    <!-- end of dynamic menu  -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="top_nav">
                    <div class="nav_menu">
                        <nav>
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <b><?php echo ucfirst(strtolower($this->session->userdata('efiskus_nm_group'))); ?></b>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">


                                        <li><a href="javascript:changepass();"><i class="fa fa-lock pull-right"></i><b> Ubah password</b></a></li>
<!--                                        <li><a href="--><?php //echo base_url()?><!--dashboard/dload_file?gn=--><?php //echo $this->session->userdata('efiskus_nm_group');?><!--"><i class="fa fa-download pull-right"></i><b>Panduan User</b></a></li>-->
                                        <li><a href="javascript:logout();"><i class="fa fa-sign-out pull-right"></i><b> Log Out</b></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--=====================================Script load======================================-->

                <script src="<?php echo base_url(); ?>assets/script/jquery/dist/jquery.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/blockui/jquery.blockUI.js"></script>
                <script src="<?= base_url('assets/script/jqueryui/jquery-ui.min.js') ?>" charset="utf-8"></script>
                <script src="<?php echo base_url(); ?>assets/script/bootstrap/dist/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/moment/min/moment.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/select2/js/select2.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/datepicker/bootstrap-datepicker.id.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/datatables.net/js/jquery.dataTables.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/input_mask/jquery.inputmask.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/chosen.jquery.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/script/custom.js"></script>
                <script type="text/javascript" src="<?php echo base_url(); ?>assets/script/function.js"></script>

                <!-- New Notification, see custom.js for reference -->
                <script src="<?= base_url('assets/script/pnotify/dist/pnotify.js') ?>" charset="utf-8"></script>
                <script src="<?= base_url('assets/script/pnotify/dist/pnotify.buttons.js') ?>" charset="utf-8"></script>
                <script src="<?= base_url('assets/script/pnotify/dist/pnotify.nonblock.js') ?>" charset="utf-8"></script>
                <script src="<?= base_url('assets/script/pnotify/dist/pnotify.confirm.js') ?>" charset="utf-8"></script>

                <script type="text/javascript">

            function logout() {
                if (confirm("Apakah anda ingin keluar dari aplikasi?")) {
                    window.location = "<?php echo base_url(); ?>login/signout";
                }
            }
            function changepass() {
                window.location = "<?php echo base_url(); ?>change_password";

            }
            //function profile() {
            //    window.location = "<?php //echo base_url(); ?>//profile";
            //
            //}
                </script>
                <div class="right_col" role="main" style="min-height:800px;">
                    <div id="main" class="row">
