<?php

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function table_paging($like = null, $length = null, $start = 0) {
        $usergroup =$this->session->userdata('siap_nm_group');
        $kd_prov =$this->session->userdata('siap_kd_prov');
        $sql = "select a.id_user,a.kode, a.username,c.nm_pegawai,b.nm_group
                from mst_user a inner JOIN mst_usergroup b on a.id_group = b.id_group 
                                        INNER JOIN mst_pegawai c on a.kode = c.nip
                where a.sts_user = 'Y' ";
        $sql = "select x.*, row_number() over(ORDER BY x.id_user desc) as rownum from($sql) x ";
        if (!empty($like) ) {
            $sql .= "where 1=1";
            $sql .= " and (upper(x.username) || upper(x.nm_pegawai)||  upper(x.nm_group)) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    function count_paging($like, $jml_data) {
        $usergroup =$this->session->userdata('siap_nm_group');
        $kd_prov =$this->session->userdata('siap_kd_prov');

        $sql = "select a.id_user,a.kode, a.username,c.nm_pegawai,b.nm_group
                from mst_user a inner JOIN mst_usergroup b on a.id_group = b.id_group 
                                        INNER JOIN mst_pegawai c on a.kode = c.nip
                where a.sts_user = 'Y' ";
        $sql = "select count(x.*) as total from ($sql) x ";
        if (!empty($like) ) {
            $sql .= " where 1=1";
            $sql .= " and (upper(x.username) ||  upper(x.nm_pegawai) || upper(x.nm_group)) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $result = $this->db->query($sql);
        $result2 = $result->row();
        return $result2->total;
    }

    //===================================================paging modal pegawai===============================
    public function table_paging_data_user($like = null, $length = null, $start = 0 ) {
        $sql = "select a.nip as code, a.nm_pegawai as name
                from mst_pegawai a 
                where  1= 1 and sts_pegawai='Y'  and a.nip not in (select x.kode from mst_user x where x.sts_user ='Y') ";
        $sql = "select x.*, row_number() over(ORDER BY x.name asc) as rownum from($sql) x ";
        if (!empty($like) ) {
            $sql .= "where 1=1";
            $sql .= " and (upper(x.code) || upper(x.name) ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo $sql;die();
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    function count_paging_data_user($like, $jml_data) {
        $sql = "select a.nip as code, a.nm_pegawai as name
                from mst_pegawai a 
                where  1= 1 and sts_pegawai='Y' and a.nip not in (select x.kode from mst_user x where x.sts_user ='Y') ";
        $sql = "select count(x.*) as total from ($sql) x ";
        if (!empty($like) ) {
            $sql .= "where 1=1";
         $sql .= " and (upper(x.code) || upper(x.name) ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $result = $this->db->query($sql);
        $result2 = $result->row();
        return $result2->total;
    }

    //======================================================================================================

    
    public function load_combo_usergroup() {
         //$usergroup =$this->session->userdata('siap_nm_group');
         //echo $usergroup ;die;

        $sql = "select id_group,nm_group from mst_usergroup where sts_group='Y'  order by nm_group asc ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt from mst_user where username='". $arr['username'] ."' and sts_user='Y'";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function insert_data($arr) {
        //$pass = generate_passwd($arr['nip']);
        $id_user = $this->session->userdata('id_user');
        $sql = "insert into mst_user
                 (id_group,
                 kode,
                 username,
                 password,
                 sts_user
                 )
               values
                    ('" . $arr['id_group'] . "',    
                    '" . $arr['kode'] . "',
                    '" . $arr['username'] . "',
                    '" . $arr['password'] . "',    
                    'Y')   
                    ";
        $this->db->query($sql);
    }


    public function update_data($arr) {
        
        $sql = "update mst_user
                 set
                 id_group ='" . $arr['id_group'] . "'
                 WHERE 
                    id_user = '" . $arr['id_user'] . "'
                    ";
        $this->db->query($sql);
    }

    public function delete_data($id_user) {
        $sql = "update mst_user set sts_user = 'N' where id_user=" . $id_user . "";
        $this->db->query($sql);
    }

    public function load_form_update_user($id) {
        $sql = "select a.id_user,a.id_group, a.kode,b.nm_pegawai,a.username,c.nm_group
                from mst_user a
                inner join mst_pegawai b on a.kode = b.nip
                inner join  mst_usergroup c on a.id_group = c.id_group
                where a.id_user=" . $id;
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    public function reset_data($id, $kode) {
        $sql = " update mst_user set password ='$kode' where id_user=" . $id;
        $result = $this->db->query($sql);
    }

    public function getDetilUser($username) {
        $sql = "select * from mst_user where username = '".$username."'";
        return $this->db->query($sql)->result_array();
    }

}
