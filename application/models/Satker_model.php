<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Satker_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $sql = "SELECT a.kdsatker,
           a.nmsatker,
           a.stssatker,
            (select x.nm_kwn from mst_kewenangan x where x.id = a.kewenangan) kwn
            ,a.kewenangan
        FROM mst_satker a 
        where 1 = 1 ";

        $sql = "select x.*, row_number() over(ORDER BY x.kewenangan asc ) as rownum
                from( $sql order by a.kewenangan asc ) x  where 1 = 1 " ;
        if (!empty($like) ) {
            $sql .= " and upper(x.kdsatker || x.nmsatker ||x.kwn ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){


        $sql = "select count(*) cnt
                FROM ( SELECT a.kdsatker,
           a.nmsatker,
            (select x.nm_kwn from mst_kewenangan x where x.id = a.kewenangan) kwn
        FROM mst_satker a 
        where  1 = 1) x where 1 = 1 ";
        if (!empty($like) ) {
            $sql .= " and upper(x.kdsatker || x.nmsatker ||x.kwn ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    
    public function aktiv($arr) {
        $sql = "update mst_satker
                set stssatker = '1'
                 where kdsatker = '$arr'";
        $this->db->query($sql);
    }
    public function inaktiv($arr) {
        $sql = "update mst_satker
                set stssatker = '0'
                 where kdsatker = '$arr'";
        $this->db->query($sql);
    }


    
}
