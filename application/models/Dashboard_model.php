<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }



    public function get_keu_tot() {

        $sql="select a.*,((a.tot_realisasi/a.tot_pagu)*100 )::numeric(5,2) persen_pagu,((a.tot_sisa/a.tot_pagu)*100 )::numeric(5,2)persen_sisa from(select sum(tot_pagu) tot_pagu,sum(tot_realisasi) tot_realisasi,sum(tot_sisa) tot_sisa
from data_demo)a 
";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }
    public function get_keu_jb() {

        $sql="select sum(bp_pagu) bp_pagu,sum(bp_realisasi) bp_realisasi,sum(bp_sisa) bp_sisa,'Pegawai' as jb from data_demo
        union all
        select sum(bb_pagu) bb_pagu,sum(bb_realisasi) bb_realisasi,sum(bb_sisa) bb_sisa, 'Barang' as jb from data_demo
        union all
        select sum(bm_pagu) bm_pagu,sum(bm_realisasi) bm_realisasi,sum(bm_sisa) bm_sisa,'Modal' as jb from data_demo
        union all
       select 0,0,0,'Hibah' as jb 
        
 
";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_keu_kw() {

        $sql="select kewenangan,sum(tot_pagu) tot_pagu,sum(tot_realisasi) tot_realisasi,sum(tot_sisa) tot_sisa
              -- sum(bb_pagu) bb_pagu,sum(bb_realisasi) bb_realisasi,sum(bb_sisa) bb_sisa,
             -- sum(bm_pagu) bm_pagu,sum(bm_realisasi) bm_realisasi,sum(bm_sisa) bm_sisa
              from data_demo
              group by kewenangan 
              order by tot_pagu desc
";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_keu_det_kw($kwgn) {

        $sql="select nm_satker,kewenangan,tot_pagu,tot_realisasi, tot_sisa
              from data_demo
              where kewenangan ='$kwgn'
              order by tot_pagu desc
";
//        echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_satker() {

        $sql="select nm_satker
              from data_demo where kewenangan <>'DEKONSENTRASI'
";
//        echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_satker2() {

        $sql="select nm_satker
              from data_demo
";
//        echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_keu_det_tot($kwngn) {

        $sql="select nm_satker,tot_pagu,tot_realisasi,tot_sisa
              from data_demo
              order by tot_pagu desc
        ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_keu_det_jb($jb) {
        if($jb=='Pegawai'){
            $kolom ="bp_pagu as pagu,bp_realisasi as realisasi,bp_sisa as sisa";
        }else if($jb=='Barang'){
            $kolom ="bb_pagu as pagu,bb_realisasi as realisasi,bb_sisa as sisa";
        }else if($jb=='Modal'){
            $kolom ="bm_pagu as pagu,bm_realisasi as realisasi,bm_sisa as sisa";
        }else{
            return FALSE;
        }

        $sql="select nm_satker, $kolom
               from data_demo
              order by pagu desc
        ";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }






//===================================================================================================================================================================================================






    public function get_dashboard_pusat_det($arr) {


        $sql='SELECT *
FROM   crosstab (
   \'SELECT a.loc_id,a.loc_name,a.target, a.kategori_name, coalesce(a.total,0)total
    FROM (select  
kk.loc_name
,kk.loc_id
,kk.kategori_name
,kk.kategori_id
,COALESCE(round(yy.total),0)total
,COALESCE(yy.target,0)target
from(select a.loc_id,a.loc_name, b.kategori_id,b.kategori_name from mst_location a join mst_kategori b on 1=1) kk
left join (
select a.loc_name,a.loc_id,a.kategori_id,a.kategori_name
,sum(total)total,(select round(x.target/12) from mst_target x where x.loc_id=a.loc_id and x.tahun=\'\''.$arr['tahun'].'\'\')target 
from rekap_day a
where to_char(a.tanggal,\'\'YYYYMM\'\')= \'\''.$arr['tahun'].$arr['bulan'].'\'\'
GROUP BY a.loc_name,a.loc_id,a.kategori_id,a.kategori_name) yy on kk.loc_id =yy.loc_id and kk.kategori_id =yy.kategori_id
ORDER BY kk.loc_id asc
)a  \'
, $$SELECT unnest(\'{F&B,RETAIL,DUTY FREE ,SERVICES,LOUNGE}\'::varchar[])$$
  ) AS ct (
   "loc_id" varchar
	,"loc_name" VARCHAR
,"target" VARCHAR
 ,"fnb"   BIGINT
 ,"retail"   BIGINT
 ,"dutyfree" BIGINT
,"services" BIGINT
,"lounge" BIGINT
);';
        //  echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }


    public function get_dashboard_loc($arr) {

        $sql="select kk.nopd,kk.opd_name,kk.kategori_name,coalesce(yy.target,0) target,kk.loc_name, 
 coalesce(yy.total,0) total
 from mst_opd kk left join (select a.opd_name,a.nopd,a.kategori_name
,round(sum(a.total))total,(select round(x.target/12) from mst_target x where x.loc_id=1 and x.tahun='".$arr['tahun'].$arr['bulan']."')target
from rekap_day  a
where to_char(a.tanggal,'YYYYMM')= '".$arr['tahun'].$arr['bulan']."' and a.loc_id=".$arr['loc_id']."
GROUP BY a.opd_name,a.nopd,a.kategori_name) yy on kk.nopd =yy.nopd where  kk.loc_id=".$arr['loc_id']."
ORDER BY yy.total desc nulls last";
$result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }
    public function get_dashboard_loc_atas($arr) {



        $sql='SELECT *
FROM   crosstab (
   \'SELECT a.loc_id,a.loc_name,a.target,(select count(DISTINCT c.nopd) from rekap_day c where loc_id =a.loc_id)outlet, a.kategori_name, coalesce(a.total,0)total
    FROM (select  
kk.loc_name
,kk.loc_id
,kk.kategori_name
,kk.kategori_id
,COALESCE(round(yy.total),0)total
,COALESCE(yy.target,0)target
from(select a.loc_id,a.loc_name, b.kategori_id,b.kategori_name from mst_location a join mst_kategori b on 1=1 and a.loc_id=\'\''.$arr['loc_id'].'\'\') kk
left join (
select a.loc_name,a.loc_id,a.kategori_id,a.kategori_name
,sum(total)total,(select round(x.target/12) from mst_target x where x.loc_id=a.loc_id and x.tahun=\'\''.$arr['tahun'].'\'\')target 
from rekap_day a
where to_char(a.tanggal,\'\'YYYYMM\'\')= \'\''.$arr['tahun'].$arr['bulan'].'\'\' and a.loc_id=\'\''.$arr['loc_id'].'\'\'
GROUP BY a.loc_name,a.loc_id,a.kategori_id,a.kategori_name) yy on kk.loc_id =yy.loc_id and kk.kategori_id =yy.kategori_id
ORDER BY kk.loc_id asc
)a  \'
, $$SELECT unnest(\'{F&B,RETAIL,DUTY FREE ,SERVICES,LOUNGE}\'::varchar[])$$
  ) AS ct (
   "loc_id" varchar
	,"loc_name" VARCHAR
,"target" VARCHAR
,"outlet" BIGINT
 ,"fnb"   BIGINT
 ,"retail"   BIGINT
 ,"dutyfree" BIGINT
,"services" BIGINT
,"lounge" BIGINT
);';
        //  echo $sql;die();
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }



//---------------------------------------------- DASHBOARD NEW-----------------------------------

    public function get_target_realisasi($tahun) {
                    $sql="select x.target,x.realisasi ,round((x.realisasi/x.target)*100) as persen from (select sum(pro_target_val) target, sum(pro_real_val) realisasi  
from mst_proyek where to_char(pro_start_date,'YYYY') ='$tahun' and pro_status <> 'DELETED')x";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return FALSE;
    }

    public function get_list_pro($tahun) {


        $sql = "select pro_id,pro_name,pro_client,pro_target_val,pro_real_val,pro_status,
pro_target_time || ' hari' as durasi,
coalesce(pro_pic1_name,' ')||','||coalesce(pro_pic2_name,' ')||','||coalesce(pro_pic3_name,' ') as pic,
 ceil((pro_real_val::decimal/pro_target_val)*100) as persen
from mst_proyek where to_char(pro_start_date,'YYYY') ='$tahun' and pro_status <> 'DELETED'
order by pro_id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return 0;
    }

    public function get_list_pro_top($tahun) {


        $sql = "select pro_id,pro_name,pro_client,pro_target_val,pro_real_val,pro_status,
pro_target_time || ' hari' as durasi,
coalesce(pro_pic1_name,' ')||','||coalesce(pro_pic2_name,' ')||','||coalesce(pro_pic3_name,' ') as pic,
 ceil((pro_real_val::decimal/pro_target_val)*100) as persen
from mst_proyek where to_char(pro_start_date,'YYYY') ='$tahun' and pro_status <> 'DELETED'
order by pro_real_val desc limit 5";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }

        return 0;
    }





}

?>
