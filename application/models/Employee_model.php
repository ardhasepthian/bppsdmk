<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Employee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $codeu = $this->session->userdata('efiskus_codeu');
        $wherecon = "";

        $sql = "SELECT a.id_pegawai,
           a.nip,
            a.nm_pegawai,
            a.tlp_pegawai
        FROM mst_pegawai a 
        where a.sts_pegawai ='Y' ".$wherecon;
        if (!empty($like) ) {
            $sql .= " and upper(a.nip || a.nm_pegawai ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.nip desc ) as rownum
                from( $sql order by a.nip desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){
        $wherecon = "";

        $sql = "select count(*) cnt
                FROM ( SELECT a.id_pegawai,
            a.nip,
            a.nm_pegawai,
            a.tlp_pegawai
        FROM mst_pegawai a 
        where a.sts_pegawai ='Y' $wherecon) x where 1 = 1 ";
        if (!empty($like) ) {
            $sql .= " and upper(x.nip || x.nm_pegawai ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function load_combo_org() {
        $codeu = $this->session->userdata('efiskus_codeu');


        $sql = "select * from v_unit_org where upper(kd_unit_org) like upper('$codeu%')  order by kd_unit_org asc";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    public function load_combo_jabatan() {

        $sql = "select * from mst_jabatan where sts_jabatan='Y' order by nm_jabatan asc ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }


    
    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from mst_pegawai 
                where 1 = 1
                and upper(nip) = upper('" . $arr['nip'] . "')
                and sts_pegawai = 'Y'  
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
   

    
    public function insert_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');

        $sql = "insert into mst_pegawai
        (
        nip,
        nm_pegawai,
        tlp_pegawai,
        sts_pegawai,
        email_pegawai,
        create_by,
        create_date,
        update_by,
        update_date)
                values (
                        '". $arr['nip'] . "',
                        '". $arr['nm_pegawai'] . "',
                        '". $arr['tlp_pegawai'] . "',
                        'Y',
                        '". $arr['email_pegawai'] . "',  
                        '$id_user',
                        now(),
                        '$id_user', 
                        now())";
        $this->db->query($sql);
    }
    
    public function cek_before_update($arr) {
        $sql = "select count(*) cnt
                from mst_pegawai 
                where 1 = 1
                and upper(nm_pegawai) = upper('" . $arr['nm_pegawai'] . "')
                and sts_pegawai = 'Y' and nip not in ('".$arr['nip']."')
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
    
    public function update_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_pegawai set
                        nm_pegawai ='". $arr['nm_pegawai'] . "',
                        email_pegawai ='". $arr['email_pegawai'] . "',
                        tlp_pegawai ='". $arr['tlp_pegawai'] . "',
                        update_by ='$id_user',
                        update_date =now()
                where nip ='". $arr['nip'] . "'";
        $this->db->query($sql);
    }
    
    public function delete_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_pegawai
                set sts_pegawai = 'N',
                update_by ='$id_user',
                        update_date =now()
                where nip ='" . $arr."'";
        $this->db->query($sql);
    }

    public function get_detail_employee($emp_code) {
         $sql = "SELECT *
            
        FROM mst_pegawai a 
        where a.sts_pegawai ='Y' and a.nip ='" .$emp_code."'";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }


    public function table_paging_data_user($like = null, $length = null, $start = 0 ,$wcon) {
        $sql = "select a.npwpd, a.wpd_name,a.wpd_address
                from mst_wpd a 
                where  1= 1 and a.npwpd  in (select x.kode from mst_user x where x.sts_user ='Y' and x.id_group = (select y.id_group from mst_usergroup y  where upper(y.nm_group) = upper('WAJIB PAJAK'))) ".$wcon;
        $sql = "select x.*, row_number() over(ORDER BY x.wpd_name asc) as rownum from($sql) x ";
        if (!empty($like) ) {
            $sql .= "where 1=1";
            $sql .= " and (upper(x.npwpd) || upper(x.wpd_name) )) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }

    function count_paging_data_user($like, $jml_data,$wcon) {
        $sql = "select a.npwpd, a.wpd_name,a.wpd_address
                from mst_wpd a 
                where  1= 1 and a.npwpd  in (select x.kode from mst_user x where x.sts_user ='Y' and x.id_group = (select y.id_group from mst_usergroup y  where upper(y.nm_group) = upper('WAJIB PAJAK'))) ".$wcon;
        $sql = "select count(x.*) as total from ($sql) x ";
        if (!empty($like) ) {
            $sql .= "where 1=1";
            $sql .= " and (upper(x.npwpd) || upper(x.wpd_name) ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $result = $this->db->query($sql);
        $result2 = $result->row();
        return $result2->total;
    }

    public function load_combo_loc() {

        $sql = "select * from mst_location order by loc_id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    
}
