<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Util_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_sequence($tablename) {
        $tablename = strtoupper($tablename);
        $sequence = 0;
        $this->db->trans_begin();
        $sql = "select (sequence_number) sequence_number
                from simpati_mst_sequence
                where 1 = 1
                and upper(sequence_table) = '$tablename'
                and sequence_sts = 1";
        $result = $this->db->query($sql)->row();
        $sequence = $result->sequence_number;
        if (!empty($sequence)) {
            $sequence = intval($sequence) + 1;
            $sql = "update simpati_mst_sequence set sequence_number = $sequence
                    where upper(sequence_table) = '$tablename'";
            $this->db->query($sql);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        return $sequence;
    }

    public function cek_modul_authorize($arr) {
        $menu_controller = $arr['menu_controller'];
        $id_group = $arr['id_group'];

        $sql = "select count(*) cnt
                from mst_usergroup_menu a inner join mst_menu b
                                                  on a.menu_id = b.menu_id
                                                  and a.usergroup_menu_sts = '1'
                where 1 = 1
                and a.id_group = $id_group
                and b.menu_controller = '$menu_controller'
                and b.menu_sts = '1' ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function get_list_parent($arr) {
        $id_group = $arr['id_group'];
        $sql = "select b.menu_id, b.menu_parent_id, b.menu_desc, b.menu_controller, b.menu_icon
                from mst_usergroup_menu a inner join mst_menu b
                                                  on a.menu_id = b.menu_id
                                                  and b.menu_sts = '1'
                where 1 = 1
                and a.usergroup_menu_sts = '1'
                and a.id_group = $id_group
                and b.menu_parent_id is null
                order by b.menu_urut asc ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return FALSE;
        }
    }

    public function cek_child($arr) {
        $id_group = $arr['id_group'];
        $menu_parent_id = $arr['menu_parent_id'];
        $sql = "select count(*) cnt
                from mst_usergroup_menu a inner join mst_menu b
                                                  on a.menu_id = b.menu_id
                                                  and b.menu_sts = '1'
                where 1 = 1
                and a.usergroup_menu_sts = '1'
                and a.id_group = $id_group
                and b.menu_parent_id = $menu_parent_id";
                // die($sql);
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function get_list_child($arr) {
        $id_group = $arr['id_group'];
        $menu_parent_id = $arr['menu_parent_id'];
        $sql = "select b.menu_id, b.menu_parent_id, b.menu_desc, b.menu_controller, b.menu_icon
                from mst_usergroup_menu a inner join mst_menu b
                                                  on a.menu_id = b.menu_id
                                                  and b.menu_sts = '1'
                where 1 = 1
                and a.usergroup_menu_sts = '1'
                and a.id_group = $id_group
                and b.menu_parent_id = $menu_parent_id
                order by b.menu_urut asc ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return FALSE;
        }
    }

    private function get_menu_id($controller) {
        $sql = "select a.menu_id
                from mst_menu a
                where 1 = 1
                and a.menu_sts = '1'
                and a.menu_controller = '$controller'";
        $result = $this->db->query($sql)->row();
        return $result->menu_id;
    }

    private function cek_level_menu($menu_id) {
        $sql = "SELECT count(*) cnt
                from mst_menu a
                where 1 = 1
                and a.menu_sts = '1'
                and a.menu_id = $menu_id
                and a.menu_parent_id is NULL";
        $result = $this->db->query($sql)->row();
        $cek1 = $result->cnt;

        $sql = "select count(*) cnt
                from mst_menu a
                where 1 = 1
                and a.menu_sts = '1'
                and a.menu_parent_id in
                (SELECT x.menu_id
                from mst_menu x
                where 1 = 1
                and x.menu_sts = '1'
                and x.menu_parent_id is NULL)
                and a.menu_id = $menu_id";
        $result = $this->db->query($sql)->row();
        $cek2 = $result->cnt;

        $level = 0;
        if (intval($cek1) > 0) {
            $level = 1;
        } else if (intval($cek2) > 0) {
            $level = 2;
        } else {
            $level = 3;
        }
        return $level;
    }

    private function get_menu_level1($menu_id) {
        $sql = "SELECT a.menu_id, a.menu_controller, a.menu_desc
                from mst_menu a
                where 1 = 1
                and a.menu_sts = '1'
                and a.menu_id in (
                        select b.menu_parent_id
                  from mst_menu b
                  where 1 = 1
                  and b.menu_sts = '1'
                  and b.menu_id in
                                (SELECT x.menu_parent_id
                                from mst_menu x
                                where 1 = 1
                                and x.menu_sts = '1'
                                and x.menu_id = $menu_id))";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return FALSE;
        }
    }

    private function get_menu_level2($menu_id) {
        $sql = "select a.menu_id, a.menu_controller, a.menu_desc
                from mst_menu a
                where 1 = 1
                and a.menu_sts = '1'
                and a.menu_id in
                (SELECT x.menu_parent_id
                from mst_menu x
                where 1 = 1
                and x.menu_sts = '1'
                and x.menu_id = $menu_id) ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return FALSE;
        }
    }

    private function get_menu_level3($menu_id) {
        $sql = "select menu_desc, menu_controller, menu_desc from mst_menu where menu_id = $menu_id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        } else {
            return FALSE;
        }
    }

    public function getmenugps($controller) {
        $menu_id = $this->get_menu_id($controller);
        $menu_level = $this->cek_level_menu($menu_id);
        $result = "";
        if ($menu_level == 1) {
            $resultmenu = $this->get_menu_level3($menu_id);
            if ($resultmenu != FALSE) {
                $result = ucwords(trim($resultmenu['menu_desc'])) . "&nbsp;";
            }
        } else if ($menu_level == 2) {
            $resultmenu = $this->get_menu_level2($menu_id);
            if ($resultmenu != FALSE) {
                $result = ucwords(trim($resultmenu['menu_desc'])) . "&nbsp;";
            }

            $resultmenu = $this->get_menu_level3($menu_id);
            if ($resultmenu != FALSE) {
                $result .= ":::&nbsp;<b><a href='" . base_url() . $resultmenu['menu_controller'] . "'>" . ucwords(trim($resultmenu['menu_desc'])) . "</a></b>" . "&nbsp;";
            }
        } else {
            $resultmenu = $this->get_menu_level1($menu_id);
            if ($resultmenu != FALSE) {
                $result = ucwords(trim($resultmenu['menu_desc'])) . "&nbsp;";
            }

            $resultmenu = $this->get_menu_level2($menu_id);
            if ($resultmenu != FALSE) {
                $result .= ":::&nbsp;<b><a href='" . base_url() . $resultmenu['menu_controller'] . "'>" . ucwords(trim($resultmenu['menu_desc'])) . "</a></b>" . "&nbsp;";
            }

            $resultmenu = $this->get_menu_level3($menu_id);
            if ($resultmenu != FALSE) {
                $result .= ":::&nbsp;<b><a href='" . base_url() . $resultmenu['menu_controller'] . "'>" . ucwords(trim($resultmenu['menu_desc'])) . "</a></b>" . "&nbsp;";
                //$result .= ":::&nbsp;".ucwords(trim($resultmenu['menu_desc']))."&nbsp;";
            }
        }
        //echo "$result<br>";
        return $result;
    }

    public function insert_audittrail($arr) {
        $sql = "insert into simpati_mst_audittrail (audittrail_date, nip, controller, function, audittrail_sts, ip, browser) values
                (now(), '" . $arr['nip'] . "', '" . $arr['controller'] . "', '" . $arr['function'] . "', " . $arr['audittrail_sts'] . ", '" . $arr['ip'] . "', '" . $arr['browser'] . "')";
        $this->db->query($sql);
    }

    private function cek_primarykey_audittrail_id($audittrail_id) {
        $sql0 = "select count(*)cnt from simpati_mst_audittrail where audittrail_id = $audittrail_id";
        $result = $this->db->query($sql0)->row();
        return $result->cnt;
    }

    public function generate_code($column) {
        $code = "";
        $column = strtolower(trim($column));
        $this->db->trans_begin();
        $sql    = "select kd_generate,table_name, prefix, curseq, length from ref_generate_kode where table_name = '$column' and sts_generate = 'Y' ";
        $result = $this->db->query($sql)->row_array();

        if(!empty($result)) {
            $sequence   = intval($result['curseq'])+1 ;

            $sql = "update ref_generate_kode set curseq = $sequence
                    where table_name = '$column'";
            $this->db->query($sql);

            $prefix     = $result['prefix'] ;
            $length     = intval($result['length']) ;

            $sufix      = "";

            if(strlen($sequence) < $length) {
                $sufix = str_pad($sequence, $length, '0', 0) ;
            }else {
                $sufix = $sequence ;
            }
            $code = $prefix.$sufix ;
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        return $code ;
    }

    function cek_bypass_controller($controller) {
        $sql = "select count(*) cnt from efiskus_setting_bypass_controller where bypass_controller_desc = '$controller' and bypass_controller_sts = 1";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function get_spt($spt_code, $nip = NULL) {
        $nip = ($nip) ? $nip : $this->session->userdata('simpati_nip');
        $this->db->select("spt_id, spt_code, spt_desc, spt_notes, spt_date, spt_by, spt_sts, satker, spt_start_date, spt_end_date,spt_inspektur,
            spt_durasi, spt_periode, lokasi, categ_modul_id, categ_pengawasan_id, subcateg_pengawasan_id, kd_unit_instansi,
            jns_kegiatan_code, lks_kegiatan_code, lokasi,
            (SELECT nama from v_pegawai where spt_head.spt_inspektur = nip )spt_inspektur_name,
            (SELECT jns_kegiatan_desc FROM simpati_mst_jenis_kegiatan
              WHERE jns_kegiatan_code = spt_head.jns_kegiatan_code AND jns_kegiatan_sts = 1 ) as jns_kegiatan_code_desc,
            (SELECT lks_kegiatan_desc FROM simpati_mst_lokasi_kegiatan
              WHERE lks_kegiatan_code = spt_head.lks_kegiatan_code AND lks_kegiatan_sts = 1) as lks_kegiatan_code_desc,
            (SELECT categ_modul_desc FROM simpati_mst_categ_modul
              WHERE categ_modul_id = spt_head.categ_modul_id AND categ_modul_sts = 1 ) as categ_modul_desc,
            (SELECT categ_pengawasan_desc FROM simpati_mst_categ_pengawasan
              WHERE categ_pengawasan_id = spt_head.categ_pengawasan_id AND categ_pengawasan_sts = 1 ) as categ_pengawasan_desc,
            (SELECT subcateg_pengawasan_desc FROM simpati_mst_subcateg_pengawasan
              WHERE subcateg_pengawasan_id = spt_head.subcateg_pengawasan_id AND subcateg_pengawasan_sts = 1 ) as subcateg_pengawasan_desc,
            (SELECT spt_pic_name FROM spt_pic
              WHERE spt_code = spt_head.spt_code AND spt_pic_sts = 1 AND jabatan_spt_id = 1 ) as katim,
            (SELECT spt_pic_name FROM spt_pic
              WHERE spt_code = spt_head.spt_code AND spt_pic_sts = 1 AND jabatan_spt_id = 2 ) as dalnis,
            (SELECT spt_pic_nip FROM spt_pic
              WHERE spt_code = spt_head.spt_code AND spt_pic_sts = 1 AND jabatan_spt_id = 1 ) as nip_katim,
            (SELECT spt_pic_nip FROM spt_pic
              WHERE spt_code = spt_head.spt_code AND spt_pic_sts = 1 AND jabatan_spt_id = 2 ) as nip_dalnis,
            (SELECT
                (   SELECT jabatan_spt_desc FROM simpati_mst_jabatan_spt WHERE jabatan_spt_id = spt_pic.jabatan_spt_id  )
                FROM spt_pic
                WHERE spt_pic_sts = 1 AND spt_code = spt_head.spt_code AND spt_pic_nip = '" . $nip . "'
            ) as jabatan,
            (SELECT nama_satker FROM v_instansi
              WHERE kd_unit_instansi = spt_head.kd_unit_instansi ) as eselon_1,
            (SELECT spt_pic_id from spt_pic WHERE spt_pic_sts = 1 AND spt_code = spt_head.spt_code AND spt_pic_nip = '" . $nip . "' ) as spt_pic_id");

        $this->db->where(array('spt_code' => $spt_code));

        return $this->db->get('spt_head')->row_array();
    }


    public function get_secodary_menus($id_group){
        $this->db->select('b.*')
        ->from('mst_usergroup_menu a')
        ->join('mst_menu b', 'b.menu_id = a.menu_id')
        ->where('a.id_group', $id_group)
        ->where('b.menu_parent_id is NOT NULL', NULL, FALSE)
        ->order_by('b.menu_urut', 'ASC');

        return $this->db->get()->result();
    }

}

?>
