<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Visimisi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    

    public function update_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_visimisi set
                        visimisi ='". $arr['visimisi'] . "',
                        renstra ='". $arr['renstra'] . "'";

        $this->db->query($sql);
    }
    
    public function loadvisimisi() {
         $sql = "SELECT *
        FROM mst_visimisi a 
       ";

        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }


    

    
}
