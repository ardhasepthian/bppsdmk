<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Belanja_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $sql = "SELECT a.kdbelanja,
           a.nmbelanja,
           a.stsbelanja
--            ,(select x.nm_kwn from mst_kewenangan x where x.id = a.kewenangan) kwn
--            ,a.kewenangan
        FROM mst_belanja a 
        where 1 = 1 ";

        $sql = "select x.*, row_number() over(ORDER BY x.kdbelanja asc ) as rownum
                from( $sql order by a.kdbelanja asc ) x  where 1 = 1 " ;
        if (!empty($like) ) {
            $sql .= " and upper(x.kdbelanja || x.nmbelanja  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){


        $sql = "select count(*) cnt
                FROM ( SELECT a.kdbelanja,
           a.nmbelanja
        FROM mst_belanja a 
        where  1 = 1) x where 1 = 1 ";
        if (!empty($like) ) {
            $sql .= " and upper(x.kdbelanja || x.nmbelanja  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    
    public function aktiv($arr) {
        $sql = "update mst_belanja
                set stsbelanja = '1'
                 where kdbelanja = '$arr'";
        $this->db->query($sql);
    }
    public function inaktiv($arr) {
        $sql = "update mst_belanja
                set stsbelanja = '0'
                 where kdbelanja = '$arr'";
        $this->db->query($sql);
    }


    
}
