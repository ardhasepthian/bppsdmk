<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Kategori_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $codeu = $this->session->userdata('efiskus_codeu');
        $wherecon = "";

        $sql = "SELECT a.kategori_id,
            a.kategori_name
            
        FROM mst_kategori a 
        where a.kategori_sts ='Y' ".$wherecon;
        if (!empty($like) ) {
            $sql .= " and upper(a.kategori_id || a.kategori_name) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.kategori_id desc ) as rownum
                from( $sql order by a.kategori_id desc ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){
        $wherecon = "";

        $sql = "select count(*) cnt
                FROM ( SELECT 
            a.kategori_id,
            a.kategori_name
            
        FROM mst_kategori a 
        where a.kategori_sts ='Y' $wherecon) x where 1 = 1 ";
        if (!empty($like) ) {
            $sql .= " and upper(x.kategori_id  || x.kategori_name) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function load_combo_loc() {

        $sql = "select * from mst_location order by loc_id";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }


    public function detail_kategori($kategori_id) {

        $sql = "select * from mst_kategori where kategori_id='$kategori_id' ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    
    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from mst_kategori 
                where 1 = 1
                and upper(kategori_name) = upper('" . $arr['kategori_name'] . "')
                and kategori_sts = 'Y'  
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }
   

    
    public function insert_data($arr) {


        $sql = "insert into mst_kategori
        (
        kategori_name
        ,kategori_sts)
                values (
                        '". $arr['kategori_name'] . "',
                        'Y')";
        $this->db->query($sql);
    }
    
    public function cek_before_update($arr) {
        $sql = "select count(*) cnt
                from mst_kategori 
                where 1 = 1
                and upper(kategori_name) = upper('" . $arr['kategori_name'] . "')
                and kategori_sts = 'Y' and kategori_id not in ('".$arr['kategori_id']."')
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    
    public function update_data($arr) {
        $id_user = $this->session->userdata('efiskus_code');
        $sql = "update mst_kategori set
                        kategori_name = '".$arr['kategori_name']."'
                where kategori_id ='". $arr['kategori_id'] . "'";
        $this->db->query($sql);
    }
    
    public function delete_data($arr) {
        $sql = "update mst_kategori
                set kategori_sts = 'N'
                where kategori_id ='" . $arr."'";
        $this->db->query($sql);
    }


    


    
}
