<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Sd_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    public function get_list_data($like = null, $length = null, $start) {
        $sql = "SELECT a.kdsd,
           a.nmsd,
           a.stssd
--            ,(select x.nm_kwn from mst_kewenangan x where x.id = a.kewenangan) kwn
--            ,a.kewenangan
        FROM mst_sd a 
        where 1 = 1 ";

        $sql = "select x.*, row_number() over(ORDER BY x.kdsd asc ) as rownum
                from( $sql order by a.kdsd asc ) x  where 1 = 1 " ;
        if (!empty($like) ) {
            $sql .= " and upper(x.kdsd || x.nmsd  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
//        echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : FALSE;
    }
    
    public function get_count_data($like, $jml_data){


        $sql = "select count(*) cnt
                FROM ( SELECT a.kdsd,
           a.nmsd
        FROM mst_sd a 
        where  1 = 1) x where 1 = 1 ";
        if (!empty($like) ) {
            $sql .= " and upper(x.kdsd || x.nmsd  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    
    public function aktiv($arr) {
        $sql = "update mst_sd
                set stssd = '1'
                 where kdsd = '$arr'";
        $this->db->query($sql);
    }
    public function inaktiv($arr) {
        $sql = "update mst_sd
                set stssd = '0'
                 where kdsd = '$arr'";
        $this->db->query($sql);
    }


    
}
