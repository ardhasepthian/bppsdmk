<?php

/* =======================================================
  | Author     : Syafira Puji Virginia
  | Email      :
  | Class name : Satuan
  | Date       : 02-05-2017
  ========================================================= */

class Stko_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_list_data($like = null, $length = null, $start) {


        $sql = "SELECT *,coalesce((select yy.tittle from  struktur_org yy where yy.id = a.pid),'--')pid_name from struktur_org a where a.status <> 'N' ";
        if (!empty($like) ) {
            $sql .= " and upper(a.jabatan || a.name  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        $sql = "select x.*, row_number() over(ORDER BY x.pid asc  NULLS FIRST ) as rownum
                from( $sql order by a.pid asc  NULLS FIRST ) x";
        if (!empty($length)) {
            $sql .= " limit $length offset $start";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql);
        return ($result->num_rows() > 0) ? $result->result_array() : false;
    }
    
    public function get_count_data($like, $jml_data){

        $sql = "SELECT count(*)cnt  from struktur_org a where a.status <> 'N'  ";
        if (!empty($like) ) {
            $sql .= " and upper(x.nopd || x.opd_name  ) like upper('%".$this->db->escape_like_str($like)."%')";
        }
        //echo "$sql<br>";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

    public function get_pid() {
        $sql = "select tittle,id from struktur_org where status <> 'N' order by pid asc NULLS FIRST";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }
    public function get_pid_edit($id) {
        $sql = "select tittle,id from struktur_org where status <> 'N' and id <> $id order by pid asc NULLS FIRST";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }
        return FALSE;
    }

    public function cek_before_insert($arr) {
        $sql = "select count(*) cnt
                from struktur_org
                where 1 = 1
                and upper(tittle) = upper('" . $arr['tittle'] . "')
                and status <> 'N'
                ";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }

   

    
    public function insert_data($arr) {

        $sql = "insert into struktur_org
                        (
                    tittle,
                    name,
                    pid,
                    email,
                    status)
                values (
                        '". $arr['tittle'] . "',
                        '". $arr['name'] . "',
                        '". $arr['pid'] . "',
                        '". $arr['email'] . "',
                        'Y'
                        )";
        
        $this->db->query($sql);
    }
    
    public function cek_before_update($arr) {
        $sql = "select count(*) cnt
                from struktur_org
                where 1 = 1
                and upper(tittle) = upper('" . $arr['tittle'] . "')
                and status <> 'N'
                and id not in('".$arr['id']."')";
        $result = $this->db->query($sql)->row();
        return $result->cnt;
    }


    
    public function update_data($arr) {
        $sql = "update struktur_org set
                    tittle = '". $arr['tittle'] . "',
                    name ='". $arr['name'] . "',
                        pid ='". $arr['pid'] . "',
                        email ='". $arr['email'] . "'
                        
                where id ='". $arr['id'] . "'";
                        
        $this->db->query($sql);
    }
    
    public function delete_data($id) {
        $sql = "update struktur_org
                set status = 'N',
                where id ='". $id . "'";
        $this->db->query($sql);
    }

    public function get_detail_stko($id) {
         $sql = "SELECT a.*  
FROM struktur_org a  
where a.status <> 'N'
        and id ='$id'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    public function get_detail_pos($pos_code) {
        $sql = "SELECT a.*,c.opd_name  
FROM mst_pos a 
INNER JOIN struktur_org c on a.nopd = c.nopd 
where a.pos_status ='Y'
                and pos_code ='" .$pos_code."'";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return $result->row_array();
        }
        return FALSE;
    }

    
}
