<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : 
  | Email      :
  | Class name : 
  | Date       : 02-05-2017
  ========================================================= */

class Visimisi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Visimisi_model');
    }

    public function index()
    {
        $data['tittle'] = 'Edit Visi Misi dan Renstra';
        $data['form'] = $this->Visimisi_model->loadvisimisi();
        $this->load->view('visimisi/visimisi_edit_view', $data);
    }

    public function edit_data()
    {
        $nip = $this->input->post('nip');
        


        $this->load->view('visimisi/visimisi_edit_view', $data);
    }

    public function update_data()
    {
        $arr['visimisi'] = ($this->input->post('visimisi'));
        $arr['renstra'] = ($this->input->post('renstra'));
        if (!empty($arr['visimisi'])) {

                $this->Visimisi_model->update_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil diubah');
        } else {
            $this->session->set_flashdata('error', 'Form kosong');
        }
    }

    


}
