<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : Arif Fajar Budiman
  | Email      :
  | Class name : usergroup
  | Date       : 04-04-2017
  ========================================================= */

class Usergroup extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('usergroup_model');
    }

    public function get_data_paging() {
        $draw   = $this->input->get('draw');
        $length = $this->input->get('length');
        $start  = $this->input->get('start');
        $cond   = $this->input->get('search')["value"];

        $data['v_det']  = $this->usergroup_model->get_list_data($cond, $length, $start);
        $totalData      = $this->usergroup_model->get_count_data($cond, true);
        $v_content['v_data'] = [];
        $i = 1;
        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $key => $value) {
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['nm_group'],
                    trim(
                    '<div class="btn-group col-md-12"><button type="button" class="btn btn-sm btn-block btn-default dropdown-toggle" data-toggle="dropdown">'. $this->config->item('text_btn_action') .' <span class="caret"></span></button>
                      <ul class="dropdown-menu col-md-12" role="menu">
                          <a href="#" onclick="edit_usergroup(' . $value['id_group'] . ')" class="btn btn-xs btn-warning btn-block">'. $this->config->item('text_btn_edit') .'</a>
                          <a href="#" onclick="delete_usergroup(' . $value['id_group'] . ', \''.$value['nm_group'].'\')" class="btn btn-block btn-xs btn-danger">'. $this->config->item('text_btn_delete') .'</a>
                          <a href="#" onclick="javascript:pengaturan_menu(' . $value['id_group'] . ');" class="btn btn-xs btn-sm btn-primary btn-block">Pengaturan Menu</a>
                      </ul></div>'
                    )
                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );

        return $this->response($output);
    }

    public function index() {
        $data['tittle'] = 'Daftar User Group';
        $this->load->view('settings/usergroup/usergroup_list_view', $data);
    }

    public function add_data() {
        $this->load->view('settings/usergroup/usergroup_add_view');
    }

    public function edit_data() {
        $id_group = $this->input->post('id_group');
        $data['form'] = $this->usergroup_model->get_detail_usergroup($id_group);
        $this->load->view('settings/usergroup/usergroup_edit_view', $data);
    }

    public function update_data() {
        $param_audit['controller']  = $this->controller;
        $param_audit['function']    = $this->function;

        $arr['id_group'] = $this->input->post('id_group');
        $arr['nm_group'] = strtoupper(trim($this->input->post('nm_group')));

        if (!empty($arr['nm_group'])) {
            $cek_usergroup = $this->usergroup_model->cek_before_update($arr);
            if (intval($cek_usergroup) > 0) {
                $error ='Username sudah terdaftar!';
                echo $error;
            } else {
                $this->usergroup_model->update_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil rubah');
            }
        } else {
            $this->session->set_flashdata('error', 'Data gagal dirubah');
        }
    }

    public function delete_data() {
        $param_audit['controller']  = $this->controller;
        $param_audit['function']    = $this->function;

        $arr['id_group'] = $this->input->post('id_group');
        if (!empty($arr['id_group'])) {

            $this->usergroup_model->delete_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('error', 'Data Gagal dihapus');
        }
    }

    public function insert_data() {
        $param_audit['controller']  = $this->controller;
        $param_audit['function']    = $this->function;

        $arr['nm_group'] = strtoupper(trim($this->input->post('nm_group')));
        if (!empty($arr['nm_group'])) {
            $cek_usergroup = $this->usergroup_model->cek_before_insert($arr);
            if (intval($cek_usergroup) > 0) {
                $error ='Usergroup  sudah terdaftar!';
                echo $error;
               // $this->session->set_flashdata("error", "User group dengan nama <strong>{$arr['nm_group']}</strong>, sudah ada silahkan coba dengan nama lain");

            } else {

                $this->usergroup_model->insert_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil ditambah');

            }
        } else {
            $this->session->set_flashdata('error', 'Gagal menambah user');
        }
    }

    public function edit_usergroup_menu() {
        $id_group   = $this->input->post('id_group');
        $data['nm_group']     = $this->usergroup_model->get_detail_usergroup($id_group);
        $data['list_combo_menu']    = $this->usergroup_model->load_combo_menu();
        $data['list_combo_menu_selected']   = $this->usergroup_model->load_combo_menu_selected($id_group);
        $this->load->view('settings/usergroup/usergroup_menu_view', $data);
    }
    public function detail_data() {
        $id_group   = $this->input->post('id_group');
        $data['usergroup_detail']           = $this->usergroup_model->get_detail_usergroup($id_group);
        $data['list_menu']            = $this->usergroup_model->load_list_menu($id_group);
        echo json_encode($data);

    }


    public function update_usergroup_menu() {
        $param_audit['controller']  = $this->controller;
        $param_audit['function']    = $this->function;

        $arr['arr_menu']       = $this->input->post('arr_menu');
        $arr['id_group']       = $this->input->post('id_group');
        $arr_menu              = $this->input->post('id_group');

        if (!empty($arr['arr_menu']) && $arr['arr_menu'] != NULL && count($arr['arr_menu']) > 0) {
            $arr['arr_menu_data'] = null;
            if ($arr['arr_menu'] != NULL) {
                for ($x = 0; $x < count($arr['arr_menu']); $x++) {
                    $arr['arr_menu_data'][$x] = $arr['arr_menu'][$x];
                }
            }

            $this->usergroup_model->update_pengaturan_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil disimpan');
        } else {
            $this->session->set_flashdata('error', 'Data gagal disimpan');
        }
    }

}
