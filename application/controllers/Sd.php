<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : 
  | Email      :
  | Class name : 
  | Date       : 02-05-2017
  ========================================================= */

class Sd extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Sd_model');
    }

    public function get_data_paging()
    {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];

        $data['v_det'] = $this->Sd_model->get_list_data($cond, $length, $start);
        $totalData = $this->Sd_model->get_count_data($cond, true);
        $v_content['v_data'] = [];

        $i = 1;
        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $value) {
//                $btn ='<a href="#"   onclick="aktiv(\'' . $value['kdsd'] . '\')" class="btn btn-xs btn-success  tooltips">Aktiv</a>';
//                if($value['stssd'] == 1){
//                    $btn ='<a href="#"   onclick="inaktiv(\'' . $value['kdsd'] . '\')" class="btn btn-xs btn-danger  tooltips">Inaktiv</a>';
//                }
                
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['kdsd'],
                    $value['nmsd'],
//                    $btn

                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );
        //output to json format
        echo json_encode($output);
    }

    public function index()
    {
        $data['tittle'] = 'Daftar Sd';
        // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        $this->load->view('sd/sd_list_view', $data);
    }



    public function aktiv()
    {
        $arr = $this->input->post('kdsd');
        if (!empty($arr)) {
            $this->Sd_model->aktiv($arr);
            $this->session->set_flashdata('info', 'Data berhasil di aktiv');
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }
    public function inaktiv()
    {
        $arr = $this->input->post('kdsd');
        if (!empty($arr)) {
            $this->Sd_model->inaktiv($arr);
            $this->session->set_flashdata('info', 'Data berhasil di nonaktiv');
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }




}
