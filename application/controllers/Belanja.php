<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : 
  | Email      :
  | Class name : 
  | Date       : 02-05-2017
  ========================================================= */

class Belanja extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Belanja_model');
    }

    public function get_data_paging()
    {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];

        $data['v_det'] = $this->Belanja_model->get_list_data($cond, $length, $start);
        $totalData = $this->Belanja_model->get_count_data($cond, true);
        $v_content['v_data'] = [];

        $i = 1;
        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $value) {
//                $btn ='<a href="#"   onclick="aktiv(\'' . $value['kdbelanja'] . '\')" class="btn btn-xs btn-success  tooltips">Aktiv</a>';
//                if($value['stsbelanja'] == 1){
//                    $btn ='<a href="#"   onclick="inaktiv(\'' . $value['kdbelanja'] . '\')" class="btn btn-xs btn-danger  tooltips">Inaktiv</a>';
//                }
                
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['kdbelanja'],
                    $value['nmbelanja'],
//                    $value['kwn'],
//                    $btn

                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );
        //output to json format
        echo json_encode($output);
    }

    public function index()
    {
        $data['tittle'] = 'Daftar Belanja';
        // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        $this->load->view('belanja/belanja_list_view', $data);
    }



    public function aktiv()
    {
        $arr = $this->input->post('kdbelanja');
        if (!empty($arr)) {
            $this->Belanja_model->aktiv($arr);
            $this->session->set_flashdata('info', 'Data berhasil di aktiv');
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }
    public function inaktiv()
    {
        $arr = $this->input->post('kdbelanja');
        if (!empty($arr)) {
            $this->Belanja_model->inaktiv($arr);
            $this->session->set_flashdata('info', 'Data berhasil di nonaktiv');
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }




}
