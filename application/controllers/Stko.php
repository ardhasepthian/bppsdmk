<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : 
  | Email      :
  | Class name : 
  | Date       : 02-05-2017
  ========================================================= */

class Stko extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('stko_model');
    }

    public function get_data_paging()
    {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];

        $data['v_det'] = $this->stko_model->get_list_data($cond, $length, $start);
        $totalData = $this->stko_model->get_count_data($cond, true);
        $v_content['v_data'] = [];
        $i = 1;
        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $key => $value) {
                if ($value['status'] =='kbd'){
                    $btn ='<a href="#" onclick="edit(\'' . $value['id'] . '\')" class="btn btn-xs btn-warning "> Ubah</a>';
                }else{
                    $btn ='<a href="#" onclick="hapus(\'' . $value['id'] . '\')" class="btn btn-xs btn-danger"> hapus</a> 
                    <a href="#" onclick="edit(\'' . $value['id'] . '\')" class="btn btn-xs btn-warning "> Ubah</a>';
                }

                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['tittle'],
                    $value['pid_name'],
                    $value['name'],
                    $value['email'],
                    $value['status'],
                    $btn
                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );
        //output to json format
        echo json_encode($output);
    }

    public function index()
    {
        $data['tittle'] = 'Struktur Organisasi';
        // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        $this->load->view('stko/stko_list_view', $data);
    }



    public function add_data()
    {
        $data['pid'] =$this->stko_model->get_pid();
        $this->load->view('stko/stko_add_view', $data);
    }

    public function edit_data()
    {
        $data['id'] = trim($this->input->post('id'));
        $data['pid'] =$this->stko_model->get_pid_edit($data['id']);
        $data['form'] =$this->stko_model->get_detail_stko($data['id']);
        $this->load->view('stko/stko_edit_view', $data);
    }

    public function update_data()
    {

        $arr['tittle'] = ucfirst(trim($this->input->post('tittle')));
        $arr['name'] = ucfirst(trim($this->input->post('name')));
        $arr['email'] = trim($this->input->post('email'));
        $arr['pid'] = trim($this->input->post('pid'));
        $arr['id'] = trim($this->input->post('id'));

        if (!empty($arr['tittle'])) {
            $cektit = $this->stko_model->cek_before_update($arr);
            if (intval($cektit) > 0) {
            echo 'POS sudah terdaftar';
            } else {
                $this->stko_model->update_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil diubah');
            }
        } else {
            $this->session->set_flashdata('error', 'Form kosong');
        }
    }

    public function delete_data()
    {

        $arr['id'] = $this->input->post('id');
        if (!empty($arr['id'])) {
            $this->stko_model->delete_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil dihapus');
        }
    }

    public function insert_data()
    {
        $arr['tittle'] = ucfirst(trim($this->input->post('tittle')));
        $arr['name'] = ucfirst(trim($this->input->post('name')));
        $arr['email'] = trim($this->input->post('email'));
        $arr['pid'] = trim($this->input->post('pid'));


        if (!empty($arr['tittle'])) {

            $cektit = $this->stko_model->cek_before_insert($arr);
            if($cektit ==1){
                $this->session->set_flashdata('error', 'Jabatan sudah terdaftar');
            }else{
                $this->stko_model->insert_data($arr);
            }


        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }

    public function detail_data()
    {
        $id = $this->input->get('id');
        $data = $this->stko_model->get_detail_outlet($id);
        echo json_encode($data);
    }


}
