<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : 
  | Email      :
  | Class name : 
  | Date       : 02-05-2017
  ========================================================= */

class Employee extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Employee_model');
    }

    public function get_data_paging()
    {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];

        $data['v_det'] = $this->Employee_model->get_list_data($cond, $length, $start);
        $totalData = $this->Employee_model->get_count_data($cond, true);
        $v_content['v_data'] = [];
        // var_dump($data['v_det']);
        $i = 1;
        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $value) {
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['nip'],
                    $value['nm_pegawai'],
                    $value['tlp_pegawai'],
                    trim('<a href="#"   onclick="edit_employee(\'' . $value['nip'] . '\')" class="btn btn-xs btn-warning  tooltips"><i class="icon_pencil-edit"></i><span class="tooltiptexts">Edit</span></a>
                    <a href="#"   onclick="delete_employee(\'' . $value['nip'] . '\')" class="btn btn-xs btn-danger  tooltips"><i class="icon_trash"></i><span class="tooltiptexts">Delete</span></a>')

//                    trim(
//                         '<div class="btn-group col-md-12">
//      <button type="button" class="btn btn-sm btn-block btn-default dropdown-toggle" data-toggle="dropdown">
//      '. $this->config->item('text_btn_action') .' <span class="caret"></span>
//      </button>
//      <ul class="dropdown-menu col-md-12" role="menu">
//      <a href="#" onclick="edit_employee(\'' . $value['nip'] . '\')" class="btn btn-xs btn-warning btn-block">'. $this->config->item('text_btn_edit') .'</a>
//                    <a href="#" onclick="delete_employee(\'' . $value['nip'] . '\',\''. $value['nm_pegawai'].'\')" class="btn btn-xs btn-danger btn-block">'. $this->config->item('text_btn_delete') .'</a>
//      </div>'
//                    )
                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );
        //output to json format
        echo json_encode($output);
    }

    public function index()
    {
        $data['tittle'] = 'Daftar Pegawai';
        // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        $this->load->view('employee/employee_list_view', $data);
    }

    public function add_data()
    {
        $data['tittle'] = 'Tambah Pegawai';

        $this->load->view('employee/employee_add_view', $data);
    }


    public function edit_data()
    {
        $nip = $this->input->post('nip');
        $data['form'] = $this->Employee_model->get_detail_employee($nip);


        $this->load->view('employee/employee_edit_view', $data);
    }

    public function update_data()
    {
        $arr['nm_pegawai'] = strtoupper(trim($this->input->post('nm_pegawai')));
        $arr['nip'] = trim($this->input->post('nip'));
//        $arr['loc_id'] = trim($this->input->post('loc_id'));
//        $arr['loc_name'] = trim($this->input->post('loc_name'));
        $arr['tlp_pegawai'] = trim($this->input->post('tlp_pegawai'));
        $arr['email_pegawai'] = trim($this->input->post('email_pegawai'));
        if (!empty($arr['nip'])) {
            $cek_employee = $this->Employee_model->cek_before_update($arr);
            if (intval($cek_employee) > 0) {
                echo 'Pegawai sudah terdaftar';
            } else {
                $this->Employee_model->update_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil diubah');
            }
        } else {
            $this->session->set_flashdata('error', 'Form kosong');
        }
    }

    public function delete_data()
    {
        $arr = $this->input->post('nip');
        if (!empty($arr)) {
            $this->Employee_model->delete_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }

    public function insert_data()
    {
        $arr['nm_pegawai'] = strtoupper(trim($this->input->post('nm_pegawai')));
        $arr['nip'] = trim($this->input->post('nip'));
//        $arr['loc_id'] = trim($this->input->post('loc_id'));
//        $arr['loc_name'] = trim($this->input->post('loc_name'));
        $arr['tlp_pegawai'] = trim($this->input->post('tlp_pegawai'));
        $arr['email_pegawai'] = trim($this->input->post('email_pegawai'));
        if (!empty($arr['nip'])) {
            $cek_employee = $this->Employee_model->cek_before_insert($arr);
            if (intval($cek_employee) > 0) {
                $this->session->set_flashdata('error', 'Pegawai sudah terdaftar.');
            } else {
                $this->Employee_model->insert_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil disimpan!!');
            }
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }
    public function load_combo_loc()
    {
        $data=$this->wpd_model->load_combo_loc();
        echo json_encode($data);
    }


}
