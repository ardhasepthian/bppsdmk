<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard_model');
//        $this->load->model('wbs_model');
    }

    public function index() {
            $this->load->view('dashboard/dashboard_view');
    }

    public function keuangan() {
        $this->load->view('dashboard/dashboard_view_keuangan');
    }
    public function profil() {
        $this->load->view('dashboard/dashboard_view_profil');
    }
    public function program() {
        $this->load->view('dashboard/dashboard_view_program');
    }
    public function kepegawaian() {
        $this->load->view('dashboard/dashboard_view_kepegawaian');
    }
    public function barjas() {
        $this->load->view('dashboard/dashboard_view_barjas');
    }

    public function get_dashboard_keuangan() {

        $output['total'] = $this->dashboard_model->get_keu_tot();
        $output['jenis_belanja'] = $this->dashboard_model->get_keu_jb();
        $output['kewenangan'] = $this->dashboard_model->get_keu_kw();

        echo json_encode($output);
    }

    public function get_data_satker() {

        $output= $this->dashboard_model->get_satker();


        echo json_encode($output);
    }
    public function get_dashboard_keu_det_pagu() {
       $arr= $this->input->post('id');
        $output = $this->dashboard_model->get_keu_det_tot($arr);

        echo json_encode($output);
    }

    public function get_dashboard_keu_det_jb() {
        $arr= $this->input->post('id');
        $output = $this->dashboard_model->get_keu_det_jb($arr);

        echo json_encode($output);
    }
    public function get_dashboard_keu_det_kw() {
        $arr= $this->input->post('id');
        $output = $this->dashboard_model->get_keu_det_kw($arr);

        echo json_encode($output);
    }

    public function get_dashboard_loc() {
        $arr['tahun'] = $this->input->post('tahun');
        $arr['bulan'] = $this->input->post('bulan');
        $arr['loc_id'] = $this->input->post('loc_id');
        $output['header'] = $this->dashboard_model->get_dashboard_loc($arr);
        $output['atas'] = $this->dashboard_model->get_dashboard_loc_atas($arr);
        echo json_encode($output);
    }

    public function dashboard_detail() {

        $data['pro_id']=$this->input->get('pro_id');
        $data['bulan']=$this->input->get('bulan');


        $this->load->view('dashboard/dashboard_view_det',$data);
    }

    public function daftar_wbs()
    {
        $pro_id = $this->input->post('pro_id');
        $alert = $this->input->post('alert');
        if($alert){
            $this->session->set_flashdata('info', $alert);
        }
        $data['pro_id'] = $pro_id;


        $data['form_pro'] =$this->wbs_model->detail_proyek($pro_id);
        $data['tittle'] = 'Daftar Task';
        // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        //print_r($data);die();
        $this->load->view('wbs/wbs_list_detail_view2', $data);
    }

    public function get_data_paging_detail()
    {
        $pro_id = $this->input->get('pro_id');
        //echo $nopd; die();
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];

        $data['v_det'] = $this->wbs_model->get_list_data_detail($cond, $length, $start,$pro_id);
        $totalData = $this->wbs_model->get_count_data_detail($cond, true,$pro_id);
        $v_content['v_data'] = [];
        $i = 1;
        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $key => $value) {
//      ===================  role untuk wbs edit ================

                            $btnok='<a href="#"   onclick="lihat_wbs(\'' . $value['wbs_id'] . '\')" class="btn btn-xs btn-success  tooltips"><i class="fa fa-eye "></i><span class="tooltiptexts">lihat</span></a>';


//      ===================  role untuk wbs edit ================


                $v_content['v_data'][] = array(
//                    $value['rownum'],
                    $value['wbs_name'],
                    $value['wbs_pic1_name'],
                    $value['wbs_time'],
                    $value['wbs_start_date'],
                    $value['wbs_end_date'],
                    $value['wbs_status'],
//                    trim($btnok)

                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );
        //output to json format
        echo json_encode($output);
    }





    
}
