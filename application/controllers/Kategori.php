<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Kategori extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('kategori_model');
    }

    public function get_data_paging() {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];
       // echo 'cc';die();

        $data['v_det'] = $this->kategori_model->get_list_data($cond, $length, $start);
        $totalData = $this->kategori_model->get_count_data($cond, true);
        $v_content['v_data'] = [];


        if (!empty($data['v_det'])) {
            foreach ($data['v_det'] as $value) {
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['kategori_name'],
                    trim('<a href="#"   onclick="edit_kategori(\'' . $value['kategori_id'] . '\')" class="btn btn-xs btn-warning  tooltips"><i class="icon_pencil-edit"></i><span class="tooltiptexts">Edit</span></a>
                    <a href="#"   onclick="delete_kategori(\'' . $value['kategori_id'] . '\')" class="btn btn-xs btn-danger  tooltips"><i class="icon_trash"></i><span class="tooltiptexts">Delete</span></a>')

                );
            }
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );
        //output to json format
        echo json_encode($output);
    }

    public function index() {
        $data['tittle'] = 'Daftar Kategori Tenant';
        // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        $this->load->view('kategori/kategori_list_view', $data);
    }

    public function add_data() {
        $data['tittle'] = 'Tambah PT';
        $this->load->view('kategori/kategori_add_view',$data);
    }



    public function edit_data() {
        $kategori_id = $this->input->post('kategori_id');
        $data['form'] = $this->kategori_model->detail_kategori($kategori_id);
        $this->load->view('kategori/kategori_edit_view', $data);
    }

    public function update_data() {
        $arr['kategori_name'] = strtoupper(trim($this->input->post('kategori_name')));
        $arr['kategori_id'] = strtoupper(trim($this->input->post('kategori_id')));
       if (!empty($arr['kategori_name']) ) {
            $cek_kategori = $this->kategori_model->cek_before_update($arr);
            if (intval($cek_kategori) > 0) {
                echo 'Kategori sudah terdaftar';
            } else {
                $this->kategori_model->update_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil diubah');
            }
        } else {
            $this->session->set_flashdata('error', 'Form kosong');
        }
    }

    public function delete_data() {
        $arr = $this->input->post('kategori_id');
        if (!empty($arr)) {
            $this->kategori_model->delete_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }
    public function insert_data(){
        $arr['kategori_name'] = strtoupper(trim($this->input->post('kategori_name')));

        if (!empty($arr['kategori_name'])) {
            $cek_kategori = $this->kategori_model->cek_before_insert($arr);
            if(intval($cek_kategori) > 0) {
                $this->session->set_flashdata('error', 'Kategori sudah terdaftar.');
            } else {
                $this->kategori_model->insert_data($arr);
                $this->session->set_flashdata('info', 'Data berhasil disimpan!!');
            }
        } else {
            $this->session->set_flashdata('error', 'Proses Gagal!');
        }
    }

    public function load_combo_loc()
    {
        $data=$this->kategori_model->load_combo_loc();
        echo json_encode($data);
    }


}
