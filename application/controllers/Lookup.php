<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : 
  | Email      :
  | Class name : 
  | Date       : 02-05-2017
  ========================================================= */

  class Lookup extends CI_Controller
  {
  	public function __construct()
    {
        parent::__construct();
        $this->load->model('input_harian_model');
    }

    public function lookup_opd($id_opd,$kd_jenis_pajak,$kd_klas_pajak)
    {
        $nm_klas_pajak = $this->input_harian_model->load_nm_klas_pajak($kd_klas_pajak);

    		if($kd_jenis_pajak == '001') {
    			redirect(site_url('input_restoran/input_harian/'.$id_opd));
    		}
        elseif ($kd_jenis_pajak == '002') {
          redirect(site_url('input_hotel/input_harian/'.$id_opd));
        }
        elseif ($kd_jenis_pajak == '003') {
          if($nm_klas_pajak == 'FITNESS') { //0001
            redirect(site_url('input_hiburan_fitness/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'SPA') {
            redirect(site_url('input_hiburan_spa/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'KARAOKE') {
            redirect(site_url('input_hiburan_karaoke/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'BAR/PUB') {
            redirect(site_url('input_hiburan_barpub/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'DISKOTIK') {
            redirect(site_url('input_hiburan_diskotik/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'PANTI PIJAT') {
            redirect(site_url('input_hiburan_pantipijat/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'PERMAINAN'|| $nm_klas_pajak == 'SIRKUS' || $nm_klas_pajak == 'BILLIAR' || $nm_klas_pajak == 'BIOSKOP' ) {
            redirect(site_url('input_hiburan_permainan/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'REFLEKSI') {
            redirect(site_url('input_hiburan_refleksi/input_harian/'.$id_opd));
          }
          elseif($nm_klas_pajak == 'BILLIAR') {
            redirect(site_url('input_hiburan_sirkus/input_harian/'.$id_opd));
          }
        }
        elseif ($kd_jenis_pajak == '004') {
          redirect(site_url('input_parkir/input_harian/'.$id_opd));
        }

    		//$nopd = $this->opd_model->get_nopd($nopd);
    }

    public function lookup_sptpd($id_opd,$kd_jenis_pajak)
    {
        if($kd_jenis_pajak == '001') {
          redirect(site_url('input_restoran/input_sptpd/'.$id_opd));
        }
        elseif ($kd_jenis_pajak == '002') {
          redirect(site_url('input_hotel/input_sptpd/'.$id_opd));
        }
        elseif ($kd_jenis_pajak == '004') {
          redirect(site_url('input_parkir/input_sptpd/'.$id_opd));
        }

        //$nopd = $this->opd_model->get_nopd($nopd);
    }

  }