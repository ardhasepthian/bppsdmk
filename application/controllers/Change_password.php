<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/* =======================================================
  | Author     : Ardha
  | Email      :
  | Class name : change password
  | Date       : 21-04-2017
  ========================================================= */

class Change_password extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('change_password_model');
    }

    public function index() {
        $data['tittle'] = 'Change password';
       // $data['mn_gps'] = $this->get_menu_gps($this->controller);
        $data['username'] = $this->change_password_model->load_data();
        $this->load->view('change_password/change_password_view', $data);
    }
    
    public function change_pass() {
        $arr['user_id']= trim($this->input->post('user_id'));
        $arr['password']= generate_passwd(trim($this->input->post('password')));
        $this->change_password_model->update_data($arr);
        $this->load->view('change_password/change_password_view', $data);
    }
}
