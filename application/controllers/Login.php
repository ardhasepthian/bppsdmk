<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : Arif Fajar Budiman
  | Email      :
  | Class name : Login
  | Date       : 04-04-2017
  ========================================================= */

/**
 * * @property Login_model $login_model
 */

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index() {
//        echo base_url().'ardha';die();
        redirect(base_url() . 'login/signin');
    }

    public function signin() {
        $data['title'] = 'Sign In';
        if($this->session->userdata('siap_sessionid')!=NULL){
            redirect(base_url().'dashboard');
        }else{
            $data['csrf'] = array(
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            );

            $this->load->helper('captcha');
            
            $captcha = create_recaptcha(array(
                'img_path'	    => './assets/captcha/',
                'img_url'	    => base_url().'assets/captcha/',
                'font_path'     => dirname(__DIR__).'/../assets/fonts/arialbd.ttf',
                'img_width'     => 200,
                'img_height'    => 70,
                'expiration'    => 7200,
                'word_length'   => 5,
                'font_size'     => 25,
                'border'        => 0,
                'pool'          => '123ABC',
                'expiration'    => 7200
            ));

            $data['image'] = $captcha['image'];

            $this->session->set_userdata('captcha', strtolower($captcha['word']));

            $this->load->view('login/login_view', $data);
        }
    }


    public function signout() {
        $this->session->sess_destroy();
        redirect(base_url().'dashboard');
    }

    public function refresh_captcha() {
        $this->load->helper('captcha');
            
        $captcha = create_recaptcha(array(
            'img_path'	    => './assets/captcha/',
            'img_url'	    => base_url().'assets/captcha/',
            'font_path'     => dirname(__DIR__).'/../assets/fonts/arialbd.ttf',
            'img_width'     => 200,
            'img_height'    => 70,
            'expiration'    => 7200,
            'word_length'   => 5,
            'font_size'     => 25,
            'border'        => 0,
            'pool'          => '123ABC',
            'expiration'    => 7200
        ));

        $this->session->set_userdata('captcha', strtolower($captcha['word']));

        echo $captcha['image'];
    }

    public function auth() {


	    $arr['usn'] = strtolower($this->input->post('usn'));
        $arr['pwd_old'] = $this->input->post('pwd');
        $arr['pwd'] = generate_passwd($this->input->post('pwd'));
      // echo $arr['pwd'];die();
//        $arr['pwd'] = $this->input->post('pwd');

        if (!empty($arr['usn']) && !empty($arr['pwd'])) {
            $result = "";

            $cek_pass   = $this->login_model->cek_pass($arr);

            if(intval($cek_pass) > 0) {

                $result = $this->login_model->get_data_user($arr);
            }else {
                $this->session->set_flashdata('error', 'Periksa kembali User dan Password anda! <br/> Silahkan hubungi Administrator!');
                redirect(base_url() . 'login/signin');
            }
            
            if($result != FALSE) {
                $sessdata['efiskus_sessionid'] = md5($result['code']).md5(date('m/d/Y H:i:s'));
                $sessdata['efiskus_code']      = $result['code'];
                $sessdata['efiskus_name']      = $result['name'];
                $sessdata['efiskus_address']     = $result['address'];
                $sessdata['efiskus_loc_id']   = $result['loc_id'];
                $sessdata['efiskus_loc_name']   = $result['loc_name'];
                $sessdata['efiskus_loc_code']   = $result['loc_code'];
                $sessdata['efiskus_kd_keldes']   = $result['kd_keldes'];
                $sessdata['efiskus_id_user']   = $result['id_user'];
                $sessdata['efiskus_id_group']  = $result['id_group'];
                $sessdata['efiskus_nm_group']  = $result['nm_group'];

                $this->session->set_userdata($sessdata);
                redirect(base_url() . 'dashboard2');
            }else {
                $this->session->set_flashdata('error', 'Data NPWP tidak ditemukan! <br/> Silahkan hubungi Administrator!');
                redirect(base_url() . 'login/signin');
            }





        } else {
            $this->session->set_flashdata('error', 'User dan Password tharus diisi!');
            redirect(base_url() . 'login/signin');
        }
    }

}
