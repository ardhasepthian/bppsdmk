<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* =======================================================
  | Author     : Ardha
  | Email      :
  | Class name : user
  | Date       : 04-04-2017
  ========================================================= */

class User extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
       // $controller = $this->controller;
       // modul_checkauthorize($controller);
    }

    public function get_paging() {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];

        $data['v_det'] = $this->user_model->table_paging($cond, $length, $start);
        $totalData = $this->user_model->count_paging($cond, true);
        $v_content['v_data'] = [];
        $i = 1;
        if (!empty($data['v_det']))
            foreach ($data['v_det'] as $key => $value) {
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['username'],
                    $value['nm_pegawai'],
                    $value['nm_group'],
                    trim(
                            '<div class="btn-group col-md-12">
      <button type="button" class="btn btn-sm btn-block btn-default dropdown-toggle" data-toggle="dropdown">
      '. $this->config->item('text_btn_action') .' <span class="caret"></span>
      </button>
      <ul class="dropdown-menu col-md-12" role="menu">
                    <a href="#" onclick="edit_user(' . $value['id_user'] . ')" class="btn btn-xs btn-warning btn-block">'. $this->config->item('text_btn_edit') .'</a>
                    <a href="#" onclick="delete_user(' . $value['id_user'] .',\''. $value['username'].'\')" class="btn btn-xs btn-danger btn-block">'. $this->config->item('text_btn_delete') .'</a>
                    <a href="#" onclick="reset_user(' . $value['id_user'] .',\''. $value['username'].'\', \''. $value['kode'].'\')" class="btn btn-xs btn-primary btn-block">Reset Password</a>    
                    </ul>
	  </div>'
                    )
                );
            }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );

        //output to json format
        echo json_encode($output);
    }
    //===============================================================getpaging modal pegawai========
    public function get_paging_data_user() {
        $draw = $this->input->get('draw');
        $length = $this->input->get('length');
        $start = $this->input->get('start');
        $cond = $this->input->get('search')["value"];
        $nm_group = $this->input->get('group');
        $source ='';
        //echo '234234234'.$nm_group;die();
        if($nm_group =='FISKUS' || $nm_group =='SUPER ADMINISTRATOR' || $nm_group =='ADMINISTRATOR' || $nm_group =='ADMIN SUBAN' || $nm_group =='ADMIN UPPRD' ){
            $source ='pegawai';
        }else if ($nm_group =='WAJIB PAJAK'){
            $source ='wpd';
        }else if($nm_group =='OBJEK PAJAK'){
            $source = 'Proyek';
        }

        $wcon = " and source='".$source."'";


        $data['v_det'] = $this->user_model->table_paging_data_user($cond, $length, $start,$wcon);
        $totalData = $this->user_model->count_paging_data_user($cond, true,$wcon);
        $v_content['v_data'] = [];
        $i = 1;
        if (!empty($data['v_det']))
            foreach ($data['v_det'] as $key => $value) {
                $v_content['v_data'][] = array(
                    $value['rownum'],
                    $value['code'],
                    $value['name'],
                    trim(
                            '<center>
                    <a href="#" data-dismiss="modal" onclick="add_data_user(\''. $value['code'].'\',\''. $value['name'].'\')" class="btn btn-xs btn-danger">'. $this->config->item('text_btn_choose') .'</a>
                    </center>'
                    )
                );
            }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $totalData,
            "recordsFiltered" => $totalData,
            "data" => $v_content['v_data']
        );

        //output to json format
        echo json_encode($output);
    }
    //===============================================================getpaging modal pegawai ENDS========
    public function index() {
       $data['tittle'] = 'Daftar User';
       // $controller = $this->controller;
       // $data['mn_gps'] = $this->get_menu_gps($this->controller);
       $data['mn_gps']="";
        $this->load->view('user/user_list_view', $data);
    }

    public function add_data() {
        $data['usergroup'] = $this->user_model->load_combo_usergroup();
        $this->load->view('user/user_add_view',$data);
    }


    public function edit_data() {
        $id = trim($this->input->post('id'));
        $data['usergroup'] = $this->user_model->load_combo_usergroup();
        $data['form'] = $this->user_model->load_form_update_user($id);
        //print_r($data);die();
        $this->load->view('user/user_edit_view', $data);
    }

    public function reset_data() {
        $id = trim($this->input->post('id'));
        $kode = trim($this->input->post('kode'));
        $kodes =generate_passwd(trim($this->input->post('kode')));
        $data = $this->user_model->reset_data($id,$kodes);

        $this->session->set_flashdata('info', 'Password berhasil direset menjadi kode '.$kode);
        //$this->load->view('administration/user/user_detail_view', $data);
        //echo json_encode($data);
    }

    public function insert_data() {

        $arr['kode'] = strtoupper(trim($this->input->post('kode')));
        $arr['id_group'] = strtoupper(trim($this->input->post('id_group')));
        $arr['username'] = strtolower(trim($this->input->post('username')));
        $arr['password'] = generate_passwd(trim($this->input->post('password'))); //with encrypt
//        $arr['password'] = trim($this->input->post('password'));

       $cek_before_insert= $this->user_model->cek_before_insert($arr);
      // $cek_before_insert=0;
       if (intval($cek_before_insert) > 0) {
              $error ='Username sudah terdaftar!';
                echo $error;
        }else{
            $id = $this->user_model->insert_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil di simpan');

            }
         }


    public function update_data() {
        $param_audit['controller']  = $this->controller;
        $param_audit['function']    = $this->function;

        $arr['id_user'] =trim($this->input->post('id_user'));
        $arr['id_group'] = trim($this->input->post('id_group'));

            $id = $this->user_model->update_data($arr);
            $this->session->set_flashdata('info', 'Data berhasil diubah');

    }

    public function delete_data () {
//        $param_audit['controller']  = $this->controller;
//        $param_audit['function']    = $this->function;

        $id_user = strtoupper(trim($this->input->post('id')));
        $this->user_model->delete_data($id_user);
//        $param_audit['audittrail_sts'] = 1 ;
//        $this->generate_audittrail($param_audit);
        $this->session->set_flashdata('info', 'Data berhasil dihapus');
        }

}
