<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'vendor/phpoffice/phpword/bootstrap.php';

class Phpword
{
    private $word;

	function __construct()
	{

	}

    public function load() {
        $this->word      = new \PhpOffice\PhpWord\PhpWord();
        return $this->word;
    }
}