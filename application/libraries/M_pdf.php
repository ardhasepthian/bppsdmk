<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require_once 'vendor/mpdf/mpdf/mpdf.php';
require_once 'vendor/autoload.php';
use Mpdf\Mpdf;

class M_pdf
{

    private $param;
    private $pdf;

	function __construct()
	{
//      $mode = '',             -- UTF-8
//      $format = 'A4',         -- paper size
//      $default_font_size = 0, -- size
//      $default_font = '',     -- font
//      $mgl = 15,              -- margin left
//      $mgr = 15,              -- margin right
//      $mgt = 16,              -- margin top
//      $mgb = 16,              -- margin bottom
//      $mgh = 9,               -- margin header
//      $mgf = 9,               -- margin footer
//      $orientation = 'P'      -- orientation

	}

	public function setParam($param = NULL) {
	    if($param != NULL && is_array($param) == 1) {
            $this->param = $param;
        }
    }

    public function load() {
	    if(is_array($this->param) == 1) {
            $mode = (isset($this->param['mode']) ? $this->param['mode'] : "''");
            $format = (isset($this->param['format']) ? $this->param['format'] : 'A4-L');
            $default_font_size = (isset($this->param['default_font_size']) ? $this->param['default_font_size'] : 0);
            $default_font = (isset($this->param['default_font']) ? $this->param['default_font'] : "''");
            $mgl = (isset($this->param['mgl']) ? $this->param['mgl'] : 15);
            $mgr = (isset($this->param['mgr']) ? $this->param['mgr'] : 15);
            $mgt = (isset($this->param['mgt']) ? $this->param['mgt'] : 15);
            $mgb = (isset($this->param['mgb']) ? $this->param['mgb'] : 15);
            $mgh = (isset($this->param['mgh']) ? $this->param['mgh'] : 9);
            $mgf = (isset($this->param['mgf']) ? $this->param['mgf'] : 9);
//            print_r($mode);echo "<br>";
//            print_r($format);echo "<br>";
//            print_r($default_font_size);echo "<br>";
//            print_r($default_font);echo "<br>";
//            print_r($mgl);echo "<br>";
//            print_r($mgr);echo "<br>";
//            print_r($mgt);echo "<br>";
//            print_r($mgb);echo "<br>";
//            print_r($mgh);echo "<br>";
//            print_r($mgf);echo "<br>";die;

            $this->pdf      = new Mpdf($mode, $format, $default_font_size, $default_font, $mgl, $mgr, $mgt, $mgb, $mgh, $mgf);
        } else {
          //  $this->pdf      = new \Mpdf();
            $this->pdf      = new \Mpdf\Mpdf();
//            $this->pdf      = new Mpdf('','A4-L',0,'',0,0,0,0,0,0);
        }
        // $this->pdf->showImageErrors = true;
        return $this->pdf;
    }
}
