<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$config['max_upload'] = 100000;
$config['text_btn_action']     = '';
$config['text_btn_add']     = 'Tambah';
$config['text_btn_approve']     = 'Setuju';
$config['text_btn_approved']     = 'di setujui';
$config['text_btn_approval']     = 'Persetujuan';
$config['text_btn_attach']  = 'Lampirkan File';
$config['text_btn_back']    = 'Kembali';
$config['text_btn_cancel']  = 'Batal';
$config['text_btn_choose']  = 'Pilih';
$config['text_btn_close']   = 'Tutup';
$config['text_btn_delete']  = 'Hapus';
$config['text_btn_done']    = 'Selesai';
$config['text_btn_edit']    = 'Ubah';
$config['text_btn_excel']   = 'Excel';
$config['text_btn_finish']  = 'Selesai';
$config['text_btn_input']   = 'Input';
$config['text_btn_need_correction']    = 'Butuh Koreksi';
$config['text_btn_next']    = 'Lanjut';
$config['text_btn_print']   = 'Cetak';
$config['text_btn_process'] = 'Proses';
$config['text_btn_proposed'] = 'Pengajuan';
$config['text_btn_refresh'] = 'Refresh';
$config['text_btn_reject']  = 'Tolak';
$config['text_btn_revision'] = 'Revisi';
$config['text_btn_send_to'] = 'Kirim ke';
$config['text_btn_setting'] = 'Pengaturan';
$config['text_btn_submit']  = 'Simpan';
$config['text_btn_update']  = 'Ubah';
$config['text_btn_view']    = 'Lihat';
$config['text_btn_filter']  = 'Filter';
$config['text_btn_go']  = 'Pergi';
$config['text_btn_search']  = 'Cari';
$config['theme']            = array('blue' => 'Blue', 'darkblue' => 'Dark Blue', 'green' => 'Green', 'purple' => 'Purple', 'orange' => 'Orange', 'pink' => 'Cool Pink');
// $config['chart_color_palette'] = json_encode(['rgb(3, 169, 244)', 'rgb(139, 195, 74)', 'rgb(255, 235, 59)', 'rgb(255, 152, 0)', 'rgb(244, 67, 54)', 'rgb(156, 39, 176)', '#ffe481', '#d2ff8b', '#a5d1ff', '#e39cff']);
$config['chart_color_palette'] = json_encode(['#56BE97', '#74CDDF', '#FACF5A', '#F16E7C', '#B494C5', '#CD683B', 'rgb(209,150,64)', 'rgb(50,54,57)', 'rgb(245,184,59)', 'rgb(242,110,125)', 'rgb(181,148,196)', 'rgb(50,54,57)', 'rgb(214,199,61)', '#99CF96', '#FCF061', '#F7AD96', '#B3BDE0', '#8FD6E0']);
$config['chart_color_palette_colorfull'] = json_encode(['#00e0ff', '#22ff00', '#ff0000', '#ff00cf', 'rgb(244, 67, 54)', 'rgb(156, 39, 176)', '#ffe481', '#d2ff8b', '#a5d1ff', '#e39cff']);
// $config['chart_color_palette'] = json_encode(['#a5ffc6', '#b3ff8d', '#fff598', '#ffd99f', '#ff9c9c', '#ff7f7f', '#ffe481', '#d2ff8b', '#a5d1ff', '#e39cff']);
